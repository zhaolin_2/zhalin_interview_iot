package com.ruoyi.web.controller.data;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.DeviceData;
import com.ruoyi.system.tcp.TcpDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Lin
 * @Date 2022 11 17 15
 * 数据处理 控制层 用于测试 数据解析相关内容
 **/
@RestController
@RequestMapping("/data.h")
public class DataHandlerController {

    @Autowired
    TcpDataHandler tcpDataHandler;

    @GetMapping("/dataAnalysis")
    public AjaxResult dataAnalysis(DeviceData deviceData){
        String s = tcpDataHandler.dataHandlerForTest(deviceData);
        return AjaxResult.success(s);
    }
}
