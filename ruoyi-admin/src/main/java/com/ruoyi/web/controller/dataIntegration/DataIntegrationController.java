package com.ruoyi.web.controller.dataIntegration;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.OfficeDevice;
import com.ruoyi.system.domain.request.dataIntegration.DeviceLatestDataRequest;
import com.ruoyi.system.domain.request.dataIntegration.ElectricPowerRateRequest;
import com.ruoyi.system.service.IDeviceLimitService;
import com.ruoyi.system.service.IElectricityMeterService;
import com.ruoyi.system.service.IElectricityRateService;
import com.ruoyi.system.service.IOfficeDeviceService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.l;

/**
 * @Author Lin
 * @Date 2022 11 18 11
 * 设备管理控制层类
 **/
@RestController
@RequestMapping("/data_igr")
public class DataIntegrationController {


    @Autowired
    private IOfficeDeviceService officeDeviceService;

    @Autowired
    private IElectricityMeterService electricityMeterService;

    @Autowired
    private IElectricityRateService electricityRateService;

    @Autowired
    private IDeviceLimitService deviceLimitService;

    /**
     * 返回设备在线状态
     * @return
     */
    @GetMapping("/deviceState")
    @Anonymous
    public AjaxResult deviceState()
    {
        List l = new ArrayList();
        l.add(officeDeviceService.deviceStateRequest());
       return AjaxResult.success(l);
    }


    /**
     * 库存数量 总数与预警
     */
    /**
     * 返回库存储备状态
     * @return
     */
    @GetMapping("/stockState")
    @Anonymous
    public AjaxResult stockState()
    {
        List l = new ArrayList();
        l.add(officeDeviceService.stockStateRequest());
        return AjaxResult.success(l);
    }

    /**
     * 库存数量 总数与预警
     */
    /**
     * 最新数据返回状态信息整合
     * @return
     */
    @GetMapping("/dataState")
    @Anonymous
    public AjaxResult dateState()
    {
        List<DeviceLatestDataRequest> deviceLatestDataRequests = officeDeviceService.deviceLatestDataRequest();
        return AjaxResult.success(deviceLatestDataRequests);
    }


    /**
     * 电费数据 整合返回
     * @return
     */
    @GetMapping("/rateState")
    @Anonymous
    public AjaxResult rateState()
    {
        ElectricPowerRateRequest electricPowerRateRequest = electricityRateService.electricRateData();
        List list = new ArrayList<>();
        list.add(electricPowerRateRequest);
        return AjaxResult.success(list);
    }


    /**
     * 电费曲线图整合返回
     */

    @GetMapping("/rateLine")
    @Anonymous
    public AjaxResult rateLine(Integer count)
    {
        List list = electricityRateService.selectRateRecordForLine(7);
        return AjaxResult.success(list);
    }


}
