package com.ruoyi.web.controller.device;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.domain.OfficeDevice;
import com.ruoyi.system.service.IDeviceLimitService;
import com.ruoyi.system.service.IOfficeDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Lin
 * @Date 2022 11 18 11
 * 设备管理控制层类
 **/
@RestController
@RequestMapping("/device")
public class DeviceController {


    @Autowired
    private IOfficeDeviceService officeDeviceService;

    @Autowired
    private IDeviceLimitService deviceLimitService;

    /**
     * 查询设备列表，用于返回给地图标识使用
     * 参数为 id  不传id 时，默认返回所有设备
     */
    @GetMapping("/list")
    public AjaxResult deviceList(Integer id)
    {
        OfficeDevice officeDevice = new OfficeDevice();
        officeDevice.setId(id);
        List<OfficeDevice> list = officeDeviceService.selectOfficeDeviceList(officeDevice);
        return AjaxResult.success(list);
    }


    /**
     * 根据设备编号 查询该设备相关的报警上下限项目
     * @param deviceNo
     * @return
     */
    @GetMapping("/limitQuery")
    public AjaxResult limitUpdate(Long id,String deviceNo){
        return AjaxResult.success(deviceLimitService.selectDeviceLimitByDeviceNo(id,deviceNo));
    }

    /**
     * 更新设备报警上下限
     * @param deviceLimit
     * @return
     */
    @PostMapping("/limitUpdate")
    public AjaxResult limitUpdate(@RequestBody DeviceLimit deviceLimit){
        return AjaxResult.success(deviceLimitService.updateDeviceLimit(deviceLimit));
    }

}
