package com.ruoyi.web.controller.fileDemo;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FileDistribute;
import com.ruoyi.system.service.IFileDistributeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件分发DemoController
 * 
 * @author Lin
 * @date 2022-11-21
 */
@RestController
@RequestMapping("/system/distribute")
public class FileDistributeController extends BaseController
{
    @Autowired
    private IFileDistributeService fileDistributeService;

    /**
     * 查询文件分发Demo列表
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:list')")
    @GetMapping("/list")
    public TableDataInfo list(FileDistribute fileDistribute)
    {
        startPage();
        if (SecurityUtils.getUsername().equals("admin")){
            List<FileDistribute> list = fileDistributeService.selectFileDistributeList(fileDistribute);
            return getDataTable(list);
        }else{
            fileDistribute.setFileReceived(SecurityUtils.getUsername());
            fileDistribute.setFileLife(0);
            List<FileDistribute> fileDistributes = fileDistributeService.selectFileDistributeList(fileDistribute);
            return getDataTable(fileDistributes);
        }

    }

    /**
     * 导出文件分发Demo列表
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:export')")
    @Log(title = "文件分发Demo", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FileDistribute fileDistribute)
    {
        List<FileDistribute> list = fileDistributeService.selectFileDistributeList(fileDistribute);
        ExcelUtil<FileDistribute> util = new ExcelUtil<FileDistribute>(FileDistribute.class);
        util.exportExcel(response, list, "文件分发Demo数据");
    }

    /**
     * 获取文件分发Demo详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fileDistributeService.selectFileDistributeById(id));
    }

    /**
     * 新增文件分发Demo
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:add')")
    @Log(title = "文件分发Demo", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FileDistribute fileDistribute)
    {
        fileDistribute.setFileUploadTime(new Date());
        return toAjax(fileDistributeService.insertFileDistribute(fileDistribute));
    }

    /**
     * 修改文件分发Demo
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:edit')")
    @Log(title = "文件分发Demo", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FileDistribute fileDistribute)
    {
        return toAjax(fileDistributeService.updateFileDistribute(fileDistribute));
    }

    /**
     * 删除文件分发Demo
     */
    @PreAuthorize("@ss.hasPermi('system:distribute:remove')")
    @Log(title = "文件分发Demo", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fileDistributeService.deleteFileDistributeByIds(ids));
    }
}
