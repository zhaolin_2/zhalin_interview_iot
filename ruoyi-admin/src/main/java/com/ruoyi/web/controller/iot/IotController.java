package com.ruoyi.web.controller.iot;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.OfficeReport;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Lin
 * @Date 2022 10 27 09
 **/
@RestController
@RequestMapping("/iot")
public class IotController extends BaseController {

    @Autowired
    TestService testService;

    @Autowired
    IOfficeHumitureService officeHumitureService;

    @Autowired
    ILouverBoxService louverBoxService;

    @Autowired
    ISmokeDetectorService smokeDetectorService;

    @Autowired
    IPtDeviceService ptDeviceService;

    @Autowired
    IElectricityMeterService electricityMeterService;

    @Autowired
    IOfficeReportService officeReportService;


    /**
     * 从数据库中查询温湿度曲线
     * @return 返回一个数组，数组内包含温度数组，湿度数组，时间数组
     */
    @GetMapping("/humitureData")
    public AjaxResult selectHumitureList(){
        List list = new ArrayList();
        list.add(officeHumitureService.getHumitureNowData());
       return AjaxResult.success(list);
    }

    /**
     * 将数据库中最新一条数据返回并且整理成 新的温湿度信息类，返回给前端，需要前端定时调用
     * @return 返回一个温湿度信息类
     */
    @GetMapping("/humitureList")
    public AjaxResult selectHumitureData(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        if (selectTime !=null){
            List list = officeHumitureService.selectHumitureListByTime(date);
            return AjaxResult.success(list);
        }else {
            List list = officeHumitureService.selectHumitureListByTime(date);
            return AjaxResult.success(list);
        }
    }

    @GetMapping("/ptTempList")
    public AjaxResult selectPtDeviceListByDate(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        if (selectTime != null){
            List list = ptDeviceService.ptTemperatureList(date);
            return AjaxResult.success(list);
        }else {
            List list = ptDeviceService.ptTemperatureList(date);
            return AjaxResult.success(list);
        }
    }

    @GetMapping("/louverHumitureList")
    public AjaxResult selectLouverBoxHumitureList(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        if (selectTime != null){
            List list = louverBoxService.selectLouverHumitureList(date);
            return AjaxResult.success(list);
        }else {
            List list = louverBoxService.selectLouverHumitureList(date);
            return AjaxResult.success(list);
        }
    }

    /**
     * 查询光照强度曲线
     *
     * @param selectTime
     * @return
     */
    @GetMapping("/louverIlluminationList")
    public AjaxResult selectLouverBoxilluminationList(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        if (selectTime != null){
            List list = louverBoxService.selectIlluminationList(date);
            return AjaxResult.success(list);
        }else {
            List list = louverBoxService.selectIlluminationList(date);
            return AjaxResult.success(list);
        }
    }

    /**
     * 返回查询日的电流曲线
     *
     * @param selectTime
     * @return
     */
    @GetMapping("/electricityList")
    public AjaxResult selectElectricityList(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        if (selectTime != null){
            List list = electricityMeterService.selectElectricityListByDate(date);
            return AjaxResult.success(list);
        }else {
            List list = electricityMeterService.selectElectricityListByDate(date);
            return AjaxResult.success(list);
        }
    }

    /**
     * 返回最新的电表数据
     * @return
     */
    @GetMapping("/electricityData")
    public AjaxResult selectLastElectricityData(){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        List list = new ArrayList();
        ElectricityMeter electricityMeter = electricityMeterService.selectLastElectricity(date);
        list.add(electricityMeter);
        return AjaxResult.success(list);
    }

    /**
     * 根据日期查询办公室报表数据
     * @param selectTime 查询时间
     * @return
     */
    @GetMapping("/dailyConsum")
    public AjaxResult selectDailyConsum(Date selectTime){
        Date date = new Date();
        date.setYear(2023 - 1900);
        date.setMonth(5);
        date.setDate(1);
        List list = new ArrayList();
        OfficeReport officeReport = officeReportService.selectReportGenerate(date);
        list.add(officeReport);
        return AjaxResult.success(list);
    }


    /**
     * 查询能耗数组的方法
     * @param count 查询的总天数【查询近？天的能耗数组】
     * @return
     */
    @GetMapping("/consumQuery")
    public AjaxResult consumOfficeQuery(Integer count){
        List list = officeReportService.selectConsumptionList(count);
        return AjaxResult.success(list);
    }


    /**
     * 模拟触发烟雾报警器
     */
    @GetMapping("/smokeTest")
    @PreAuthorize("@ss.hasRole('admin')")
    public AjaxResult smokeDetectorTest() throws Exception {
//            smokeDetectorService.smokeDetectorRecord("04030200017444");
            return AjaxResult.success("烟雾报警器模拟触发：完成！");
    }







}
