package com.ruoyi.web.controller.iot;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.TElectricityMeter;
import com.ruoyi.system.service.IOfficeReportService;
import com.ruoyi.system.service.ITElectricityMeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.driver.mac.net.NetStat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Lin
 * @Date 2022 11 09 17
 **/
@RestController
@RequestMapping("/t.iot")
public class TiantuoIotController {

    @Autowired
    ITElectricityMeterService itElectricityMeterService;

    @Autowired
    IOfficeReportService officeReportService;


    /**
     * 返回三项电表的电流 总有功功率 时间
     * @return
     */
    @GetMapping("/TElectricityList")
    public AjaxResult selectHumitureData(Date selectTime){
        if (selectTime !=null){
            List list = itElectricityMeterService.selectTElectricityListByDate(selectTime);
            return AjaxResult.success(list);
        }else {
            Date date = new Date();
            List list = itElectricityMeterService.selectTElectricityListByDate(date);
            return AjaxResult.success(list);
        }
    }

    /**
     * 返回三项电表 的全部数据
     * @return
     */
    @GetMapping("/TElectricityData")
    public AjaxResult selectTElectricityData(){
        List list = new ArrayList();
        TElectricityMeter tElectricityMeter = itElectricityMeterService.selectLastTElectricity();
        list.add(tElectricityMeter);
        return AjaxResult.success(list);
    }


    /**
     * 查询三项电表能耗数组
     * @param count 查询的总天数【查询近？天的能耗数组】
     * @return
     */
    @GetMapping("/TConsumQuery")
    public AjaxResult consumTest(Integer count){
        List list = officeReportService.selectConsumptionListForT(count);
        return AjaxResult.success(list);
    }


}
