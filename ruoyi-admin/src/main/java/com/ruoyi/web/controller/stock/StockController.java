package com.ruoyi.web.controller.stock;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.request.OfficeStockRequest;
import com.ruoyi.system.domain.request.StockMessageRequest;
import com.ruoyi.system.domain.request.TemplateUnitRequest;
import com.ruoyi.system.domain.request.TemplateUnitUpdateRequest;
import com.ruoyi.system.service.IOfficeStockService;
import com.ruoyi.system.service.IProductTemplateService;
import com.ruoyi.system.service.IStockMessageService;
import com.ruoyi.system.service.ITemplateUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Lin
 * @Date 2022 11 30 15
 **/
@RestController
@RequestMapping("/stock")
public class StockController extends BaseController {

    @Autowired
    private IOfficeStockService officeStockService;


    @Autowired
    private ITemplateUnitService templateUnitService;

    @Autowired
    private IProductTemplateService productTemplateService;


    /**
     * 出库管理
     * num 出库数量
     */
    @PostMapping("/outStock")
    public AjaxResult outStock(@RequestBody StockMessageRequest stockMessageRequest){
        officeStockService.outStock(stockMessageRequest.getId(),stockMessageRequest.getNum(), SecurityUtils.getUsername(),stockMessageRequest.getMessage());
        return AjaxResult.success("出库成功！");
    }

    /**
     * 入库管理
     */
    @PostMapping("/inStock")
    public AjaxResult inStock(@RequestBody StockMessageRequest stockMessageRequest){
        officeStockService.inStock(stockMessageRequest.getId(),stockMessageRequest.getNum(),SecurityUtils.getUsername(),stockMessageRequest.getMessage());
        return AjaxResult.success("入库成功");
    }

    /**
     * 库存详情
     * id ：库存id
     */
    @GetMapping("/stockMes")
    public AjaxResult stockMessage(Long id){
        return AjaxResult.success(officeStockService.selectsStockMessagesByStockId(id));
    }

    /**
     *
     * @param stockMessageRequest id 数量 报废信息
     * @return
     */
    @PostMapping("/stkToWst")
    public AjaxResult stockToWaste(@RequestBody StockMessageRequest stockMessageRequest){
        officeStockService.outStockToWst(stockMessageRequest.getId(),stockMessageRequest.getNum(),stockMessageRequest.getMessage());
        return AjaxResult.success("操作成功！");
    }

    /**
     * 根据产品模板id 查询内含组件信息
     */
    @GetMapping("/stockUnits")
    public AjaxResult getStockUnitsByTpltId(Long templateId){
        List<TemplateUnitRequest> templateUnitRequests = templateUnitService.showTemplateUnitMes(templateId);
        return AjaxResult.success(templateUnitRequests);
    }

    /**
     * 为产品模板添加组件
     */
    @PostMapping("/addUnits")
    public AjaxResult addTemplateUnits(@RequestBody TemplateUnitUpdateRequest templateUnitUpdateRequest){
       templateUnitService.addTemplateUnits(templateUnitUpdateRequest.getTemplateId(),templateUnitUpdateRequest.getStockId(),templateUnitUpdateRequest.getNum());
        return AjaxResult.success("添加成功");
    }

    /**
     * 为产品模板修改组件数量
     */
    @PostMapping("/updateUnits")
    public AjaxResult updateTemplateUnits(@RequestBody TemplateUnitUpdateRequest templateUnitUpdateRequest){
        if (templateUnitUpdateRequest.getNum() == 0){
            return toAjax(templateUnitService.deleteTemplateUnits(templateUnitUpdateRequest.getStockId()));
        }else{
            return toAjax(templateUnitService.updateTemplateUnits(templateUnitUpdateRequest.getStockId(),templateUnitUpdateRequest.getNum()));
        }
    }

    /**
     * 为产品模板删除组件
     */
    @PostMapping("/deleteUnits/{stockId}")
    public AjaxResult deleteTemplateUnits(@PathVariable("stockId") Long stockId){
        return toAjax(templateUnitService.deleteTemplateUnits(stockId));
    }

    //组装核对
    @PostMapping("/asbCheck")
    public AjaxResult assembleCheck(@RequestBody StockMessageRequest stockMessageRequest){
        //需要模板id 组装数量 两个参数
        List<OfficeStockRequest> officeStockRequests = productTemplateService.checkBeforeAssemble(stockMessageRequest.getId(), stockMessageRequest.getNum());
        return AjaxResult.success(officeStockRequests);
    }

    //组装入库
    @PostMapping("/asbInStk")
    public AjaxResult assembleInStock(@RequestBody OfficeStock officeStock){
        productTemplateService.assembleToStock(officeStock);
        return AjaxResult.success("操作成功");
    }

}
