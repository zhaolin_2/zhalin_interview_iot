package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.service.IDeviceAlertService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报警信息Controller
 * 
 * @author Lin
 * @date 2022-11-23
 */
@RestController
@RequestMapping("/system/alert")
public class DeviceAlertController extends BaseController
{
    @Autowired
    private IDeviceAlertService deviceAlertService;

    /**
     * 查询报警信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DeviceAlert deviceAlert)
    {
        startPage();
        List<DeviceAlert> list = deviceAlertService.selectDeviceAlertList(deviceAlert);
        return getDataTable(list);
    }

    /**
     * 导出报警信息列表
     */
    @Log(title = "报警信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DeviceAlert deviceAlert)
    {
        List<DeviceAlert> list = deviceAlertService.selectDeviceAlertList(deviceAlert);
        ExcelUtil<DeviceAlert> util = new ExcelUtil<DeviceAlert>(DeviceAlert.class);
        util.exportExcel(response, list, "报警信息数据");
    }

    /**
     * 获取报警信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(deviceAlertService.selectDeviceAlertById(id));
    }

    /**
     * 新增报警信息
     */
    @Log(title = "报警信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DeviceAlert deviceAlert)
    {
        return toAjax(deviceAlertService.insertDeviceAlert(deviceAlert));
    }

    /**
     * 修改报警信息
     */
    @Log(title = "报警信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DeviceAlert deviceAlert)
    {
        return toAjax(deviceAlertService.updateDeviceAlert(deviceAlert));
    }



    /**
     * 删除报警信息
     */
    @Log(title = "报警信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(deviceAlertService.deleteDeviceAlertByIds(ids));
    }
}
