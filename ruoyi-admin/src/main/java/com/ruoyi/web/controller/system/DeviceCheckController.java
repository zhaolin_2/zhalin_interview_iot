package com.ruoyi.web.controller.system;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DeviceCheck;
import com.ruoyi.system.service.IDeviceCheckService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 点检Controller
 * 
 * @author Lin
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/system/check")
public class DeviceCheckController extends BaseController
{
    @Autowired
    private IDeviceCheckService deviceCheckService;

    /**
     * 查询点检列表
     */
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(DeviceCheck deviceCheck)
    {
        startPage();
        List<DeviceCheck> list = deviceCheckService.selectDeviceCheckList(deviceCheck);
        return getDataTable(list);
    }

    /**
     * 导出点检列表
     */
    @PostMapping("/export")
    @Anonymous
    public void export(HttpServletResponse response, DeviceCheck deviceCheck)
    {
        List<DeviceCheck> list = deviceCheckService.selectDeviceCheckList(deviceCheck);
        ExcelUtil<DeviceCheck> util = new ExcelUtil<DeviceCheck>(DeviceCheck.class);
        util.exportExcel(response, list, "点检数据");
    }

    /**
     * 获取点检详细信息
     */
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(deviceCheckService.selectDeviceCheckById(id));
    }

    /**
     * 新增点检
     */
    @PostMapping
    @Anonymous
    public AjaxResult add(@RequestBody DeviceCheck deviceCheck)
    {
        return toAjax(deviceCheckService.insertDeviceCheck(deviceCheck));
    }

    /**
     * 修改点检
     */
    @PutMapping
    @Anonymous
    public AjaxResult edit(@RequestBody DeviceCheck deviceCheck)
    {
        return toAjax(deviceCheckService.updateDeviceCheck(deviceCheck));
    }

    /**
     * 删除点检
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(deviceCheckService.deleteDeviceCheckByIds(ids));
    }
}
