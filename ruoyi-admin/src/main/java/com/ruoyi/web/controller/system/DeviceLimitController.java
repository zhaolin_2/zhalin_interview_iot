package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.service.IDeviceLimitService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 限制Controller
 * 
 * @author Lin
 * @date 2022-11-18
 */
@RestController
@RequestMapping("/system/limit")
public class DeviceLimitController extends BaseController
{
    @Autowired
    private IDeviceLimitService deviceLimitService;

    /**
     * 查询限制列表
     */
    @PreAuthorize("@ss.hasPermi('system:limit:list')")
    @GetMapping("/list")
    public TableDataInfo list(DeviceLimit deviceLimit)
    {
        startPage();
        List<DeviceLimit> list = deviceLimitService.selectDeviceLimitList(deviceLimit);
        return getDataTable(list);
    }

    /**
     * 导出限制列表
     */
    @PreAuthorize("@ss.hasPermi('system:limit:export')")
    @Log(title = "限制", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DeviceLimit deviceLimit)
    {
        List<DeviceLimit> list = deviceLimitService.selectDeviceLimitList(deviceLimit);
        ExcelUtil<DeviceLimit> util = new ExcelUtil<DeviceLimit>(DeviceLimit.class);
        util.exportExcel(response, list, "限制数据");
    }

    /**
     * 获取限制详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:limit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(deviceLimitService.selectDeviceLimitById(id));
    }

    /**
     * 新增限制
     */
    @PreAuthorize("@ss.hasPermi('system:limit:add')")
    @Log(title = "限制", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DeviceLimit deviceLimit)
    {
        return toAjax(deviceLimitService.insertDeviceLimit(deviceLimit));
    }

    /**
     * 修改限制
     */
    @PreAuthorize("@ss.hasPermi('system:limit:edit')")
    @Log(title = "限制", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DeviceLimit deviceLimit)
    {
        return toAjax(deviceLimitService.updateDeviceLimit(deviceLimit));
    }

    /**
     * 删除限制
     */
    @PreAuthorize("@ss.hasPermi('system:limit:remove')")
    @Log(title = "限制", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(deviceLimitService.deleteDeviceLimitByIds(ids));
    }
}
