package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OfficeDevice;
import com.ruoyi.system.service.IOfficeDeviceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 设备Controller
 * 
 * @author Lin
 * @date 2022-11-18
 */
@RestController
@RequestMapping("/system/device")
public class OfficeDeviceController extends BaseController
{
    @Autowired
    private IOfficeDeviceService officeDeviceService;

    /**
     * 查询设备列表
     */
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(OfficeDevice officeDevice)
    {
        startPage();
        List<OfficeDevice> list = officeDeviceService.selectOfficeDeviceList(officeDevice);
        return getDataTable(list);
    }

    /**
     * 导出设备列表
     */
    @Log(title = "设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @Anonymous
    public void export(HttpServletResponse response, OfficeDevice officeDevice)
    {
        List<OfficeDevice> list = officeDeviceService.selectOfficeDeviceList(officeDevice);
        ExcelUtil<OfficeDevice> util = new ExcelUtil<OfficeDevice>(OfficeDevice.class);
        util.exportExcel(response, list, "设备数据");
    }

    /**
     * 获取设备详细信息
     */
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(officeDeviceService.selectOfficeDeviceById(id));
    }

    /**
     * 新增设备
     */
    @PreAuthorize("@ss.hasPermi('system:device:add')")
    @Log(title = "设备", businessType = BusinessType.INSERT)
    @PostMapping
    @Anonymous
    public AjaxResult add(@RequestBody OfficeDevice officeDevice)
    {
        return toAjax(officeDeviceService.insertOfficeDevice(officeDevice));
    }

    /**
     * 修改设备
     */
    @PreAuthorize("@ss.hasPermi('system:device:edit')")
    @Log(title = "设备", businessType = BusinessType.UPDATE)
    @PutMapping
    @Anonymous
    public AjaxResult edit(@RequestBody OfficeDevice officeDevice)
    {
        return toAjax(officeDeviceService.updateOfficeDevice(officeDevice));
    }

    /**
     * 删除设备
     */
    @PreAuthorize("@ss.hasPermi('system:device:remove')")
    @Log(title = "设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @Anonymous
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(officeDeviceService.deleteOfficeDeviceByIds(ids));
    }
}
