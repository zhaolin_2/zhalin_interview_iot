package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.service.IOfficeStockService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 库存Controller
 * 
 * @author lin
 * @date 2022-12-06
 */
@RestController
@RequestMapping("/system/stock")
public class OfficeStockController extends BaseController
{
    @Autowired
    private IOfficeStockService officeStockService;

    /**
     * 查询库存列表
     */
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(OfficeStock officeStock)
    {
        startPage();
        List<OfficeStock> list = officeStockService.selectOfficeStockList(officeStock);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */
    @Log(title = "库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OfficeStock officeStock)
    {
        List<OfficeStock> list = officeStockService.selectOfficeStockList(officeStock);
        ExcelUtil<OfficeStock> util = new ExcelUtil<OfficeStock>(OfficeStock.class);
        util.exportExcel(response, list, "库存数据");
    }

    /**
     * 获取库存详细信息
     */
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(officeStockService.selectOfficeStockById(id));
    }

    /**
     * 新增库存
     */
    @Log(title = "库存", businessType = BusinessType.INSERT)
    @Anonymous
    @PostMapping
    public AjaxResult add(@RequestBody OfficeStock officeStock)
    {
        return toAjax(officeStockService.insertOfficeStock(officeStock));
    }

    /**
     * 修改库存
     */
    @Log(title = "库存", businessType = BusinessType.UPDATE)
    @Anonymous
    @PutMapping
    public AjaxResult edit(@RequestBody OfficeStock officeStock)
    {
        return toAjax(officeStockService.updateOfficeStock(officeStock));
    }

    /**
     * 删除库存
     */
    @Log(title = "库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @PreAuthorize("@ss.hasPermi('admin')")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(officeStockService.deleteOfficeStockByIds(ids));
    }

    /**
     * 批量导入 内部接口
     */
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<OfficeStock> util = new ExcelUtil<OfficeStock>(OfficeStock.class);
        List<OfficeStock> stockList = util.importExcel(file.getInputStream());
        String message = officeStockService.importStock(stockList, updateSupport);
        return AjaxResult.success(message);
    }
}
