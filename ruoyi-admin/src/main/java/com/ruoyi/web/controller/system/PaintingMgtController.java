package com.ruoyi.web.controller.system;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PaintingMgt;
import com.ruoyi.system.service.IPaintingMgtService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 书画管理Controller
 * 
 * @author Lin
 * @date 2023-02-15
 */
@RestController
@RequestMapping("/system/painting")
public class PaintingMgtController extends BaseController
{
    @Autowired
    private IPaintingMgtService paintingMgtService;

    /**
     * 查询书画管理列表
     */
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(PaintingMgt paintingMgt)
    {
        startPage();
        List<PaintingMgt> list = paintingMgtService.selectPaintingMgtList(paintingMgt);
        return getDataTable(list);
    }

    /**
     * 导出书画管理列表
     */
    @Log(title = "书画管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @Anonymous
    public void export(HttpServletResponse response, PaintingMgt paintingMgt)
    {
        List<PaintingMgt> list = paintingMgtService.selectPaintingMgtList(paintingMgt);
        ExcelUtil<PaintingMgt> util = new ExcelUtil<PaintingMgt>(PaintingMgt.class);
        util.exportExcel(response, list, "书画管理数据");
    }

    /**
     * 获取书画管理详细信息
     */
    @GetMapping(value = "/{id}")
    @Log(title = "书画管理", businessType = BusinessType.OTHER)
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(paintingMgtService.selectPaintingMgtById(id));
    }


    /**
     * 新增书画管理
     */
    @Log(title = "书画管理", businessType = BusinessType.INSERT)
    @PostMapping
    @Anonymous
    public AjaxResult add(@RequestBody PaintingMgt paintingMgt)
    {
        return toAjax(paintingMgtService.insertPaintingMgt(paintingMgt));
    }

    /**
     * 修改书画管理
     */
    @Log(title = "书画管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @Anonymous
    public AjaxResult edit(@RequestBody PaintingMgt paintingMgt)
    {
        return toAjax(paintingMgtService.updatePaintingMgt(paintingMgt));
    }

    /**
     * 删除书画管理
     */
    @Log(title = "书画管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @Anonymous
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(paintingMgtService.deletePaintingMgtByIds(ids));
    }
}
