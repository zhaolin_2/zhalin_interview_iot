package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductTemplate;
import com.ruoyi.system.service.IProductTemplateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模板Controller
 * 
 * @author lin
 * @date 2022-12-07
 */
@RestController
@RequestMapping("/system/template")
public class ProductTemplateController extends BaseController
{
    @Autowired
    private IProductTemplateService productTemplateService;

    /**
     * 查询模板列表
     */
    @PreAuthorize("@ss.hasPermi('system:template:list')")
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(ProductTemplate productTemplate)
    {
        startPage();
        List<ProductTemplate> list = productTemplateService.selectProductTemplateList(productTemplate);
        return getDataTable(list);
    }

    /**
     * 导出模板列表
     */
    @PreAuthorize("@ss.hasPermi('system:template:export')")
    @Log(title = "模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @Anonymous
    public void export(HttpServletResponse response, ProductTemplate productTemplate)
    {
        List<ProductTemplate> list = productTemplateService.selectProductTemplateList(productTemplate);
        ExcelUtil<ProductTemplate> util = new ExcelUtil<ProductTemplate>(ProductTemplate.class);
        util.exportExcel(response, list, "模板数据");
    }

    /**
     * 获取模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:template:query')")
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(productTemplateService.selectProductTemplateById(id));
    }

    /**
     * 新增模板
     */
    @PreAuthorize("@ss.hasPermi('system:template:add')")
    @Log(title = "模板", businessType = BusinessType.INSERT)
    @PostMapping
    @Anonymous
    public AjaxResult add(@RequestBody ProductTemplate productTemplate)
    {
        return toAjax(productTemplateService.insertProductTemplate(productTemplate));
    }

    /**
     * 修改模板
     */
    @PreAuthorize("@ss.hasPermi('system:template:edit')")
    @Log(title = "模板", businessType = BusinessType.UPDATE)
    @PutMapping
    @Anonymous
    public AjaxResult edit(@RequestBody ProductTemplate productTemplate)
    {
        return toAjax(productTemplateService.updateProductTemplate(productTemplate));
    }

    /**
     * 删除模板
     */
    @PreAuthorize("@ss.hasPermi('system:template:remove')")
    @Log(title = "模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @Anonymous
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productTemplateService.deleteProductTemplateByIds(ids));
    }
}
