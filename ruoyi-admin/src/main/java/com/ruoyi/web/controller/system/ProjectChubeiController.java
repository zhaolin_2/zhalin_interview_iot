package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProjectChubei;
import com.ruoyi.system.service.IProjectChubeiService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 项目储备Controller
 * 
 * @author Lin
 * @date 2023-02-22
 */
@RestController
@RequestMapping("/system/chubei")
public class ProjectChubeiController extends BaseController
{
    @Autowired
    private IProjectChubeiService projectChubeiService;

    /**
     * 查询项目储备列表
     */
    @PreAuthorize("@ss.hasPermi('system:chubei:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProjectChubei projectChubei)
    {
        startPage();
        List<ProjectChubei> list = projectChubeiService.selectProjectChubeiList(projectChubei);
        return getDataTable(list);
    }

    /**
     * 导出项目储备列表
     */
    @PreAuthorize("@ss.hasPermi('system:chubei:export')")
    @Log(title = "项目储备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProjectChubei projectChubei)
    {
        List<ProjectChubei> list = projectChubeiService.selectProjectChubeiList(projectChubei);
        ExcelUtil<ProjectChubei> util = new ExcelUtil<ProjectChubei>(ProjectChubei.class);
        util.exportExcel(response, list, "项目储备数据");
    }


    /**
     * 新增项目储备
     */
    @PreAuthorize("@ss.hasPermi('system:chubei:add')")
    @Log(title = "项目储备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProjectChubei projectChubei)
    {
        return toAjax(projectChubeiService.insertProjectChubei(projectChubei));
    }

    /**
     * 修改项目储备
     */
    @PreAuthorize("@ss.hasPermi('system:chubei:edit')")
    @Log(title = "项目储备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProjectChubei projectChubei)
    {
        return toAjax(projectChubeiService.updateProjectChubei(projectChubei));
    }

    /**
     * 删除项目储备
     */
    @PreAuthorize("@ss.hasPermi('system:chubei:remove')")
    @Log(title = "项目储备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(projectChubeiService.deleteProjectChubeiByIds(ids));
    }
}
