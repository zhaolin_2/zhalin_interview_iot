package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProjectZaitan;
import com.ruoyi.system.service.IProjectZaitanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 在谈项目Controller
 * 
 * @author Lin
 * @date 2023-02-22
 */
@RestController
@RequestMapping("/system/zaitan")
public class ProjectZaitanController extends BaseController
{
    @Autowired
    private IProjectZaitanService projectZaitanService;

    /**
     * 查询在谈项目列表
     */
    @PreAuthorize("@ss.hasPermi('system:zaitan:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProjectZaitan projectZaitan)
    {
        startPage();
        List<ProjectZaitan> list = projectZaitanService.selectProjectZaitanList(projectZaitan);
        return getDataTable(list);
    }

    /**
     * 导出在谈项目列表
     */
    @PreAuthorize("@ss.hasPermi('system:zaitan:export')")
    @Log(title = "在谈项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProjectZaitan projectZaitan)
    {
        List<ProjectZaitan> list = projectZaitanService.selectProjectZaitanList(projectZaitan);
        ExcelUtil<ProjectZaitan> util = new ExcelUtil<ProjectZaitan>(ProjectZaitan.class);
        util.exportExcel(response, list, "在谈项目数据");
    }



    /**
     * 新增在谈项目
     */
    @PreAuthorize("@ss.hasPermi('system:zaitan:add')")
    @Log(title = "在谈项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProjectZaitan projectZaitan)
    {
        return toAjax(projectZaitanService.insertProjectZaitan(projectZaitan));
    }

    /**
     * 修改在谈项目
     */
    @PreAuthorize("@ss.hasPermi('system:zaitan:edit')")
    @Log(title = "在谈项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProjectZaitan projectZaitan)
    {
        return toAjax(projectZaitanService.updateProjectZaitan(projectZaitan));
    }

    /**
     * 删除在谈项目
     */
    @PreAuthorize("@ss.hasPermi('system:zaitan:remove')")
    @Log(title = "在谈项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(projectZaitanService.deleteProjectZaitanByIds(ids));
    }
}
