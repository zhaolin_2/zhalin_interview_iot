package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TemplateUnit;
import com.ruoyi.system.service.ITemplateUnitService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 组件Controller
 * 
 * @author lin
 * @date 2022-12-07
 */
@RestController
@RequestMapping("/system/unit")
public class TemplateUnitController extends BaseController
{
    @Autowired
    private ITemplateUnitService templateUnitService;

    /**
     * 查询组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:unit:list')")
    @GetMapping("/list")
    public TableDataInfo list(TemplateUnit templateUnit)
    {
        startPage();
        List<TemplateUnit> list = templateUnitService.selectTemplateUnitList(templateUnit);
        return getDataTable(list);
    }

    /**
     * 导出组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:unit:export')")
    @Log(title = "组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TemplateUnit templateUnit)
    {
        List<TemplateUnit> list = templateUnitService.selectTemplateUnitList(templateUnit);
        ExcelUtil<TemplateUnit> util = new ExcelUtil<TemplateUnit>(TemplateUnit.class);
        util.exportExcel(response, list, "组件数据");
    }

    /**
     * 获取组件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:unit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(templateUnitService.selectTemplateUnitById(id));
    }

    /**
     * 新增组件
     */
    @PreAuthorize("@ss.hasPermi('system:unit:add')")
    @Log(title = "组件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TemplateUnit templateUnit)
    {
        return toAjax(templateUnitService.insertTemplateUnit(templateUnit));
    }

    /**
     * 修改组件
     */
    @PreAuthorize("@ss.hasPermi('system:unit:edit')")
    @Log(title = "组件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TemplateUnit templateUnit)
    {
        return toAjax(templateUnitService.updateTemplateUnit(templateUnit));
    }

    /**
     * 删除组件
     */
    @PreAuthorize("@ss.hasPermi('system:unit:remove')")
    @Log(title = "组件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(templateUnitService.deleteTemplateUnitByIds(ids));
    }
}
