package com.ruoyi.common.utils;

import java.lang.reflect.Field;

/**
 * @Author Lin
 * @Date 2023 03 10 11
 **/

public class ObjectComparator {
    //属性对比工具
    public String compareObjects(Object obj1, Object obj2) {
        String diffMessage = "";
        try {
            boolean isEqual = true;
            Class<?> clazz = obj1.getClass();
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                Object value1 = field.get(obj1);
                Object value2 = field.get(obj2);
                if (value1 == null |value2 ==null){
                    value1 = "无";
                    value2 = "无";
                }
                if (!value1.equals(value2)) {
                    diffMessage += "将[" + value1 + "]修改为：[" + value2 + "];";
                    isEqual = false;
                }
            }
            if (isEqual) {
                System.out.println("没有更改内容");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffMessage;
    }


}
