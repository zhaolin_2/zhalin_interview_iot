package com.ruoyi.common.utils.encoding;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @Author Lin
 * @Date 2022 10 27 14
 **/
public class MyEncoder extends MessageToByteEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, String s, ByteBuf byteBuf) throws Exception {
            //将16进制字符串转为数组
        byteBuf.writeBytes(hexString2Bytes(s));

    }


    public static byte[] hexString2Bytes(String src) {

        int l = src.length() / 2;

        byte[] ret = new byte[l];

        for (int i = 0; i < l; i++) {
            ret[i] = (byte) Integer.valueOf(src.substring(i * 2, i * 2 + 2), 16).byteValue();
        }
        return ret;
    }

    /**

     * @return 接收字节数据并转为16进制字符串

     */

    public static String receiveHexToString(byte[] by) {

        try {

            String str = bytes2Str(by);

            str = str.toLowerCase();

            return str;

        } catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("接收字节数据并转为16进制字符串异常");

        }

        return null;

    }

    /**

     * Convert byte[] to hex string.这里我们可以将byte转换成int

     * @param src byte[] data

     * @return hex string

     */

    public static String bytes2Str(byte[] src) {

        StringBuilder stringBuilder = new StringBuilder("");

        if (src == null || src.length <= 0) {

            return null;

        }

        for (int i = 0; i < src.length; i++) {

            int v = src[i] & 0xFF;

            String hv = Integer.toHexString(v);

            if (hv.length() < 2) {

                stringBuilder.append(0);

            }

            stringBuilder.append(hv);

        }

        return stringBuilder.toString();

    }



}
