package com.ruoyi.common.utils.imageUtils;

import net.coobird.thumbnailator.Thumbnails;

import java.io.IOException;

public class ImageUtil {

    public void ImageReduce(String imgSrc,String imgTarget) throws IOException {
        Thumbnails.of(imgSrc).
                scale(1f).outputQuality(0.2f).toFile(imgTarget);
    }


}


