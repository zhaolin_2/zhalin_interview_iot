package com.ruoyi.common.utils.math;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Lin
 * @Date 2022 10 31 18
 * 数学工具类
 **/
public class MathUtil {
    public Map<String,Double> getMaxAndMin(List<Double> list){
        Map<String,Double> maxAndminMap = new HashMap<>();
        double max  = list.get(0);
        double min =  list.get(1);
        for (int i = 0; i < list.size(); i++) {
            Double aDouble =  list.get(i);
            if (aDouble >  max){
                max = aDouble;
            }else if (aDouble < min){
                min= aDouble;
            }
        }
        maxAndminMap.put("最大值",max);
        maxAndminMap.put("最小值",min);
        return maxAndminMap;
    }
}
