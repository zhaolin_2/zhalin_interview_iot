package com.ruoyi.quartz.task;

import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.service.impl.*;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }

    /** 烟雾报警器轮询*/
    public void smokeDetectorAsk(){
        SmokeDetectorServiceImpl bean = SpringUtils.getBean(SmokeDetectorServiceImpl.class);
        bean.sendMessageForSmokeDetector();
    }

    /** 86液晶盒温湿度变送器*/
    public void officeHumitureAsk(){
        OfficeHumitureServiceImpl bean = SpringUtils.getBean(OfficeHumitureServiceImpl.class);
        bean.sendMessageForHumiture();
    }

    /** 电表电压电流 有功功率轮询*/
    public void electricityAUAPAsk(){
        ElectricityMeterServiceImpl bean = SpringUtils.getBean(ElectricityMeterServiceImpl.class);
        bean.sendMessageForAUAp();
    }

    /** 电表功率因数轮询*/
    public void electricityPEAsk(){
        ElectricityMeterServiceImpl bean = SpringUtils.getBean(ElectricityMeterServiceImpl.class);
        bean.sendMessageForPF();
    }

    /** 电表频率轮询*/
    public void electricityFrequencyAsk(){
        ElectricityMeterServiceImpl bean = SpringUtils.getBean(ElectricityMeterServiceImpl.class);
        bean.sendMessageForFrequency();
    }

    /** 电表频率轮询*/
    public void electricityAEAsk(){
        ElectricityMeterServiceImpl bean = SpringUtils.getBean(ElectricityMeterServiceImpl.class);
        bean.sendMessageForAE();
    }

    /** 电表开合闸状态轮询*/
    public void electricityOpenStateAsk(){
        ElectricityMeterServiceImpl bean = SpringUtils.getBean(ElectricityMeterServiceImpl.class);
        bean.sendMessageForOpenState();
    }

    /** 百叶箱集成式传感器温湿度轮询*/
    public void louverBoxHumitureAsk(){
        LouverBoxServiceImpl bean = SpringUtils.getBean(LouverBoxServiceImpl.class);
        bean.sendMessageForLouverBoxA();
    }

    /** 百叶箱集成式传感器光照轮询*/
    public void louverBoxIlluminationAsk(){
        LouverBoxServiceImpl bean = SpringUtils.getBean(LouverBoxServiceImpl.class);
        bean.sendMessageForLouverBoxB();
    }

    /** pt100 温度轮询*/
    public void ptTemperatureAsk(){
        PtDeviceServiceImpl bean = SpringUtils.getBean(PtDeviceServiceImpl.class);
        bean.sendMessageForHumiture();
    }

    /** 三项电表 电压 电流 有功功率轮询*/
    public void TElecMeterAEAPAsk(){
        TElectricityMeterServiceImpl bean = SpringUtils.getBean(TElectricityMeterServiceImpl.class);
        bean.sendMessageForThreeElectricity();
    }

    /** 三项电表有功电能轮询*/
    public void TElecMeterKWHAsk(){
        TElectricityMeterServiceImpl bean = SpringUtils.getBean(TElectricityMeterServiceImpl.class);
        bean.sendMessageForTKWH();
    }

    /** 数据报表生成*/
    public void reportGenerate(){
        OfficeReportServiceImpl bean = SpringUtils.getBean(OfficeReportServiceImpl.class);
        bean.reportGenerateDaily();
    }

    /** 文件生命周期减少*/
    public void fileLifeReduce(){
        FileDistributeServiceImpl bean = SpringUtils.getBean(FileDistributeServiceImpl.class);
        bean.reduceFileLife();
    }

    /**
     * 生产数量模拟
     */
    public void deviceCountSimulator(){
        DeviceCountServiceImpl bean = SpringUtils.getBean(DeviceCountServiceImpl.class);
        bean.deviceCountDataSimulation();
    }

    /**
     * 电费记录自动添加
     */
    public void electricityRateRecordGenerate(){
        ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
        bean.newDailyRateRecordGenerate();
    }






}
