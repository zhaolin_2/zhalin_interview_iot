package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报警信息对象 device_alert
 * 
 * @author Lin
 * @date 2022-11-23
 */
@AllArgsConstructor
@NoArgsConstructor
public class DeviceAlert extends BaseEntity
{
    /** id */
    private Long id;

    /** 报警设备 */
    @Excel(name = "报警设备")
    private String alertDevice;

    /** 报警名称 */
    @Excel(name = "报警名称")
    private String alertName;

    /** 报警数值 */
    @Excel(name = "报警数值")
    private String alertValue;

    /** 报警处理结果 */
    @Excel(name = "报警处理结果")
    private String alertResult;

    /** 报警时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报警时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date alertTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlertDevice() {
        return alertDevice;
    }

    public void setAlertDevice(String alertDevice) {
        this.alertDevice = alertDevice;
    }

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public String getAlertValue() {
        return alertValue;
    }

    public void setAlertValue(String alertValue) {
        this.alertValue = alertValue;
    }

    public String getAlertResult() {
        return alertResult;
    }

    public void setAlertResult(String alertResult) {
        this.alertResult = alertResult;
    }

    public Date getAlertTime() {
        return alertTime;
    }

    public void setAlertTime(Date alertTime) {
        this.alertTime = alertTime;
    }
}
