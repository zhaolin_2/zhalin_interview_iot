package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 点检对象 device_check
 * 
 * @author Lin
 * @date 2023-02-10
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceCheck extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 点检设备名称 */
    @Excel(name = "点检设备名称")
    private String deviceName;

    /** 设备位置 */
    @Excel(name = "设备位置")
    private String devicePosition;

    /** 点检位置 */
    @Excel(name = "点检位置")
    private String checkPosition;

    /** 点检人 */
    @Excel(name = "点检人")
    private String checkMan;

    /** 点检类型 */
    @Excel(name = "点检类型")
    private String checkType;

    /** 点检拍照1 */
    @Excel(name = "点检拍照1")
    private String checkPic1;

    /** 点检拍照2 */
    @Excel(name = "点检拍照2")
    private String checkPic2;

    /** 点检拍照3 */
    @Excel(name = "点检拍照3")
    private String checkPic3;

    /** 点检描述 */
    @Excel(name = "点检描述")
    private String checkMsg;

    /** 点检时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "点检时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

}
