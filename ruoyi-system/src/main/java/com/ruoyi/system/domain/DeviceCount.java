package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产计数对象 device_count
 * 
 * @author Lin
 * @date 2023-02-15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceCount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 设备名称 */
    private String deviceName;

    /** 生产计数 */
    private Long count;

    /** 当日生产总数 */
    private Long sumCount;

    /** 上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date recordTime;

}
