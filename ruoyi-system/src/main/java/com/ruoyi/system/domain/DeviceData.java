package com.ruoyi.system.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Lin
 * @Date 2022 11 17 15
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceData {

    /** 关联数据编号 */
    private String deviceNo;

    /**  数据解析 标识码*/
    private String dataCode;

    /** 待处理接收数据 通常是16 进制 */
    private String receivedData;

    /** 字符处理截取首位 */
    private Integer subHead;

    /** 字符处理 截取末尾 */
    private Integer subEnd;

    /** 数据类型：16进制 或 16进制单精度浮点型数字
     * true :16进制  Integer.parseInt 转换 字符串 ，再转换为Double
     *
     * false:16进制单精度浮点型 字符串转化用 new BigInteger 包裹，.intValue，再使用Float.intBitsToFloat 转化该结果
     *
     * */
    private boolean dataType;

    /** 数字保留小数点 */
    private Integer dataRound;

    /**
     *  数据参数
     *  说明 ：个别数据处理后，需要进行乘法或者除法操作，该参数为数据处理的使用参数
     *  */
    private Double dataArg;

    /**
     * 数据单位名称
     */
    private String dataUnit;

    /**
     *  申请数据轮询码
     */
    private String sendCode;

    /**
     * 数据轮询规则
     */
    private String cronExpression;

    public DeviceData(String receivedData, Integer subHead, Integer subEnd, boolean dataType, Integer dataRound, Double dataArg, String dataUnit) {
        this.receivedData = receivedData;
        this.subHead = subHead;
        this.subEnd = subEnd;
        this.dataType = dataType;
        this.dataRound = dataRound;
        this.dataArg = dataArg;
        this.dataUnit = dataUnit;
    }
}
