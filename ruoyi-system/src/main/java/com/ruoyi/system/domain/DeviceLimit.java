package com.ruoyi.system.domain;

import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 限制对象 device_limit
 * 
 * @author Lin
 * @date 2022-11-18
 */
@Data
public class DeviceLimit
{
    /** id */
    private Long id;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceNo;

    /** 检查项 */
    @Excel(name = "检查项")
    private String limitName;

    /** 报警上限 */
    @Excel(name = "报警上限")
    private Double limitUpper;

    /** 报警下限 */
    @Excel(name = "报警下限")
    private Double limitLower;

    public String limitCheckValue;

    public String deviceState;

}
