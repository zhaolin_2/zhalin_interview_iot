package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 电表对象 electricity_meter
 * 
 * @author Lin
 * @date 2022-11-07
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ElectricityMeter
{
    private static final long serialVersionUID = 1L;

    /** id主键 */
    private Integer id;

    /** 电流 */
    @Excel(name = "电流")
    private Double emA;

    /** 电压 */
    @Excel(name = "电压")
    private Double emU;

    /** 有功功率 */
    @Excel(name = "有功功率")
    private Double emActivePower;

    /** 功率因数 */
    @Excel(name = "功率因数")
    private Double emPowerFactor;

    /** 频率 */
    @Excel(name = "频率")
    private Double emFrequency;

    /** 总有功电量 */
    @Excel(name = "总有功电量")
    private Double emActiveElectricity;

    /** 拉合闸状态 */
    @Excel(name = "拉合闸状态")
    private Integer emState;

    /** 监测时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "监测时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;


}
