package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 电费对象 electricity_rate
 * 
 * @author Lin
 * @date 2023-03-03
 */
@AllArgsConstructor
@NoArgsConstructor
public class ElectricityRate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 尖峰电费 */
    private Double electricityRateTop;

    /** 峰值电费 */
    private Double electricityRateHigh;

    /** 平值电费 */
    private Double electricityRateCommon;

    /** 谷值电费 */
    private Double electricityRateLow;

    /** 电费总值 */
    private Double electricityRateAll;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date recordTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getElectricityRateTop() {
        return electricityRateTop;
    }

    public void setElectricityRateTop(Double electricityRateTop) {
        this.electricityRateTop = electricityRateTop;
    }

    public Double getElectricityRateHigh() {
        return electricityRateHigh;
    }

    public void setElectricityRateHigh(Double electricityRateHigh) {
        this.electricityRateHigh = electricityRateHigh;
    }

    public Double getElectricityRateCommon() {
        return electricityRateCommon;
    }

    public void setElectricityRateCommon(Double electricityRateCommon) {
        this.electricityRateCommon = electricityRateCommon;
    }

    public Double getElectricityRateLow() {
        return electricityRateLow;
    }

    public void setElectricityRateLow(Double electricityRateLow) {
        this.electricityRateLow = electricityRateLow;
    }

    public Double getElectricityRateAll() {
        return electricityRateAll;
    }

    public void setElectricityRateAll(Double electricityRateAll) {
        this.electricityRateAll = electricityRateAll;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }
}
