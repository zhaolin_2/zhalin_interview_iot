package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件分发Demo对象 file_distribute
 * 
 * @author Lin
 * @date 2022-11-21
 */
@Data
public class FileDistribute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分发人 */
    @Excel(name = "分发人")
    private String fileDistributor;

    /** 接收人 */
    @Excel(name = "接收人")
    private String fileReceived;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fileName;

    /** 文件上传日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件上传日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fileUploadTime;

    /** 文件存放剩余时间(天) */
    @Excel(name = "文件存放剩余时间(天)")
    private Integer fileLife;


}
