package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 百叶箱对象 louver_box
 * 
 * @author Lin
 * @date 2022-11-04
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LouverBox extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Integer id;

    /** 温度(℃) */
    @Excel(name = "温度(℃)")
    private Double lbTemperature;

    /** 湿度(%) */
    @Excel(name = "湿度(%)")
    private Double lbHumidity;

    /** 光照强度(LX) */
    @Excel(name = "光照强度(LX)")
    private Long lbIllumination;

    /** 监测时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "监测时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

}
