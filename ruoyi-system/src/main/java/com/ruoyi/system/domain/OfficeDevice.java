package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备对象 office_device
 * 
 * @author Lin
 * @date 2022-11-18
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OfficeDevice extends BaseEntity
{

    /** id */
    private Integer id;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String deviceNo;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 设备状态 */
    @Excel(name = "设备状态")
    private Long deviceState;

    /** 设备负责人 */
    @Excel(name = "设备负责人")
    private String deviceManager;

    /** 设备登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "设备登记时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registerTime;

    /** 设备地图地址 */
    @Excel(name = "设备地图地址")
    private String deviceAddress;

    private String deviceRouter;
}
