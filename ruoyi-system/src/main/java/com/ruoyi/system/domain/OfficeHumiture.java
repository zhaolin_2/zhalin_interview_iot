package com.ruoyi.system.domain;

import java.math.BigInteger;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * humiture对象 office_humiture
 * 
 * @author Lin
 * @date 2022-10-31
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OfficeHumiture
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 温度(℃) */
    @Excel(name = "温度(℃)")
    private Double officeTemperature;

    /** 湿度(%) */
    @Excel(name = "湿度(%)")
    private Double officeHumidity;

    /** 最后上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后上传时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;


}
