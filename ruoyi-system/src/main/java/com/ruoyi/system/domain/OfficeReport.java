package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 报表对象 office_report
 * 
 * @author Allin
 * @date 2022-11-10
 */

public class OfficeReport
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 86液晶盒温度最高 */
    @Excel(name = "86液晶盒温度最高")
    private Double oTempMax;

    /** 86液晶盒温度最低 */
    @Excel(name = "86液晶盒温度最低")
    private Double oTempMin;

    /** 温度均值 */
    @Excel(name = "温度均值")
    private Double oTempAvg;

    /** 86液晶盒湿度最高 */
    @Excel(name = "86液晶盒湿度最高")
    private Double oHumiMax;

    /** 86液晶盒湿度最低 */
    @Excel(name = "86液晶盒湿度最低")
    private Double oHumiMin;

    /** 湿度均值 */
    @Excel(name = "湿度均值")
    private Double oHumiAvg;

    /** 电流峰值 */
    @Excel(name = "电流峰值")
    private Double oAmax;

    /** 电流最低值 */
    @Excel(name = "电流最低值")
    private Double oAmin;

    /** 当日总能耗 */
    @Excel(name = "当日总能耗")
    private Double oApAll;

    /** 烟雾报警器报警次数 */
    @Excel(name = "烟雾报警器报警次数")
    private Integer oDtcCount;

    /** 光照强度峰值 */
    @Excel(name = "光照强度峰值")
    private Double oIllMax;

    /** 光照强度最低值 */
    @Excel(name = "光照强度最低值")
    private Double oIllMin;

    /** 光照强度均值 */
    @Excel(name = "光照强度均值")
    private Double oIllAvg;

    /** 三项电表能耗查询 */
    @Excel(name = "三项电表能耗查询")
    private Double tApAll;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    /** 产量 */
    private Long output;

    /** 产能与能耗比 */
    private Double proportion;

    public Double getProportion() {
        return proportion;
    }

    public void setProportion(Double proportion) {
        this.proportion = proportion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getoTempMax() {
        return oTempMax;
    }

    public void setoTempMax(Double oTempMax) {
        this.oTempMax = oTempMax;
    }

    public Double getoTempMin() {
        return oTempMin;
    }

    public void setoTempMin(Double oTempMin) {
        this.oTempMin = oTempMin;
    }

    public Double getoTempAvg() {
        return oTempAvg;
    }

    public void setoTempAvg(Double oTempAvg) {
        this.oTempAvg = oTempAvg;
    }

    public Double getoHumiMax() {
        return oHumiMax;
    }

    public void setoHumiMax(Double oHumiMax) {
        this.oHumiMax = oHumiMax;
    }

    public Double getoHumiMin() {
        return oHumiMin;
    }

    public void setoHumiMin(Double oHumiMin) {
        this.oHumiMin = oHumiMin;
    }

    public Double getoHumiAvg() {
        return oHumiAvg;
    }

    public void setoHumiAvg(Double oHumiAvg) {
        this.oHumiAvg = oHumiAvg;
    }

    public Double getoAmax() {
        return oAmax;
    }

    public void setoAmax(Double oAmax) {
        this.oAmax = oAmax;
    }

    public Double getoAmin() {
        return oAmin;
    }

    public void setoAmin(Double oAmin) {
        this.oAmin = oAmin;
    }

    public Double getoApAll() {
        return oApAll;
    }

    public void setoApAll(Double oApAll) {
        this.oApAll = oApAll;
    }

    public Integer getoDtcCount() {
        return oDtcCount;
    }

    public void setoDtcCount(Integer oDtcCount) {
        this.oDtcCount = oDtcCount;
    }

    public Double getoIllMax() {
        return oIllMax;
    }

    public void setoIllMax(Double oIllMax) {
        this.oIllMax = oIllMax;
    }

    public Double getoIllMin() {
        return oIllMin;
    }

    public void setoIllMin(Double oIllMin) {
        this.oIllMin = oIllMin;
    }

    public Double getoIllAvg() {
        return oIllAvg;
    }

    public void setoIllAvg(Double oIllAvg) {
        this.oIllAvg = oIllAvg;
    }

    public Double gettApAll() {
        return tApAll;
    }

    public void settApAll(Double tApAll) {
        this.tApAll = tApAll;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Long getOutput() {
        return output;
    }

    public void setOutput(Long output) {
        this.output = output;
    }
}
