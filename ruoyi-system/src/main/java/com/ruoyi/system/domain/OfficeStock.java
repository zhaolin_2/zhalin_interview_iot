package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存对象 office_stock
 * 
 * @author lin
 * @date 2022-12-06
 */
@Data
public class OfficeStock extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 库存名称
     */
    @Excel(name = "库存名称")
    private String stockName;

    /**
     * 硬件或产品型号
     */
    @Excel(name = "硬件或产品型号")
    private String stockType;

    /**
     * 封装
     */
    @Excel(name = "封装")
    private String stockPackage;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String stockManager;

    /**
     * 库存数量
     */
    @Excel(name = "库存数量")
    private Long stockNum;

    /**
     * 上级存放位置
     */
    @Excel(name = "上级存放位置")
    private String stockPositionA;

    /**
     * 下级存放位置
     */
    @Excel(name = "下级存放位置")
    private String stockPositionB;

    /**
     * 最初入库时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最初入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    /**
     * 库存类别，区别与型号
     */
    @Excel(name = "库存类别")
    private String stockClass;

    /**
     * 采购提示上限
     */
    private Integer limitMax;

    /**
     * 采购提示下限
     */
    private Integer limitDown;

    /**
     * 供应商
     */
    private String stockSup;

    /**
     * 库存图片
     */
    @Excel(name = "图片")
    private String stockImg;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String stockMsg;

}


