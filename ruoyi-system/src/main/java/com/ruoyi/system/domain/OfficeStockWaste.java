package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 废弃品对象 office_stock
 * 
 * @author lin
 * @date 2022-12-06
 */
@Data
public class OfficeStockWaste
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 库存名称 */
    private String stockName;

    /** 硬件或产品型号 */
    private String stockType;

    /** 封装 */
    private String stockPackage;

    /** 负责人 */
    private String stockManager;

    /** 数量 */
    private Long stockNum;

    /** 上级存放位置 */
    private String stockPositionA;

    /** 下级存放位置 */
    private String stockPositionB;

    /** 废弃时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date recordTime;

    /** 库存类别，区别与型号 */
    private String stockClass;

    /** 库存图片 */
    private String temp1;

    /** 备注 */
    private String temp2;


}
