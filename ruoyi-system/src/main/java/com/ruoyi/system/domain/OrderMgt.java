package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单管理对象 order_mgt
 * 
 * @author Lin
 * @date 2023-02-10
 */
@Data
public class OrderMgt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String orderName;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderCode;

    /** 订单类型 */
    @Excel(name = "订单类型")
    private String orderType;

    /** 所属项目 */
    @Excel(name = "所属项目")
    private String orderProject;

    /** 关联客户 */
    @Excel(name = "关联客户")
    private String orderCustomer;

    /** 签订时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "签订时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderSignTime;

    /** 签单人(销售员) */
    @Excel(name = "签单人(销售员)")
    private String orderSignMan;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderStartTime;

    /** 订单审核状态 */
    @Excel(name = "订单审核状态")
    private String orderCheckState;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderState;

}
