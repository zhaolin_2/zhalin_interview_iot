package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 书画管理对象 painting_mgt
 * 
 * @author Lin
 * @date 2023-02-15
 */
public class PaintingMgt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 作品码 */
    @Excel(name = "作品码")
    private String paintingCode;

    /** 作品名称 */
    @Excel(name = "作品名称")
    private String paintingName;

    /** 作者 */
    @Excel(name = "作者")
    private String paintingAuthor;

    /** 作品持有人 */
    @Excel(name = "作品持有人")
    private String paintingHolder;

    /** 最后交易时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后交易时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paintingLastTradeTime;

    /** 作品图片 */
    @Excel(name = "作品图片")
    private String paintingImg;

    /** 作品交易价格 */
    @Excel(name = "作品交易价格")
    private Double paintingPrice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPaintingCode(String paintingCode) 
    {
        this.paintingCode = paintingCode;
    }

    public String getPaintingCode() 
    {
        return paintingCode;
    }
    public void setPaintingName(String paintingName) 
    {
        this.paintingName = paintingName;
    }

    public String getPaintingName() 
    {
        return paintingName;
    }
    public void setPaintingAuthor(String paintingAuthor) 
    {
        this.paintingAuthor = paintingAuthor;
    }

    public String getPaintingAuthor() 
    {
        return paintingAuthor;
    }
    public void setPaintingHolder(String paintingHolder) 
    {
        this.paintingHolder = paintingHolder;
    }

    public String getPaintingHolder() 
    {
        return paintingHolder;
    }
    public void setPaintingLastTradeTime(Date paintingLastTradeTime) 
    {
        this.paintingLastTradeTime = paintingLastTradeTime;
    }

    public Date getPaintingLastTradeTime() 
    {
        return paintingLastTradeTime;
    }
    public void setPaintingImg(String paintingImg) 
    {
        this.paintingImg = paintingImg;
    }

    public String getPaintingImg() 
    {
        return paintingImg;
    }
    public void setPaintingPrice(Double paintingPrice) 
    {
        this.paintingPrice = paintingPrice;
    }

    public Double getPaintingPrice() 
    {
        return paintingPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("paintingCode", getPaintingCode())
            .append("paintingName", getPaintingName())
            .append("paintingAuthor", getPaintingAuthor())
            .append("paintingHolder", getPaintingHolder())
            .append("paintingLastTradeTime", getPaintingLastTradeTime())
            .append("paintingImg", getPaintingImg())
            .append("paintingPrice", getPaintingPrice())
            .toString();
    }
}
