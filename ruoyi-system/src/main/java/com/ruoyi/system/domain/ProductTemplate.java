package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模板对象 product_template
 * 
 * @author lin
 * @date 2022-12-07
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductTemplate
{
    /** 模板id */
    private Long id;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String templateName;

    /** 创建者 */
    @Excel(name = "创建者")
    private String templateManager;

    /** 产品型号 */
    @Excel(name = "产品型号")
    private String templateType;

    /** 说明 */
    @Excel(name = "说明")
    private String templateMessage;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;


}
