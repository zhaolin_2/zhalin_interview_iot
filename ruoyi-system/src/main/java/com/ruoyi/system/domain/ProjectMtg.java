package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 项目管理对象 project_mtg
 * 
 * @author Lin
 * @date 2023-02-22
 */
public class ProjectMtg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目年度 */
    @Excel(name = "项目年度")
    private String projectA;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectB;

    /** 项目投资方 */
    @Excel(name = "项目投资方")
    private String projectC;

    /** 责任单位 */
    @Excel(name = "责任单位")
    private String projectD;

    /** 相关责任单位 */
    @Excel(name = "相关责任单位")
    private String projectE;

    /** 区领导 */
    @Excel(name = "区领导")
    private String projectF;

    /** 项目类别 */
    @Excel(name = "项目类别")
    private String projectG;

    /** 项目当期阶段 */
    @Excel(name = "项目当期阶段")
    private String projectH;

    /** 产业领域-大类 */
    @Excel(name = "产业领域-大类")
    private String projectI;

    /** 产业领域-小类 */
    @Excel(name = "产业领域-小类")
    private String projectJ;

    /** 行业类别 */
    @Excel(name = "行业类别")
    private String projectK;

    /** 项目总投资查询 */
    @Excel(name = "项目总投资查询")
    private String projectL;

    /** 资金区间起 */
    @Excel(name = "资金区间起")
    private String projectM;

    /** 资金区间止 */
    @Excel(name = "资金区间止")
    private String projectN;

    /** 投资类别 */
    @Excel(name = "投资类别")
    private String projectO;

    /** 附件 */
    @Excel(name = "附件")
    private String projectP;

    /** 是否督办 */
    @Excel(name = "是否督办")
    private String projectQ;

    /** 是否调度 */
    @Excel(name = "是否调度")
    private String projectR;

    /** 是否分拨 */
    @Excel(name = "是否分拨")
    private String projectS;

    /** 项目审核状态 */
    @Excel(name = "项目审核状态")
    private String projectT;

    /** 项目进度状态 */
    @Excel(name = "项目进度状态")
    private String projectU;

    /** 是否终止 */
    @Excel(name = "是否终止")
    private String projectV;

    /** 是否储备 */
    @Excel(name = "是否储备")
    private String projectW;

    /** 是否参与考核 */
    @Excel(name = "是否参与考核")
    private String projectX;

    /** 是否专报项目 */
    @Excel(name = "是否专报项目")
    private String projectY;

    /** 是否履约监管 */
    @Excel(name = "是否履约监管")
    private String projectZ;

    /** 履约监管状态 */
    @Excel(name = "履约监管状态")
    private String projectAb;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date projectAc;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectA(String projectA) 
    {
        this.projectA = projectA;
    }

    public String getProjectA() 
    {
        return projectA;
    }
    public void setProjectB(String projectB) 
    {
        this.projectB = projectB;
    }

    public String getProjectB() 
    {
        return projectB;
    }
    public void setProjectC(String projectC) 
    {
        this.projectC = projectC;
    }

    public String getProjectC() 
    {
        return projectC;
    }
    public void setProjectD(String projectD) 
    {
        this.projectD = projectD;
    }

    public String getProjectD() 
    {
        return projectD;
    }
    public void setProjectE(String projectE) 
    {
        this.projectE = projectE;
    }

    public String getProjectE() 
    {
        return projectE;
    }
    public void setProjectF(String projectF) 
    {
        this.projectF = projectF;
    }

    public String getProjectF() 
    {
        return projectF;
    }
    public void setProjectG(String projectG) 
    {
        this.projectG = projectG;
    }

    public String getProjectG() 
    {
        return projectG;
    }
    public void setProjectH(String projectH) 
    {
        this.projectH = projectH;
    }

    public String getProjectH() 
    {
        return projectH;
    }
    public void setProjectI(String projectI) 
    {
        this.projectI = projectI;
    }

    public String getProjectI() 
    {
        return projectI;
    }
    public void setProjectJ(String projectJ) 
    {
        this.projectJ = projectJ;
    }

    public String getProjectJ() 
    {
        return projectJ;
    }
    public void setProjectK(String projectK) 
    {
        this.projectK = projectK;
    }

    public String getProjectK() 
    {
        return projectK;
    }
    public void setProjectL(String projectL) 
    {
        this.projectL = projectL;
    }

    public String getProjectL() 
    {
        return projectL;
    }
    public void setProjectM(String projectM) 
    {
        this.projectM = projectM;
    }

    public String getProjectM() 
    {
        return projectM;
    }
    public void setProjectN(String projectN) 
    {
        this.projectN = projectN;
    }

    public String getProjectN() 
    {
        return projectN;
    }
    public void setProjectO(String projectO) 
    {
        this.projectO = projectO;
    }

    public String getProjectO() 
    {
        return projectO;
    }
    public void setProjectP(String projectP) 
    {
        this.projectP = projectP;
    }

    public String getProjectP() 
    {
        return projectP;
    }
    public void setProjectQ(String projectQ) 
    {
        this.projectQ = projectQ;
    }

    public String getProjectQ() 
    {
        return projectQ;
    }
    public void setProjectR(String projectR) 
    {
        this.projectR = projectR;
    }

    public String getProjectR() 
    {
        return projectR;
    }
    public void setProjectS(String projectS) 
    {
        this.projectS = projectS;
    }

    public String getProjectS() 
    {
        return projectS;
    }
    public void setProjectT(String projectT) 
    {
        this.projectT = projectT;
    }

    public String getProjectT() 
    {
        return projectT;
    }
    public void setProjectU(String projectU) 
    {
        this.projectU = projectU;
    }

    public String getProjectU() 
    {
        return projectU;
    }
    public void setProjectV(String projectV) 
    {
        this.projectV = projectV;
    }

    public String getProjectV() 
    {
        return projectV;
    }
    public void setProjectW(String projectW) 
    {
        this.projectW = projectW;
    }

    public String getProjectW() 
    {
        return projectW;
    }
    public void setProjectX(String projectX) 
    {
        this.projectX = projectX;
    }

    public String getProjectX() 
    {
        return projectX;
    }
    public void setProjectY(String projectY) 
    {
        this.projectY = projectY;
    }

    public String getProjectY() 
    {
        return projectY;
    }
    public void setProjectZ(String projectZ) 
    {
        this.projectZ = projectZ;
    }

    public String getProjectZ() 
    {
        return projectZ;
    }
    public void setProjectAb(String projectAb) 
    {
        this.projectAb = projectAb;
    }

    public String getProjectAb() 
    {
        return projectAb;
    }
    public void setProjectAc(Date projectAc) 
    {
        this.projectAc = projectAc;
    }

    public Date getProjectAc() 
    {
        return projectAc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectA", getProjectA())
            .append("projectB", getProjectB())
            .append("projectC", getProjectC())
            .append("projectD", getProjectD())
            .append("projectE", getProjectE())
            .append("projectF", getProjectF())
            .append("projectG", getProjectG())
            .append("projectH", getProjectH())
            .append("projectI", getProjectI())
            .append("projectJ", getProjectJ())
            .append("projectK", getProjectK())
            .append("projectL", getProjectL())
            .append("projectM", getProjectM())
            .append("projectN", getProjectN())
            .append("projectO", getProjectO())
            .append("projectP", getProjectP())
            .append("projectQ", getProjectQ())
            .append("projectR", getProjectR())
            .append("projectS", getProjectS())
            .append("projectT", getProjectT())
            .append("projectU", getProjectU())
            .append("projectV", getProjectV())
            .append("projectW", getProjectW())
            .append("projectX", getProjectX())
            .append("projectY", getProjectY())
            .append("projectZ", getProjectZ())
            .append("projectAb", getProjectAb())
            .append("projectAc", getProjectAc())
            .toString();
    }
}
