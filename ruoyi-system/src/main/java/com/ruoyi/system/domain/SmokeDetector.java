package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报警器对象 smoke_detector
 * 
 * @author Lin
 * @date 2022-11-04
 */
public class SmokeDetector extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Integer id;

    /** 报警器状态，0正常，1报警 */
    @Excel(name = "报警器状态，0正常，1报警")
    private Integer detectorState;

    /** 监测时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "监测时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setDetectorState(Integer detectorState) 
    {
        this.detectorState = detectorState;
    }

    public Integer getDetectorState() 
    {
        return detectorState;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("detectorState", getDetectorState())
            .append("recordTime", getRecordTime())
            .toString();
    }
}
