package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存详情对象 stock_message
 * 
 * @author Lin
 * @date 2022-11-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 库存id */
    @Excel(name = "库存id")
    private Long stockId;

    /** 操作人 */
    @Excel(name = "操作人")
    private String handleMan;

    /** 操作类型(出库、入库) */
    @Excel(name = "操作类型(出库、入库)")
    private String handleType;

    private Long handleNum;

    /** 操作备注(出入库理由) */
    @Excel(name = "操作备注(出入库理由)")
    private String handleMessage;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date handleTime;


}
