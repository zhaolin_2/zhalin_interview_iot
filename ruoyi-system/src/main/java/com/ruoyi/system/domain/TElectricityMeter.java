package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 三项电表对象 t_electricity_meter
 * 
 * @author Lin
 * @date 2022-11-09
 */
@Data
public class TElectricityMeter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** A项电流 */
    @Excel(name = "A项电流")
    private Double ia;

    /** B项电流 */
    @Excel(name = "B项电流")
    private Double ib;

    /** C项电流 */
    @Excel(name = "C项电流")
    private Double ic;

    /** A项电压 */
    @Excel(name = "A项电压")
    private Double ua;

    /** B项电压 */
    @Excel(name = "B项电压")
    private Double ub;

    /** C项电压 */
    @Excel(name = "C项电压")
    private Double uc;

    /** 总有功功率 */
    @Excel(name = "总有功功率")
    private Double apAll;

    /** A项有功功率 */
    @Excel(name = "A项有功功率")
    private Double apA;

    /** B项有功功率 */
    @Excel(name = "B项有功功率")
    private Double apB;

    /** C项有功功率 */
    @Excel(name = "C项有功功率")
    private Double apC;

    /**总有功电能 */
    private Double aeAll;

    /** 数据上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "数据上传时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;

}
