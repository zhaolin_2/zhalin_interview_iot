package com.ruoyi.system.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 组件对象 template_unit
 * 
 * @author lin
 * @date 2022-12-07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateUnit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 模板id */
    @Excel(name = "模板id")
    private Long templateId;

    /** 组件id */
    @Excel(name = "组件id")
    private Long stockId;

    /** 组件数量 */
    @Excel(name = "组件数量")
    private Long stockNum;


}
