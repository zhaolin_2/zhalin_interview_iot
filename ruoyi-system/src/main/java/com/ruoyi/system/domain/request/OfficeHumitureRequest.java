package com.ruoyi.system.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @Author Lin
 * @Date 2022 10 31 17
 **/

@Data
public class OfficeHumitureRequest {

    /** 温度(℃) */
    private Double officeTemperature;

    /** 湿度(%) */
    private Double officeHumidity;

    /** 最后上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;

    /**最大温度*/
    private Double maxTemperature;

    /**最低温度*/
    private Double minTemperature;

    /**最大湿度*/
    private Double maxOfficeHumidity;

    /**最低湿度*/
    private Double minOfficeHumidity;

    /**平均温度*/
    private Double averageTemperature;

    /**平均湿度*/
    private Double averageHumidity;


}
