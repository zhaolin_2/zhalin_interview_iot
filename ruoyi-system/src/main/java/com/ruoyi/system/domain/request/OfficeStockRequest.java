package com.ruoyi.system.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * @Author Lin
 * @Date 2022 12 09 16
 **/
@Data
public class OfficeStockRequest {

        /**
         * id
         */
        private Long id;

        /**
         * 库存名称
         */
        private String stockName;

        /**
         * 硬件或产品型号
         */
        private String stockType;

        /**
         * 封装
         */
        private String stockPackage;

        /**
         * 上级存放位置
         */
        private String stockPositionA;

        /**
         * 下级存放位置
         */
        private String stockPositionB;

        /**
         * 配件需求总量
         */
        private Long stockSum;


        /**
         * 库存剩余
         */
        private Long stockNum;

        /**
         * 组装后的剩余数量
         */
        private Long assembledStockNum;


}
