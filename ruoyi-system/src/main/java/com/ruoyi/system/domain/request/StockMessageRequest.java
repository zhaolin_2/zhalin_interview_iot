package com.ruoyi.system.domain.request;

import lombok.Data;

/**
 * @Author Lin
 * @Date 2022 11 30 18
 **/
@Data
public class StockMessageRequest {
    private  Long id;
    private Long num;
    private String message;
}
