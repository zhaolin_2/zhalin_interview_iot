package com.ruoyi.system.domain.request;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 返回前端的组件列表
 * 
 * @author lin
 * @date 2022-12-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateUnitRequest
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 库存名称 */
    private String stockName;

    /** 硬件或产品型号 */
    private String stockType;

    /** 封装 */
    private String stockPackage;

    /** 组件数量 */
    private Long stockNum;

    /**
     * 库存id  唯一
     */
    private Long stockId;

}
