package com.ruoyi.system.domain.request;

import lombok.Data;

/**
 * @Author Lin
 * @Date 2022 12 07 18
 * 给产品模板用于增删改参数传递的 实体类
 **/
@Data
public class TemplateUnitUpdateRequest {
    private Long templateId;
    private Long stockId;
    private Long num;
}
