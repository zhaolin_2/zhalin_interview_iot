package com.ruoyi.system.domain.request.dataIntegration;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author Lin
 * @Date 2023 02 20 15
 * 设备最新返回数据整合返回类
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceLatestDataRequest {

    private String deviceDataName;//设备采集项

    private String deviceData;//采集数据

    @JsonFormat(pattern = "HH:mm:ss")
    private Date latestRecordTime;//数据最后采集时间

    private String dateState;// 数据状态：  正常；未超时  异常；超时
}
