package com.ruoyi.system.domain.request.dataIntegration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Lin
 * @Date 2023 02 16 16
 * 数据整合页面：设备在线状态
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceStateRequest {

    //设备总数
    private Integer DeviceCounts;

    //设备在线数
    private Integer DeviceOnlineCounts;

    //设备运行状态
    private String DeviceRunningState;

}
