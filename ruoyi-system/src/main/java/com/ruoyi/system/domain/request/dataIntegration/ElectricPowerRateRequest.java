package com.ruoyi.system.domain.request.dataIntegration;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.ElectricityRate;
import lombok.Data;

import java.util.Date;

/**
 * @Author Lin
 * @Date 2023 03 03 09
 * 数据整合页面 电费统计功能实体类
 **/
@Data
public class ElectricPowerRateRequest{

    /** 当前电费 计算类型 ： 尖峰 峰值 平值 谷值 */
    private String electricityRateType;

    /** 当前电费价格 */
    private Double electricityRate;

    /** 本日能耗 */
    private Double ActiveElectricity;

    /** 尖峰电费 */
    private Double electricityRateTop;

    /** 峰值电费 */
    private Double electricityRateHigh;

    /** 平值电费 */
    private Double electricityRateCommon;

    /** 谷值电费 */
    private Double electricityRateLow;

    /** 电费总值 */
    private Double electricityRateAll;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date recordTime;
}
