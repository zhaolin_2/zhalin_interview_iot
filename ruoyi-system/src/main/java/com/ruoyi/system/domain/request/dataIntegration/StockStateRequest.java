package com.ruoyi.system.domain.request.dataIntegration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Lin
 * @Date 2023 02 16 16
 * 数据整合页面：库存状态
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockStateRequest {

    //设备总数
    private Integer stockSumNum;

    //设备在线数
    private Integer stockWarnNum;


}
