package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DeviceAlert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报警信息Mapper接口
 * 
 * @author Lin
 * @date 2022-11-23
 */
@Mapper
public interface DeviceAlertMapper 
{
    /**
     * 查询报警信息
     * 
     * @param id 报警信息主键
     * @return 报警信息
     */
    public DeviceAlert selectDeviceAlertById(Long id);

    /**
     * 查询报警信息列表
     * 
     * @param deviceAlert 报警信息
     * @return 报警信息集合
     */
    public List<DeviceAlert> selectDeviceAlertList(DeviceAlert deviceAlert);

    /**
     * 新增报警信息
     * 
     * @param deviceAlert 报警信息
     * @return 结果
     */
    public int insertDeviceAlert(DeviceAlert deviceAlert);

    /**
     * 修改报警信息
     * 
     * @param deviceAlert 报警信息
     * @return 结果
     */
    public int updateDeviceAlert(DeviceAlert deviceAlert);

    /**
     * 删除报警信息
     * 
     * @param id 报警信息主键
     * @return 结果
     */
    public int deleteDeviceAlertById(Long id);

    /**
     * 批量删除报警信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDeviceAlertByIds(Long[] ids);
}
