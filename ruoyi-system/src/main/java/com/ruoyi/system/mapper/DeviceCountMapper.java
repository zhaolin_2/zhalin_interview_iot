package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DeviceCount;
import org.apache.ibatis.annotations.Mapper;

/**
 * 生产计数Mapper接口
 * 
 * @author Lin
 * @date 2023-02-15
 */
@Mapper
public interface DeviceCountMapper 
{
    /**
     * 查询生产计数
     * 
     * @param id 生产计数主键
     * @return 生产计数
     */
    public DeviceCount selectDeviceCountById(Long id);

    /**
     * 查询生产计数列表
     * 
     * @param deviceCount 生产计数
     * @return 生产计数集合
     */
    public List<DeviceCount> selectDeviceCountList(DeviceCount deviceCount);

    /**
     * 新增生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    public int insertDeviceCount(DeviceCount deviceCount);

    /**
     * 修改生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    public int updateDeviceCount(DeviceCount deviceCount);

    /**
     * 删除生产计数
     * 
     * @param id 生产计数主键
     * @return 结果
     */
    public int deleteDeviceCountById(Long id);

    /**
     * 批量删除生产计数
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDeviceCountByIds(Long[] ids);

    /**
     * 根据时间查询生产数量的最新一条数据
     * @param selectDate
     * @return
     */
    public DeviceCount selectLastCountByDate(String selectDate);
}
