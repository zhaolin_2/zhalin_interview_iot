package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DeviceLimit;
import org.apache.ibatis.annotations.Mapper;

/**
 * 限制Mapper接口
 * 
 * @author Lin
 * @date 2022-11-18
 */
@Mapper
public interface DeviceLimitMapper 
{
    /**
     * 查询限制
     * 
     * @param id 限制主键
     * @return 限制
     */
    public DeviceLimit selectDeviceLimitById(Long id);

    /**
     * 查询限制列表
     * 
     * @param deviceLimit 限制
     * @return 限制集合
     */
    public List<DeviceLimit> selectDeviceLimitList(DeviceLimit deviceLimit);


    public List<DeviceLimit> selectDeviceLimitListByIdAndName(DeviceLimit deviceLimit);

    /**
     * 新增限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    public int insertDeviceLimit(DeviceLimit deviceLimit);

    /**
     * 修改限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    public int updateDeviceLimit(DeviceLimit deviceLimit);

    /**
     * 删除限制
     * 
     * @param id 限制主键
     * @return 结果
     */
    public int deleteDeviceLimitById(Long id);

    /**
     * 批量删除限制
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDeviceLimitByIds(Long[] ids);

    /** 根据数据对比字段查询限制*/
    public DeviceLimit selectDeviceLimitByCheckValue(String deviceCheckValue);

}
