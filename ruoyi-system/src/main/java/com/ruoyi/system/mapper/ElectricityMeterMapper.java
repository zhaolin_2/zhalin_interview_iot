package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.OfficeReport;
import org.apache.ibatis.annotations.Mapper;

/**
 * 电表Mapper接口
 * 
 * @author Lin
 * @date 2022-11-07
 */
@Mapper
public interface ElectricityMeterMapper 
{
    /**
     * 查询电表
     * 
     * @param id 电表主键
     * @return 电表
     */
    public ElectricityMeter selectElectricityMeterById(Integer id);

    /**
     * 查询电表列表
     * 
     * @param electricityMeter 电表
     * @return 电表集合
     */
    public List<ElectricityMeter> selectElectricityMeterList(ElectricityMeter electricityMeter);

    /**
     * 新增电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    public int insertElectricityMeter(ElectricityMeter electricityMeter);

    /**
     * 修改电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    public int updateElectricityMeter(ElectricityMeter electricityMeter);

    /**
     * 删除电表
     * 
     * @param id 电表主键
     * @return 结果
     */
    public int deleteElectricityMeterById(Integer id);

    /**
     * 批量删除电表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteElectricityMeterByIds(Integer[] ids);

    /**
     * 查询 电流 有功电量 曲线的方法
     */
    public List<ElectricityMeter> selectElectricityListByTime(String selectTime);

    /**
     * 根据时间查询 参数日期 的 最新一条电表采集的数据
     * 这条方法的前身不需要参数
     */
    public ElectricityMeter selectLastElectricityByDate(String selectDate);

    public OfficeReport rangeOfElectricityData(String selectDate);

    /**
     * 查询最新一套电表数据
     * @return
     */
    public ElectricityMeter selectLatestElectricityMeter();

    /**
     * 根据小时查询某一小时的数据
     */
    ElectricityMeter selectElectricityMeterDataByHour(String selectHourTime);



}
