package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ElectricityRate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * 电费Mapper接口
 * 
 * @author Lin
 * @date 2023-03-03
 */
@Mapper
public interface ElectricityRateMapper 
{
    /**
     * 查询电费
     * 
     * @param id 电费主键
     * @return 电费
     */
    public ElectricityRate selectElectricityRateById(Long id);

    /**
     * 查询电费列表
     * 
     * @param electricityRate 电费
     * @return 电费集合
     */
    public List<ElectricityRate> selectElectricityRateList(ElectricityRate electricityRate);

    /**
     * 新增电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    public int insertElectricityRate(ElectricityRate electricityRate);

    /**
     * 修改电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    public int updateElectricityRate(ElectricityRate electricityRate);

    /**
     * 删除电费
     * 
     * @param id 电费主键
     * @return 结果
     */
    public int deleteElectricityRateById(Long id);

    /**
     * 批量删除电费
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteElectricityRateByIds(Long[] ids);

    ElectricityRate selectLatestRecord();

//    查询电费曲线，参数为返回的数量
    List<ElectricityRate> selectRateRecordForLine(@Param("count") Integer count);
}
