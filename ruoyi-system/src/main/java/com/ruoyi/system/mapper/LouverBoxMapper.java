package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.LouverBox;
import com.ruoyi.system.domain.OfficeReport;
import org.apache.ibatis.annotations.Mapper;

/**
 * 百叶箱Mapper接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Mapper
public interface LouverBoxMapper 
{
    /**
     * 查询百叶箱
     * 
     * @param id 百叶箱主键
     * @return 百叶箱
     */
    public LouverBox selectLouverBoxById(Integer id);

    /**
     * 查询百叶箱列表
     * 
     * @param louverBox 百叶箱
     * @return 百叶箱集合
     */
    public List<LouverBox> selectLouverBoxList(LouverBox louverBox);

    /**
     * 新增百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    public int insertLouverBox(LouverBox louverBox);

    /**
     * 修改百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    public int updateLouverBox(LouverBox louverBox);

    /**
     * 删除百叶箱
     * 
     * @param id 百叶箱主键
     * @return 结果
     */
    public int deleteLouverBoxById(Integer id);

    /**
     * 批量删除百叶箱
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLouverBoxByIds(Integer[] ids);

/*
根据时间查询百叶箱集成式传感器的温湿度曲线
 */
    List<LouverBox> selectLouverHumitureList(String selectTime);

    /**
     * 查询温湿度 光照峰值
     */
    OfficeReport rangeOfHumitureIllData(String selectTime);

    /**
     * 查询最新一条百叶箱集成式传感器数据
     * @return
     */
    LouverBox selectLatestLouverBox();
}
