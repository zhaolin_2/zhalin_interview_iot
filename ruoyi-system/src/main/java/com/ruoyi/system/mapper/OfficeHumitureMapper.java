package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.OfficeHumiture;
import org.apache.ibatis.annotations.Mapper;

/**
 * humitureMapper接口
 * 
 * @author Lin
 * @date 2022-10-31
 */
@Mapper
public interface OfficeHumitureMapper 
{
    /**
     * 查询humiture
     * 
     * @param id humiture主键
     * @return humiture
     */
    public OfficeHumiture selectOfficeHumitureById(Long id);

    /**
     * 查询humiture列表
     * 
     * @param officeHumiture humiture
     * @return humiture集合
     */
    public List<OfficeHumiture> selectOfficeHumitureList(OfficeHumiture officeHumiture);

    /**
     * 新增humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    public int insertOfficeHumiture(OfficeHumiture officeHumiture);

    /**
     * 修改humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    public int updateOfficeHumiture(OfficeHumiture officeHumiture);

    /**
     * 删除humiture
     * 
     * @param id humiture主键
     * @return 结果
     */
    public int deleteOfficeHumitureById(Long id);

    /**
     * 批量删除humiture
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOfficeHumitureByIds(Long[] ids);

    /**
     * 根据日期时间查询温湿度数组
     */
    List<OfficeHumiture> selectHumitureListByTime(String selectTime);

    OfficeHumiture selectLastHumiture();
}
