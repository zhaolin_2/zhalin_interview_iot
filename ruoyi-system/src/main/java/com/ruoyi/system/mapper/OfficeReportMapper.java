package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.OfficeReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 报表Mapper接口
 * 
 * @author Allin
 * @date 2022-11-10
 */
@Mapper
public interface OfficeReportMapper 
{
    /**
     * 查询报表
     * 
     * @param id 报表主键
     * @return 报表
     */
    public OfficeReport selectOfficeReportById(Long id);

    /**
     * 查询报表列表
     * 
     * @param officeReport 报表
     * @return 报表集合
     */
    public List<OfficeReport> selectOfficeReportList(OfficeReport officeReport);

    /**
     * 新增报表
     * 
     * @param officeReport 报表
     * @return 结果
     */
    public int insertOfficeReport(OfficeReport officeReport);

    /**
     * 修改报表
     * 
     * @param officeReport 报表
     * @return 结果
     */
    public int updateOfficeReport(OfficeReport officeReport);

    /**
     * 删除报表
     * 
     * @param id 报表主键
     * @return 结果
     */
    public int deleteOfficeReportById(Long id);

    /**
     * 批量删除报表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOfficeReportByIds(Long[] ids);

    public OfficeReport selectReportByDate(String selectDate);


    public List<OfficeReport> selectAllReport(@Param("count") Integer count);
}
