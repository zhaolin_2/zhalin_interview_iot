package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.OfficeStock;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存Mapper接口
 * 
 * @author lin
 * @date 2022-12-06
 */
@Mapper
public interface OfficeStockMapper 
{
    /**
     * 查询库存
     * 
     * @param id 库存主键
     * @return 库存
     */
    public OfficeStock selectOfficeStockById(Long id);

    /**
     * 查询库存列表
     * 
     * @param officeStock 库存
     * @return 库存集合
     */
    public List<OfficeStock> selectOfficeStockList(OfficeStock officeStock);

    /**
     * 新增库存
     * 
     * @param officeStock 库存
     * @return 结果
     */
    public int insertOfficeStock(OfficeStock officeStock);

    /**
     * 修改库存
     * 
     * @param officeStock 库存
     * @return 结果
     */
    public int updateOfficeStock(OfficeStock officeStock);

    /**
     * 删除库存
     * 
     * @param id 库存主键
     * @return 结果
     */
    public int deleteOfficeStockById(Long id);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOfficeStockByIds(Long[] ids);
}
