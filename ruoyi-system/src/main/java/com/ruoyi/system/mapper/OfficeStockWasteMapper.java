package com.ruoyi.system.mapper;
import com.ruoyi.system.domain.OfficeStockWaste;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 废弃品Mapper接口
 * 
 * @author lin
 * @date 2022-12-06
 */
@Mapper
public interface OfficeStockWasteMapper
{


    /**
     * 查询库存
     * 
     * @param id 库存主键
     * @return 废弃品
     */
    public OfficeStockWaste selectOfficeStockWasteById(Long id);

    /**
     * 查询库存列表
     * 
     * @param officeStockWaste 废弃品
     * @return 库存集合
     */
    public List<OfficeStockWaste> selectOfficeStockWasteList(OfficeStockWaste officeStockWaste);

    /**
     * 新增库存
     * 
     * @param officeStockWaste 废弃品
     * @return 结果
     */
    public int insertOfficeStockWaste(OfficeStockWaste officeStockWaste);

    /**
     * 修改库存
     * 
     * @param officeStockWaste 废弃品
     * @return 结果
     */
    public int updateOfficeStockWaste(OfficeStockWaste officeStockWaste);

    /**
     * 删除库存
     * 
     * @param id 库存主键
     * @return 结果
     */
    public int deleteOfficeStockWasteById(Long id);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOfficeStockWasteByIds(Long[] ids);
}
