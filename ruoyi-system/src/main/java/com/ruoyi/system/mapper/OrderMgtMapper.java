package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.OrderMgt;

/**
 * 订单管理Mapper接口
 * 
 * @author Lin
 * @date 2023-02-10
 */
public interface OrderMgtMapper 
{
    /**
     * 查询订单管理
     * 
     * @param id 订单管理主键
     * @return 订单管理
     */
    public OrderMgt selectOrderMgtById(Long id);

    /**
     * 查询订单管理列表
     * 
     * @param orderMgt 订单管理
     * @return 订单管理集合
     */
    public List<OrderMgt> selectOrderMgtList(OrderMgt orderMgt);

    /**
     * 新增订单管理
     * 
     * @param orderMgt 订单管理
     * @return 结果
     */
    public int insertOrderMgt(OrderMgt orderMgt);

    /**
     * 修改订单管理
     * 
     * @param orderMgt 订单管理
     * @return 结果
     */
    public int updateOrderMgt(OrderMgt orderMgt);

    /**
     * 删除订单管理
     * 
     * @param id 订单管理主键
     * @return 结果
     */
    public int deleteOrderMgtById(Long id);

    /**
     * 批量删除订单管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderMgtByIds(Long[] ids);
}
