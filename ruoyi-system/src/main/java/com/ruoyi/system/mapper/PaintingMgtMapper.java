package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PaintingMgt;

/**
 * 书画管理Mapper接口
 * 
 * @author Lin
 * @date 2023-02-15
 */
public interface PaintingMgtMapper 
{
    /**
     * 查询书画管理
     * 
     * @param id 书画管理主键
     * @return 书画管理
     */
    public PaintingMgt selectPaintingMgtById(Long id);

    /**
     * 查询书画管理列表
     * 
     * @param paintingMgt 书画管理
     * @return 书画管理集合
     */
    public List<PaintingMgt> selectPaintingMgtList(PaintingMgt paintingMgt);

    /**
     * 新增书画管理
     * 
     * @param paintingMgt 书画管理
     * @return 结果
     */
    public int insertPaintingMgt(PaintingMgt paintingMgt);

    /**
     * 修改书画管理
     * 
     * @param paintingMgt 书画管理
     * @return 结果
     */
    public int updatePaintingMgt(PaintingMgt paintingMgt);

    /**
     * 删除书画管理
     * 
     * @param id 书画管理主键
     * @return 结果
     */
    public int deletePaintingMgtById(Long id);

    /**
     * 批量删除书画管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePaintingMgtByIds(Long[] ids);
}
