package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ProductTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 模板Mapper接口
 * 
 * @author lin
 * @date 2022-12-07
 */
@Mapper
public interface ProductTemplateMapper 
{
    /**
     * 查询模板
     * 
     * @param id 模板主键
     * @return 模板
     */
    public ProductTemplate selectProductTemplateById(Long id);

    /**
     * 查询模板列表
     * 
     * @param productTemplate 模板
     * @return 模板集合
     */
    public List<ProductTemplate> selectProductTemplateList(ProductTemplate productTemplate);

    /**
     * 新增模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    public int insertProductTemplate(ProductTemplate productTemplate);

    /**
     * 修改模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    public int updateProductTemplate(ProductTemplate productTemplate);

    /**
     * 删除模板
     * 
     * @param id 模板主键
     * @return 结果
     */
    public int deleteProductTemplateById(Long id);

    /**
     * 批量删除模板
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductTemplateByIds(Long[] ids);
}
