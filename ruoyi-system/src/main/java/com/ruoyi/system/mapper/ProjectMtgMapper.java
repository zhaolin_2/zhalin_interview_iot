package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ProjectMtg;

/**
 * 项目管理Mapper接口
 * 
 * @author Lin
 * @date 2023-02-22
 */
public interface ProjectMtgMapper 
{
    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    public ProjectMtg selectProjectMtgById(Long id);

    /**
     * 查询项目管理列表
     * 
     * @param projectMtg 项目管理
     * @return 项目管理集合
     */
    public List<ProjectMtg> selectProjectMtgList(ProjectMtg projectMtg);

    /**
     * 新增项目管理
     * 
     * @param projectMtg 项目管理
     * @return 结果
     */
    public int insertProjectMtg(ProjectMtg projectMtg);

    /**
     * 修改项目管理
     * 
     * @param projectMtg 项目管理
     * @return 结果
     */
    public int updateProjectMtg(ProjectMtg projectMtg);

    /**
     * 删除项目管理
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    public int deleteProjectMtgById(Long id);

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProjectMtgByIds(Long[] ids);
}
