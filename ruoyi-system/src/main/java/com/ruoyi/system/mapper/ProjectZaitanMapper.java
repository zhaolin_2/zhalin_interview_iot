package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ProjectZaitan;

/**
 * 在谈项目Mapper接口
 * 
 * @author Lin
 * @date 2023-02-22
 */
public interface ProjectZaitanMapper 
{
    /**
     * 查询在谈项目
     * 
     * @param id 在谈项目主键
     * @return 在谈项目
     */
    public ProjectZaitan selectProjectZaitanById(Long id);

    /**
     * 查询在谈项目列表
     * 
     * @param projectZaitan 在谈项目
     * @return 在谈项目集合
     */
    public List<ProjectZaitan> selectProjectZaitanList(ProjectZaitan projectZaitan);

    /**
     * 新增在谈项目
     * 
     * @param projectZaitan 在谈项目
     * @return 结果
     */
    public int insertProjectZaitan(ProjectZaitan projectZaitan);

    /**
     * 修改在谈项目
     * 
     * @param projectZaitan 在谈项目
     * @return 结果
     */
    public int updateProjectZaitan(ProjectZaitan projectZaitan);

    /**
     * 删除在谈项目
     * 
     * @param id 在谈项目主键
     * @return 结果
     */
    public int deleteProjectZaitanById(Long id);

    /**
     * 批量删除在谈项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProjectZaitanByIds(Long[] ids);
}
