package com.ruoyi.system.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.PtDevice;
import org.apache.ibatis.annotations.Mapper;

/**
 * pt测温Mapper接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Mapper
public interface PtDeviceMapper 
{
    /**
     * 查询pt测温
     * 
     * @param id pt测温主键
     * @return pt测温
     */
    public PtDevice selectPtDeviceById(Long id);

    /**
     * 查询pt测温列表
     * 
     * @param ptDevice pt测温
     * @return pt测温集合
     */
    public List<PtDevice> selectPtDeviceList(PtDevice ptDevice);

    /**
     * 新增pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    public int insertPtDevice(PtDevice ptDevice);

    /**
     * 修改pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    public int updatePtDevice(PtDevice ptDevice);

    /**
     * 删除pt测温
     * 
     * @param id pt测温主键
     * @return 结果
     */
    public int deletePtDeviceById(Long id);

    /**
     * 批量删除pt测温
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePtDeviceByIds(Long[] ids);

    /**
     * 用于查询pt100的温度曲线的maopper
     * @return 返回内容为数据对象的数组
     */
    public List<PtDevice> selectPtTemperatureListByDate(String date);

    /**
     * 查询最新一条pt100数据
     * @return
     */
    PtDevice selectLatestPtDevice();
}
