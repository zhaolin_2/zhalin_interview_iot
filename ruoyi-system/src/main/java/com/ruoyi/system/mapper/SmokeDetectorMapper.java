package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SmokeDetector;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报警器Mapper接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Mapper
public interface SmokeDetectorMapper 
{
    /**
     * 查询报警器
     * 
     * @param id 报警器主键
     * @return 报警器
     */
    public SmokeDetector selectSmokeDetectorById(Integer id);

    /**
     * 查询报警器列表
     * 
     * @param smokeDetector 报警器
     * @return 报警器集合
     */
    public List<SmokeDetector> selectSmokeDetectorList(SmokeDetector smokeDetector);

    /**
     * 新增报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    public int insertSmokeDetector(SmokeDetector smokeDetector);

    /**
     * 修改报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    public int updateSmokeDetector(SmokeDetector smokeDetector);

    /**
     * 删除报警器
     * 
     * @param id 报警器主键
     * @return 结果
     */
    public int deleteSmokeDetectorById(Integer id);

    /**
     * 批量删除报警器
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSmokeDetectorByIds(Integer[] ids);

    SmokeDetector selectLatestSmokeDetector();
}
