package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TElectricityMeter;
import org.apache.ibatis.annotations.Mapper;

/**
 * 三项电表Mapper接口
 * 
 * @author Lin
 * @date 2022-11-09
 */
@Mapper
public interface TElectricityMeterMapper 
{
    /**
     * 查询三项电表
     * 
     * @param id 三项电表主键
     * @return 三项电表
     */
    public TElectricityMeter selectTElectricityMeterById(Long id);

    /**
     * 查询三项电表列表
     * 
     * @param tElectricityMeter 三项电表
     * @return 三项电表集合
     */
    public List<TElectricityMeter> selectTElectricityMeterList(TElectricityMeter tElectricityMeter);

    /**
     * 新增三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    public int insertTElectricityMeter(TElectricityMeter tElectricityMeter);

    /**
     * 修改三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    public int updateTElectricityMeter(TElectricityMeter tElectricityMeter);

    /**
     * 删除三项电表
     * 
     * @param id 三项电表主键
     * @return 结果
     */
    public int deleteTElectricityMeterById(Long id);

    /**
     * 批量删除三项电表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTElectricityMeterByIds(Long[] ids);

    /**
     * 查询三项电表的电流 总有功功率列表
     */
    public List<TElectricityMeter> selectTElectricityListByDate(String selectTime);

    /**
     * 查询表中最新一条 三项电表数据
     */
    TElectricityMeter selectLastTElectricity();


    /**
     * 根据日期查询当日的最后（最新）一条数据
     */
    TElectricityMeter selectLastTElectricityByDate(String selectDate);


}
