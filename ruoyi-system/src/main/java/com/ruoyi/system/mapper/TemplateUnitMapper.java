package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TemplateUnit;
import org.apache.ibatis.annotations.Mapper;

/**
 * 组件Mapper接口
 * 
 * @author lin
 * @date 2022-12-07
 */
@Mapper
public interface TemplateUnitMapper 
{
    /**
     * 查询组件
     * 
     * @param id 组件主键
     * @return 组件
     */
    public TemplateUnit selectTemplateUnitById(Long id);

    /**
     * 查询组件列表
     * 
     * @param templateUnit 组件
     * @return 组件集合
     */
    public List<TemplateUnit> selectTemplateUnitList(TemplateUnit templateUnit);

    /**
     * 新增组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    public int insertTemplateUnit(TemplateUnit templateUnit);

    /**
     * 修改组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    public int updateTemplateUnit(TemplateUnit templateUnit);

    /**
     * 删除组件
     * 
     * @param id 组件主键
     * @return 结果
     */
    public int deleteTemplateUnitById(Long id);

    /**
     * 批量删除组件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTemplateUnitByIds(Long[] ids);

    TemplateUnit selectTemplateUnitByStockId(Long stockId);

    List<TemplateUnit> selectTemplateUnitByTemplateId(Long templateId);
}
