package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TestMessage;
import org.apache.ibatis.annotations.Mapper;

/**
 * 录入测试Mapper接口
 * 
 * @author lin
 * @date 2022-09-06
 */
@Mapper
public interface TestMessageMapper 
{
    /**
     * 查询录入测试
     * 
     * @param id 录入测试主键
     * @return 录入测试
     */
    public TestMessage selectTestMessageById(Long id);

    /**
     * 查询录入测试列表
     * 
     * @param testMessage 录入测试
     * @return 录入测试集合
     */
    public List<TestMessage> selectTestMessageList(TestMessage testMessage);

    /**
     * 新增录入测试
     * 
     * @param testMessage 录入测试
     * @return 结果
     */
    public int insertTestMessage(TestMessage testMessage);

    /**
     * 修改录入测试
     * 
     * @param testMessage 录入测试
     * @return 结果
     */
    public int updateTestMessage(TestMessage testMessage);

    /**
     * 删除录入测试
     * 
     * @param id 录入测试主键
     * @return 结果
     */
    public int deleteTestMessageById(Long id);

    /**
     * 批量删除录入测试
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTestMessageByIds(Long[] ids);
}
