package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DeviceCheck;

/**
 * 点检Service接口
 * 
 * @author Lin
 * @date 2023-02-10
 */
public interface IDeviceCheckService 
{
    /**
     * 查询点检
     * 
     * @param id 点检主键
     * @return 点检
     */
    public DeviceCheck selectDeviceCheckById(Long id);

    /**
     * 查询点检列表
     * 
     * @param deviceCheck 点检
     * @return 点检集合
     */
    public List<DeviceCheck> selectDeviceCheckList(DeviceCheck deviceCheck);

    /**
     * 新增点检
     * 
     * @param deviceCheck 点检
     * @return 结果
     */
    public int insertDeviceCheck(DeviceCheck deviceCheck);

    /**
     * 修改点检
     * 
     * @param deviceCheck 点检
     * @return 结果
     */
    public int updateDeviceCheck(DeviceCheck deviceCheck);

    /**
     * 批量删除点检
     * 
     * @param ids 需要删除的点检主键集合
     * @return 结果
     */
    public int deleteDeviceCheckByIds(Long[] ids);

    /**
     * 删除点检信息
     * 
     * @param id 点检主键
     * @return 结果
     */
    public int deleteDeviceCheckById(Long id);
}
