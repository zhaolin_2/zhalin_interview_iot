package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DeviceCount;

/**
 * 生产计数Service接口
 * 
 * @author Lin
 * @date 2023-02-15
 */
public interface IDeviceCountService 
{
    /**
     * 查询生产计数
     * 
     * @param id 生产计数主键
     * @return 生产计数
     */
    public DeviceCount selectDeviceCountById(Long id);

    /**
     * 查询生产计数列表
     * 
     * @param deviceCount 生产计数
     * @return 生产计数集合
     */
    public List<DeviceCount> selectDeviceCountList(DeviceCount deviceCount);

    /**
     * 新增生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    public int insertDeviceCount(DeviceCount deviceCount);

    /**
     * 修改生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    public int updateDeviceCount(DeviceCount deviceCount);

    /**
     * 批量删除生产计数
     * 
     * @param ids 需要删除的生产计数主键集合
     * @return 结果
     */
    public int deleteDeviceCountByIds(Long[] ids);

    /**
     * 删除生产计数信息
     * 
     * @param id 生产计数主键
     * @return 结果
     */
    public int deleteDeviceCountById(Long id);

    void deviceCountDataSimulation();
}
