package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DeviceLimit;

/**
 * 限制Service接口
 * 
 * @author Lin
 * @date 2022-11-18
 */
public interface IDeviceLimitService 
{
    /**
     * 查询限制
     * 
     * @param id 限制主键
     * @return 限制
     */
    public DeviceLimit selectDeviceLimitById(Long id);

    /**
     * 查询限制列表
     * 
     * @param deviceLimit 限制
     * @return 限制集合
     */
    public List<DeviceLimit> selectDeviceLimitList(DeviceLimit deviceLimit);

    /**
     * 新增限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    public int insertDeviceLimit(DeviceLimit deviceLimit);

    /**
     * 修改限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    public int updateDeviceLimit(DeviceLimit deviceLimit);

    /**
     * 批量删除限制
     * 
     * @param ids 需要删除的限制主键集合
     * @return 结果
     */
    public int deleteDeviceLimitByIds(Long[] ids);

    /**
     * 删除限制信息
     * 
     * @param id 限制主键
     * @return 结果
     */
    public int deleteDeviceLimitById(Long id);

    /**
     * 根据设备编号查询数据上线限列表
     * @param deviceNo
     * @return
     */
    List<DeviceLimit> selectDeviceLimitByDeviceNo(Long id,String deviceNo);
}
