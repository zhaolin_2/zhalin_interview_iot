package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.ElectricityMeter;

/**
 * 电表Service接口
 * 
 * @author Lin
 * @date 2022-11-07
 */
public interface IElectricityMeterService 
{
    /**
     * 查询电表
     * 
     * @param id 电表主键
     * @return 电表
     */
    public ElectricityMeter selectElectricityMeterById(Integer id);

    /**
     * 查询电表列表
     * 
     * @param electricityMeter 电表
     * @return 电表集合
     */
    public List<ElectricityMeter> selectElectricityMeterList(ElectricityMeter electricityMeter);

    /**
     * 新增电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    public int insertElectricityMeter(ElectricityMeter electricityMeter);

    /**
     * 修改电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    public int updateElectricityMeter(ElectricityMeter electricityMeter);

    /**
     * 批量删除电表
     * 
     * @param ids 需要删除的电表主键集合
     * @return 结果
     */
    public int deleteElectricityMeterByIds(Integer[] ids);

    /**
     * 删除电表信息
     * 
     * @param id 电表主键
     * @return 结果
     */
    public int deleteElectricityMeterById(Integer id);

    //电流 电压 有功功率
    void AUApRecord(String msg);

    //功率因数
    void PfRecord(String msg);

    //频率
    void frequencyRecord(String msg);

    //总有功电量
    void AERecord(String msg);

    //拉合闸状态
    void stateRecord(String msg);

    List<ElectricityMeter> selectElectricityListByDate(Date selectTime);

    ElectricityMeter selectLastElectricity(Date selectDate);

    ElectricityMeter selectElectricityByHourTime(String selectHourTime);
}
