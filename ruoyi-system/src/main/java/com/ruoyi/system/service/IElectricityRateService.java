package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.ElectricityRate;
import com.ruoyi.system.domain.request.dataIntegration.ElectricPowerRateRequest;

/**
 * 电费Service接口
 * 
 * @author Lin
 * @date 2023-03-03
 */
public interface IElectricityRateService 
{
    /**
     * 查询电费
     * 
     * @param id 电费主键
     * @return 电费
     */
    public ElectricityRate selectElectricityRateById(Long id);

    /**
     * 查询电费列表
     * 
     * @param electricityRate 电费
     * @return 电费集合
     */
    public List<ElectricityRate> selectElectricityRateList(ElectricityRate electricityRate);

    /**
     * 新增电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    public int insertElectricityRate(ElectricityRate electricityRate);

    /**
     * 修改电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    public int updateElectricityRate(ElectricityRate electricityRate);

    /**
     * 批量删除电费
     * 
     * @param ids 需要删除的电费主键集合
     * @return 结果
     */
    public int deleteElectricityRateByIds(Long[] ids);

    /**
     * 删除电费信息
     * 
     * @param id 电费主键
     * @return 结果
     */
    public int deleteElectricityRateById(Long id);

    void electricityRateUpdate(ElectricityMeter electricityMeter);

    ElectricPowerRateRequest electricRateData();

    List selectRateRecordForLine(Integer count);
}
