package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FileDistribute;

/**
 * 文件分发DemoService接口
 * 
 * @author Lin
 * @date 2022-11-21
 */
public interface IFileDistributeService 
{
    /**
     * 查询文件分发Demo
     * 
     * @param id 文件分发Demo主键
     * @return 文件分发Demo
     */
    public FileDistribute selectFileDistributeById(Long id);

    /**
     * 查询文件分发Demo列表
     * 
     * @param fileDistribute 文件分发Demo
     * @return 文件分发Demo集合
     */
    public List<FileDistribute> selectFileDistributeList(FileDistribute fileDistribute);

    /**
     * 新增文件分发Demo
     * 
     * @param fileDistribute 文件分发Demo
     * @return 结果
     */
    public int insertFileDistribute(FileDistribute fileDistribute);

    /**
     * 修改文件分发Demo
     * 
     * @param fileDistribute 文件分发Demo
     * @return 结果
     */
    public int updateFileDistribute(FileDistribute fileDistribute);

    /**
     * 批量删除文件分发Demo
     * 
     * @param ids 需要删除的文件分发Demo主键集合
     * @return 结果
     */
    public int deleteFileDistributeByIds(Long[] ids);

    /**
     * 删除文件分发Demo信息
     * 
     * @param id 文件分发Demo主键
     * @return 结果
     */
    public int deleteFileDistributeById(Long id);
}
