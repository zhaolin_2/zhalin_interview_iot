package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.LouverBox;
import com.ruoyi.system.domain.OfficeReport;

/**
 * 百叶箱Service接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
public interface ILouverBoxService 
{
    /**
     * 查询百叶箱
     * 
     * @param id 百叶箱主键
     * @return 百叶箱
     */
    public LouverBox selectLouverBoxById(Integer id);

    /**
     * 查询百叶箱列表
     * 
     * @param louverBox 百叶箱
     * @return 百叶箱集合
     */
    public List<LouverBox> selectLouverBoxList(LouverBox louverBox);

    /**
     * 新增百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    public int insertLouverBox(LouverBox louverBox);

    /**
     * 修改百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    public int updateLouverBox(LouverBox louverBox);

    /**
     * 批量删除百叶箱
     * 
     * @param ids 需要删除的百叶箱主键集合
     * @return 结果
     */
    public int deleteLouverBoxByIds(Integer[] ids);

    /**
     * 删除百叶箱信息
     * 
     * @param id 百叶箱主键
     * @return 结果
     */
    public int deleteLouverBoxById(Integer id);

    void humitureRecord(String msg);

    void illuminationRecord(String msg);

    List selectLouverHumitureList(Date selectTime);

    List selectIlluminationList(Date selectTime);

    OfficeReport rangeOfHumitureIllData(Date selectTime);
}
