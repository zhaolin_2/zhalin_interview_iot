package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OfficeDevice;
import com.ruoyi.system.domain.StockMessage;
import com.ruoyi.system.domain.request.dataIntegration.DeviceLatestDataRequest;
import com.ruoyi.system.domain.request.dataIntegration.DeviceStateRequest;
import com.ruoyi.system.domain.request.dataIntegration.StockStateRequest;

/**
 * 设备Service接口
 * 
 * @author Lin
 * @date 2022-11-18
 */
public interface IOfficeDeviceService 
{
    /**
     * 查询设备
     * 
     * @param id 设备主键
     * @return 设备
     */
    public OfficeDevice selectOfficeDeviceById(Integer id);

    /**
     * 查询设备列表
     * 
     * @param officeDevice 设备
     * @return 设备集合
     */
    public List<OfficeDevice> selectOfficeDeviceList(OfficeDevice officeDevice);

    /**
     * 新增设备
     * 
     * @param officeDevice 设备
     * @return 结果
     */
    public int insertOfficeDevice(OfficeDevice officeDevice);

    /**
     * 修改设备
     * 
     * @param officeDevice 设备
     * @return 结果
     */
    public int updateOfficeDevice(OfficeDevice officeDevice);

    /**
     * 批量删除设备
     * 
     * @param ids 需要删除的设备主键集合
     * @return 结果
     */
    public int deleteOfficeDeviceByIds(Integer[] ids);

    /**
     * 删除设备信息
     * 
     * @param id 设备主键
     * @return 结果
     */
    public int deleteOfficeDeviceById(Integer id);

    DeviceStateRequest deviceStateRequest();

    StockStateRequest stockStateRequest();

    List<DeviceLatestDataRequest> deviceLatestDataRequest();
}
