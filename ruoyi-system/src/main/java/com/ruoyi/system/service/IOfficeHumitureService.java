package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.OfficeHumiture;
import com.ruoyi.system.domain.request.OfficeHumitureRequest;

/**
 * humitureService接口
 * 
 * @author Lin
 * @date 2022-10-31
 */
public interface IOfficeHumitureService 
{
    /**
     * 查询humiture
     * 
     * @param id humiture主键
     * @return humiture
     */
    public OfficeHumiture selectOfficeHumitureById(Long id);

    /**
     * 查询humiture列表
     * 
     * @param officeHumiture humiture
     * @return humiture集合
     */
    public List<OfficeHumiture> selectOfficeHumitureList(OfficeHumiture officeHumiture);

    /**
     * 新增humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    public int insertOfficeHumiture(OfficeHumiture officeHumiture);

    /**
     * 修改humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    public int updateOfficeHumiture(OfficeHumiture officeHumiture);

    /**
     * 批量删除humiture
     * 
     * @param ids 需要删除的humiture主键集合
     * @return 结果
     */
    public int deleteOfficeHumitureByIds(Long[] ids);

    /**
     * 删除humiture信息
     * 
     * @param id humiture主键
     * @return 结果
     */
    public int deleteOfficeHumitureById(Long id);

    void humitureRecord(String msg);

    List selectHumitureListByTime(Date selectTime);

    OfficeHumitureRequest getHumitureNowData();
}
