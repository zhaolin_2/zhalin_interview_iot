package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.OfficeReport;

/**
 * 报表Service接口
 * 
 * @author Allin
 * @date 2022-11-10
 */
public interface IOfficeReportService 
{
    /**
     * 查询报表
     * 
     * @param id 报表主键
     * @return 报表
     */
    public OfficeReport selectOfficeReportById(Long id);

    /**
     * 查询报表列表
     * 
     * @param officeReport 报表
     * @return 报表集合
     */
    public List<OfficeReport> selectOfficeReportList(OfficeReport officeReport);

    /**
     * 新增报表
     * 
     * @param officeReport 报表
     * @return 结果
     */
    public int insertOfficeReport(OfficeReport officeReport);

    /**
     * 修改报表
     * 
     * @param officeReport 报表
     * @return 结果
     */
    public int updateOfficeReport(OfficeReport officeReport);

    /**
     * 批量删除报表
     * 
     * @param ids 需要删除的报表主键集合
     * @return 结果
     */
    public int deleteOfficeReportByIds(Long[] ids);

    /**
     * 删除报表信息
     * 
     * @param id 报表主键
     * @return 结果
     */
    public int deleteOfficeReportById(Long id);

    List selectConsumptionList(Integer count);

    List selectConsumptionListForT(Integer count);

    OfficeReport selectReportGenerate(Date selectTime);

    OfficeReport reportGenerate(Date selectTime);
}
