package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.StockMessage;
import com.ruoyi.system.domain.request.dataIntegration.StockStateRequest;

/**
 * 库存管理Service接口
 * 
 * @author Lin
 * @date 2022-11-30
 */
public interface IOfficeStockService 
{
    /**
     * 查询库存管理
     * 
     * @param id 库存管理主键
     * @return 库存管理
     */
    public OfficeStock selectOfficeStockById(Long id);

    /**
     * 查询库存管理列表
     * 
     * @param officeStock 库存管理
     * @return 库存管理集合
     */
    public List<OfficeStock> selectOfficeStockList(OfficeStock officeStock);

    /**
     * 新增库存管理
     * 
     * @param officeStock 库存管理
     * @return 结果
     */
    public int insertOfficeStock(OfficeStock officeStock);

    /**
     * 修改库存管理
     * 
     * @param officeStock 库存管理
     * @return 结果
     */
    public int updateOfficeStock(OfficeStock officeStock);

    /**
     * 批量删除库存管理
     * 
     * @param ids 需要删除的库存管理主键集合
     * @return 结果
     */
    public int deleteOfficeStockByIds(Long[] ids);

    /**
     * 删除库存管理信息
     * 
     * @param id 库存管理主键
     * @return 结果
     */
    public int deleteOfficeStockById(Long id);

    public int outStock(Long id,Long num,String name,String message);

    public int inStock(Long id,Long num,String name,String message);

    List<StockMessage> selectsStockMessagesByStockId(Long id);

    void outStockToWst(Long id,Long num,String message);

    String importStock(List<OfficeStock> userList, Boolean isUpdateSupport);

}
