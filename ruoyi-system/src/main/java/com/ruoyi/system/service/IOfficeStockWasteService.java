package com.ruoyi.system.service;

import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.OfficeStockWaste;
import com.ruoyi.system.domain.StockMessage;

import java.util.List;

/**
 * @Author Lin
 * @Date 2022 12 07 11
 **/
public interface IOfficeStockWasteService {

        /**
         * 查询库存管理
         *
         * @param id 库存管理主键
         * @return 库存管理
         */
        public OfficeStockWaste selectOfficeStockWasteById(Long id);

        /**
         * 查询库存管理列表
         *
         * @param officeStockWaste 废弃管理
         * @return 库存管理集合
         */
        public List<OfficeStockWaste> selectOfficeStockWasteList(OfficeStockWaste officeStockWaste);

        /**
         * 新增废弃品
         *
         * @param officeStockWaste 库存管理
         * @return 结果
         */
        public int insertOfficeStockWaste(OfficeStockWaste officeStockWaste);

        /**
         * 修改废弃品
         *
         * @param officeStockWaste 库存管理
         * @return 结果
         */
        public int updateOfficeStock(OfficeStockWaste officeStockWaste);


        /**
         * 删除库存管理信息
         *
         * @param id 库存管理主键
         * @return 结果
         */
        public int deleteOfficeStockWasteById(Long id);


}
