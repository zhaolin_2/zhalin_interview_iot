package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.ProductTemplate;
import com.ruoyi.system.domain.request.OfficeStockRequest;

/**
 * 模板Service接口
 * 
 * @author lin
 * @date 2022-12-07
 */
public interface IProductTemplateService 
{
    /**
     * 查询模板
     * 
     * @param id 模板主键
     * @return 模板
     */
    public ProductTemplate selectProductTemplateById(Long id);

    /**
     * 查询模板列表
     * 
     * @param productTemplate 模板
     * @return 模板集合
     */
    public List<ProductTemplate> selectProductTemplateList(ProductTemplate productTemplate);

    /**
     * 新增模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    public int insertProductTemplate(ProductTemplate productTemplate);

    /**
     * 修改模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    public int updateProductTemplate(ProductTemplate productTemplate);

    /**
     * 批量删除模板
     * 
     * @param ids 需要删除的模板主键集合
     * @return 结果
     */
    public int deleteProductTemplateByIds(Long[] ids);

    /**
     * 删除模板信息
     * 
     * @param id 模板主键
     * @return 结果
     */
    public int deleteProductTemplateById(Long id);

    /**
     * 组装前核对组装后库存剩余
     * @param id
     * @param num
     */
    List<OfficeStockRequest> checkBeforeAssemble(Long id, Long num);

    void assembleToStock(OfficeStock officeStock);
}
