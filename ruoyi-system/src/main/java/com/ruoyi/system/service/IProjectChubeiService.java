package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProjectChubei;

/**
 * 项目储备Service接口
 * 
 * @author Lin
 * @date 2023-02-22
 */
public interface IProjectChubeiService 
{
    /**
     * 查询项目储备
     * 
     * @param id 项目储备主键
     * @return 项目储备
     */
    public ProjectChubei selectProjectChubeiById(Long id);

    /**
     * 查询项目储备列表
     * 
     * @param projectChubei 项目储备
     * @return 项目储备集合
     */
    public List<ProjectChubei> selectProjectChubeiList(ProjectChubei projectChubei);

    /**
     * 新增项目储备
     * 
     * @param projectChubei 项目储备
     * @return 结果
     */
    public int insertProjectChubei(ProjectChubei projectChubei);

    /**
     * 修改项目储备
     * 
     * @param projectChubei 项目储备
     * @return 结果
     */
    public int updateProjectChubei(ProjectChubei projectChubei);

    /**
     * 批量删除项目储备
     * 
     * @param ids 需要删除的项目储备主键集合
     * @return 结果
     */
    public int deleteProjectChubeiByIds(Long[] ids);

    /**
     * 删除项目储备信息
     * 
     * @param id 项目储备主键
     * @return 结果
     */
    public int deleteProjectChubeiById(Long id);
}
