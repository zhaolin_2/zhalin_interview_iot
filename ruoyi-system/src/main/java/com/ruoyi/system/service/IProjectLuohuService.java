package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProjectLuohu;

/**
 * 落户项目Service接口
 * 
 * @author Lin
 * @date 2023-02-22
 */
public interface IProjectLuohuService 
{
    /**
     * 查询落户项目
     * 
     * @param id 落户项目主键
     * @return 落户项目
     */
    public ProjectLuohu selectProjectLuohuById(Long id);

    /**
     * 查询落户项目列表
     * 
     * @param projectLuohu 落户项目
     * @return 落户项目集合
     */
    public List<ProjectLuohu> selectProjectLuohuList(ProjectLuohu projectLuohu);

    /**
     * 新增落户项目
     * 
     * @param projectLuohu 落户项目
     * @return 结果
     */
    public int insertProjectLuohu(ProjectLuohu projectLuohu);

    /**
     * 修改落户项目
     * 
     * @param projectLuohu 落户项目
     * @return 结果
     */
    public int updateProjectLuohu(ProjectLuohu projectLuohu);

    /**
     * 批量删除落户项目
     * 
     * @param ids 需要删除的落户项目主键集合
     * @return 结果
     */
    public int deleteProjectLuohuByIds(Long[] ids);

    /**
     * 删除落户项目信息
     * 
     * @param id 落户项目主键
     * @return 结果
     */
    public int deleteProjectLuohuById(Long id);
}
