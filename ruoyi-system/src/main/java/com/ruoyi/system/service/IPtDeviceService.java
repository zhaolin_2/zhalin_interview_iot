package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.PtDevice;

/**
 * pt测温Service接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
public interface IPtDeviceService 
{
    /**
     * 查询pt测温
     * 
     * @param id pt测温主键
     * @return pt测温
     */
    public PtDevice selectPtDeviceById(Long id);

    /**
     * 查询pt测温列表
     * 
     * @param ptDevice pt测温
     * @return pt测温集合
     */
    public List<PtDevice> selectPtDeviceList(PtDevice ptDevice);

    /**
     * 新增pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    public int insertPtDevice(PtDevice ptDevice);

    /**
     * 修改pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    public int updatePtDevice(PtDevice ptDevice);

    /**
     * 批量删除pt测温
     * 
     * @param ids 需要删除的pt测温主键集合
     * @return 结果
     */
    public int deletePtDeviceByIds(Long[] ids);

    /**
     * 删除pt测温信息
     * 
     * @param id pt测温主键
     * @return 结果
     */
    public int deletePtDeviceById(Long id);

    void ptTemperatureRecord(String msg);

    List ptTemperatureList(Date date);
}
