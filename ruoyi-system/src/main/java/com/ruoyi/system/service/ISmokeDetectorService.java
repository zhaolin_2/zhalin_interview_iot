package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SmokeDetector;

/**
 * 报警器Service接口
 * 
 * @author Lin
 * @date 2022-11-04
 */
public interface ISmokeDetectorService 
{
    /**
     * 查询报警器
     * 
     * @param id 报警器主键
     * @return 报警器
     */
    public SmokeDetector selectSmokeDetectorById(Integer id);

    /**
     * 查询报警器列表
     * 
     * @param smokeDetector 报警器
     * @return 报警器集合
     */
    public List<SmokeDetector> selectSmokeDetectorList(SmokeDetector smokeDetector);

    /**
     * 新增报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    public int insertSmokeDetector(SmokeDetector smokeDetector);

    /**
     * 修改报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    public int updateSmokeDetector(SmokeDetector smokeDetector);

    /**
     * 批量删除报警器
     * 
     * @param ids 需要删除的报警器主键集合
     * @return 结果
     */
    public int deleteSmokeDetectorByIds(Integer[] ids);

    /**
     * 删除报警器信息
     * 
     * @param id 报警器主键
     * @return 结果
     */
    public int deleteSmokeDetectorById(Integer id);

    void smokeDetectorRecord(String msg) throws Exception;
}
