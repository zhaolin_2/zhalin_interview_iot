package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.StockMessage;

/**
 * 库存详情Service接口
 * 
 * @author Lin
 * @date 2022-11-30
 */
public interface IStockMessageService 
{
    /**
     * 查询库存详情
     * 
     * @param id 库存详情主键
     * @return 库存详情
     */
    public StockMessage selectStockMessageById(Long id);

    /**
     * 查询库存详情列表
     * 
     * @param stockMessage 库存详情
     * @return 库存详情集合
     */
    public List<StockMessage> selectStockMessageList(StockMessage stockMessage);

    /**
     * 新增库存详情
     * 
     * @param stockMessage 库存详情
     * @return 结果
     */
    public int insertStockMessage(StockMessage stockMessage);

    /**
     * 修改库存详情
     * 
     * @param stockMessage 库存详情
     * @return 结果
     */
    public int updateStockMessage(StockMessage stockMessage);

    /**
     * 批量删除库存详情
     * 
     * @param ids 需要删除的库存详情主键集合
     * @return 结果
     */
    public int deleteStockMessageByIds(Long[] ids);

    /**
     * 删除库存详情信息
     * 
     * @param id 库存详情主键
     * @return 结果
     */
    public int deleteStockMessageById(Long id);
}
