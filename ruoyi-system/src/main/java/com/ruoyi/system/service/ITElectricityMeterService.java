package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.TElectricityMeter;

/**
 * 三项电表Service接口
 * 
 * @author Lin
 * @date 2022-11-09
 */
public interface ITElectricityMeterService 
{
    /**
     * 查询三项电表
     * 
     * @param id 三项电表主键
     * @return 三项电表
     */
    public TElectricityMeter selectTElectricityMeterById(Long id);

    /**
     * 查询三项电表列表
     * 
     * @param tElectricityMeter 三项电表
     * @return 三项电表集合
     */
    public List<TElectricityMeter> selectTElectricityMeterList(TElectricityMeter tElectricityMeter);

    /**
     * 新增三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    public int insertTElectricityMeter(TElectricityMeter tElectricityMeter);

    /**
     * 修改三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    public int updateTElectricityMeter(TElectricityMeter tElectricityMeter);

    /**
     * 批量删除三项电表
     * 
     * @param ids 需要删除的三项电表主键集合
     * @return 结果
     */
    public int deleteTElectricityMeterByIds(Long[] ids);

    /**
     * 删除三项电表信息
     * 
     * @param id 三项电表主键
     * @return 结果
     */
    public int deleteTElectricityMeterById(Long id);

    void tElectricityMeterRecord(String msg);

    void tElectricityMeterKWHRecord(String msg);

    List selectTElectricityListByDate(Date selectDate);

    TElectricityMeter selectLastTElectricity();
}
