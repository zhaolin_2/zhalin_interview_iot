package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TemplateUnit;
import com.ruoyi.system.domain.request.TemplateUnitRequest;

/**
 * 组件Service接口
 * 
 * @author lin
 * @date 2022-12-07
 */
public interface ITemplateUnitService 
{
    /**
     * 查询组件
     * 
     * @param id 组件主键
     * @return 组件
     */
    public TemplateUnit selectTemplateUnitById(Long id);

    /**
     * 查询组件列表
     * 
     * @param templateUnit 组件
     * @return 组件集合
     */
    public List<TemplateUnit> selectTemplateUnitList(TemplateUnit templateUnit);

    /**
     * 新增组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    public int insertTemplateUnit(TemplateUnit templateUnit);

    /**
     * 修改组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    public int updateTemplateUnit(TemplateUnit templateUnit);

    /**
     * 批量删除组件
     * 
     * @param ids 需要删除的组件主键集合
     * @return 结果
     */
    public int deleteTemplateUnitByIds(Long[] ids);

    /**
     * 删除组件信息
     * 
     * @param id 组件主键
     * @return 结果
     */
    public int deleteTemplateUnitById(Long id);

    List<TemplateUnitRequest> showTemplateUnitMes(Long templateId);

    void addTemplateUnits(Long templateId, Long stockId, Long num);

    int updateTemplateUnits(Long stockId,Long num);

    int deleteTemplateUnits(Long stockId);
}
