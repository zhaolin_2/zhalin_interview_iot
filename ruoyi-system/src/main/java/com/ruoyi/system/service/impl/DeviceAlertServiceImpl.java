package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.service.IDeviceAlertService;

/**
 * 报警信息Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-23
 */
@Service
public class DeviceAlertServiceImpl implements IDeviceAlertService 
{
    @Autowired
    private DeviceAlertMapper deviceAlertMapper;

    /**
     * 查询报警信息
     * 
     * @param id 报警信息主键
     * @return 报警信息
     */
    @Override
    public DeviceAlert selectDeviceAlertById(Long id)
    {
        return deviceAlertMapper.selectDeviceAlertById(id);
    }

    /**
     * 查询报警信息列表
     * 
     * @param deviceAlert 报警信息
     * @return 报警信息
     */
    @Override
    public List<DeviceAlert> selectDeviceAlertList(DeviceAlert deviceAlert)
    {
        return deviceAlertMapper.selectDeviceAlertList(deviceAlert);
    }

    /**
     * 新增报警信息
     * 
     * @param deviceAlert 报警信息
     * @return 结果
     */
    @Override
    public int insertDeviceAlert(DeviceAlert deviceAlert)
    {
        return deviceAlertMapper.insertDeviceAlert(deviceAlert);
    }

    /**
     * 修改报警信息
     * 
     * @param deviceAlert 报警信息
     * @return 结果
     */
    @Override
    public int updateDeviceAlert(DeviceAlert deviceAlert)
    {
        return deviceAlertMapper.updateDeviceAlert(deviceAlert);
    }

    /**
     * 批量删除报警信息
     * 
     * @param ids 需要删除的报警信息主键
     * @return 结果
     */
    @Override
    public int deleteDeviceAlertByIds(Long[] ids)
    {
        return deviceAlertMapper.deleteDeviceAlertByIds(ids);
    }

    /**
     * 删除报警信息信息
     * 
     * @param id 报警信息主键
     * @return 结果
     */
    @Override
    public int deleteDeviceAlertById(Long id)
    {
        return deviceAlertMapper.deleteDeviceAlertById(id);
    }
}
