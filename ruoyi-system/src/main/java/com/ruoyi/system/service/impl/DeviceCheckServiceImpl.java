package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceCheckMapper;
import com.ruoyi.system.domain.DeviceCheck;
import com.ruoyi.system.service.IDeviceCheckService;

/**
 * 点检Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-10
 */
@Service
public class DeviceCheckServiceImpl implements IDeviceCheckService 
{
    @Autowired
    private DeviceCheckMapper deviceCheckMapper;

    /**
     * 查询点检
     * 
     * @param id 点检主键
     * @return 点检
     */
    @Override
    public DeviceCheck selectDeviceCheckById(Long id)
    {
        return deviceCheckMapper.selectDeviceCheckById(id);
    }

    /**
     * 查询点检列表
     * 
     * @param deviceCheck 点检
     * @return 点检
     */
    @Override
    public List<DeviceCheck> selectDeviceCheckList(DeviceCheck deviceCheck)
    {
        return deviceCheckMapper.selectDeviceCheckList(deviceCheck);
    }

    /**
     * 新增点检
     * 
     * @param deviceCheck 点检
     * @return 结果
     */
    @Override
    public int insertDeviceCheck(DeviceCheck deviceCheck)
    {
        return deviceCheckMapper.insertDeviceCheck(deviceCheck);
    }

    /**
     * 修改点检
     * 
     * @param deviceCheck 点检
     * @return 结果
     */
    @Override
    public int updateDeviceCheck(DeviceCheck deviceCheck)
    {
        return deviceCheckMapper.updateDeviceCheck(deviceCheck);
    }

    /**
     * 批量删除点检
     * 
     * @param ids 需要删除的点检主键
     * @return 结果
     */
    @Override
    public int deleteDeviceCheckByIds(Long[] ids)
    {
        return deviceCheckMapper.deleteDeviceCheckByIds(ids);
    }

    /**
     * 删除点检信息
     * 
     * @param id 点检主键
     * @return 结果
     */
    @Override
    public int deleteDeviceCheckById(Long id)
    {
        return deviceCheckMapper.deleteDeviceCheckById(id);
    }
}
