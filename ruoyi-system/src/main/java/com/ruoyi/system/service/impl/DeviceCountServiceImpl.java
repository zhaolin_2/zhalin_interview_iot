package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceCountMapper;
import com.ruoyi.system.domain.DeviceCount;
import com.ruoyi.system.service.IDeviceCountService;

/**
 * 生产计数Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-15
 */
@Service
public class DeviceCountServiceImpl implements IDeviceCountService 
{

    private static Long count = 1l;

    @Autowired
    private DeviceCountMapper deviceCountMapper;

    /**
     * 查询生产计数
     * 
     * @param id 生产计数主键
     * @return 生产计数
     */
    @Override
    public DeviceCount selectDeviceCountById(Long id)
    {
        return deviceCountMapper.selectDeviceCountById(id);
    }

    /**
     * 查询生产计数列表
     * 
     * @param deviceCount 生产计数
     * @return 生产计数
     */
    @Override
    public List<DeviceCount> selectDeviceCountList(DeviceCount deviceCount)
    {
        return deviceCountMapper.selectDeviceCountList(deviceCount);
    }

    /**
     * 新增生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    @Override
    public int insertDeviceCount(DeviceCount deviceCount)
    {
        return deviceCountMapper.insertDeviceCount(deviceCount);
    }

    /**
     * 修改生产计数
     * 
     * @param deviceCount 生产计数
     * @return 结果
     */
    @Override
    public int updateDeviceCount(DeviceCount deviceCount)
    {
        return deviceCountMapper.updateDeviceCount(deviceCount);
    }

    /**
     * 批量删除生产计数
     * 
     * @param ids 需要删除的生产计数主键
     * @return 结果
     */
    @Override
    public int deleteDeviceCountByIds(Long[] ids)
    {
        return deviceCountMapper.deleteDeviceCountByIds(ids);
    }

    /**
     * 删除生产计数信息
     * 
     * @param id 生产计数主键
     * @return 结果
     */
    @Override
    public int deleteDeviceCountById(Long id)
    {
        return deviceCountMapper.deleteDeviceCountById(id);
    }


    /**
     * 生产数据模拟
     */
    @Override
    public void deviceCountDataSimulation(){
        DeviceCount deviceCount = new DeviceCount(null,"测试设备",count,null,new Date());count++;
        deviceCountMapper.insertDeviceCount(deviceCount);
    }

}
