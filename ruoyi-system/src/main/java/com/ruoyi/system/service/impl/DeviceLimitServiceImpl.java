package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.service.IDeviceLimitService;

/**
 * 限制Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-18
 */
@Service
public class DeviceLimitServiceImpl implements IDeviceLimitService 
{
    @Autowired
    private DeviceLimitMapper deviceLimitMapper;

    /**
     * 查询限制
     * 
     * @param id 限制主键
     * @return 限制
     */
    @Override
    public DeviceLimit selectDeviceLimitById(Long id)
    {
        return deviceLimitMapper.selectDeviceLimitById(id);
    }

    /**
     * 查询限制列表
     * 
     * @param deviceLimit 限制
     * @return 限制
     */
    @Override
    public List<DeviceLimit> selectDeviceLimitList(DeviceLimit deviceLimit)
    {
        return deviceLimitMapper.selectDeviceLimitList(deviceLimit);
    }

    /**
     * 新增限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    @Override
    public int insertDeviceLimit(DeviceLimit deviceLimit)
    {
        return deviceLimitMapper.insertDeviceLimit(deviceLimit);
    }

    /**
     * 修改限制
     * 
     * @param deviceLimit 限制
     * @return 结果
     */
    @Override
    public int updateDeviceLimit(DeviceLimit deviceLimit)
    {
        return deviceLimitMapper.updateDeviceLimit(deviceLimit);
    }

    /**
     * 批量删除限制
     * 
     * @param ids 需要删除的限制主键
     * @return 结果
     */
    @Override
    public int deleteDeviceLimitByIds(Long[] ids)
    {
        return deviceLimitMapper.deleteDeviceLimitByIds(ids);
    }

    /**
     * 删除限制信息
     * 
     * @param id 限制主键
     * @return 结果
     */
    @Override
    public int deleteDeviceLimitById(Long id)
    {
        return deviceLimitMapper.deleteDeviceLimitById(id);
    }

    @Override
    public List<DeviceLimit> selectDeviceLimitByDeviceNo(Long id,String deviceNo) {
        DeviceLimit deviceLimit = new DeviceLimit();
        deviceLimit.setDeviceNo(deviceNo);
        deviceLimit.setId(id);
        return deviceLimitMapper.selectDeviceLimitListByIdAndName(deviceLimit);
    }
}
