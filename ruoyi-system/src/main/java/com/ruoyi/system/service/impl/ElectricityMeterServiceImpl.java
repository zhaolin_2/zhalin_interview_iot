package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.domain.ElectricityRate;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.TcpDataHandler;
import com.ruoyi.system.tcp.DataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ElectricityMeterMapper;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.service.IElectricityMeterService;

/**
 * 电表Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-07
 */
@Service
@Slf4j
public class ElectricityMeterServiceImpl implements IElectricityMeterService 
{
    @Autowired
    private ElectricityMeterMapper electricityMeterMapper;

    @Autowired
    TcpDataHandler tcpDataHandler;

    @Autowired
    DeviceLimitMapper deviceLimitMapper;

    @Autowired
    DeviceAlertMapper c;

    /**
     * 静态Map类，用于存放电压，电流，有功功率，频率，总有功电量，拉合闸状态 元素都集齐时，再进行一次完整的更新
     */
    public static Map<String,Double> electricity_meter_allMap =new HashMap<>();




    /**
     * 查询电表
     * 
     * @param id 电表主键
     * @return 电表
     */
    @Override
    public ElectricityMeter selectElectricityMeterById(Integer id)
    {
        return electricityMeterMapper.selectElectricityMeterById(id);
    }

    /**
     * 查询电表列表
     * 
     * @param electricityMeter 电表
     * @return 电表
     */
    @Override
    public List<ElectricityMeter> selectElectricityMeterList(ElectricityMeter electricityMeter)
    {
        return electricityMeterMapper.selectElectricityMeterList(electricityMeter);
    }

    /**
     * 新增电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    @Override
    public int insertElectricityMeter(ElectricityMeter electricityMeter)
    {
        return electricityMeterMapper.insertElectricityMeter(electricityMeter);
    }

    /**
     * 修改电表
     * 
     * @param electricityMeter 电表
     * @return 结果
     */
    @Override
    public int updateElectricityMeter(ElectricityMeter electricityMeter)
    {
        return electricityMeterMapper.updateElectricityMeter(electricityMeter);
    }

    /**
     * 批量删除电表
     * 
     * @param ids 需要删除的电表主键
     * @return 结果
     */
    @Override
    public int deleteElectricityMeterByIds(Integer[] ids)
    {
        return electricityMeterMapper.deleteElectricityMeterByIds(ids);
    }

    /**
     * 删除电表信息
     * 
     * @param id 电表主键
     * @return 结果
     */

    @Override
    public int deleteElectricityMeterById(Integer id)
    {
        return electricityMeterMapper.deleteElectricityMeterById(id);
    }



    /**
     * 通过tcp向客户端发送询问 电表的电压 电流 有功功率
     */
//    @Scheduled(cron = "20 0/4 * * * ?")
    public void sendMessageForAUAp() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "020400000014F036";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }

    }

    /**
     * 通过tcp向客户端发送询问 功率因数
     */
//    @Scheduled(cron = "40 0/4 * * * ?")
    public void sendMessageForPF() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "0204002A00025030";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    /**
     * 通过tcp向客户端发送询问 频率
     */
//    @Scheduled(cron = "50 0/4 * * * ?")
    public void sendMessageForFrequency() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "0204003600035036";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    /**
     * 通过tcp向客户端发送询问 总有功电量
     */
//    @Scheduled(cron = "10 1/4 * * * ?")
    public void sendMessageForAE() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "020401000004F006";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    /**
     * 通过tcp向客户端发送询问 拉闸合闸状态
     */
//    @Scheduled(cron = "20 1/4 * * * ?")
    public void sendMessageForOpenState() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "0204006400017026";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    @Override
    public void AUApRecord(String msg) {
        Map<String, Double> stringDoubleMap = tcpDataHandler.receivedAUApHex2String(msg);
        electricity_meter_allMap.put("电压",stringDoubleMap.get("电压"));
        electricity_meter_allMap.put("电流",stringDoubleMap.get("电流"));
        electricity_meter_allMap.put("有功功率",stringDoubleMap.get("有功功率"));
        if (electricity_meter_allMap.size()==7){
            ElectricityMeter electricityMeter = this.updateIfStateOK();
            electricityMeterMapper.insertElectricityMeter(electricityMeter);
            //电费信息同步 更新
            ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
            bean.electricityRateUpdate(electricityMeter);
        }
    }

    @Override
    public void PfRecord(String msg) {
        Double powerFactor = tcpDataHandler.receivedFPHex2String(msg);
        electricity_meter_allMap.put("功率因数",powerFactor);
        if (electricity_meter_allMap.size()==7){
            ElectricityMeter electricityMeter = this.updateIfStateOK();
            electricityMeterMapper.insertElectricityMeter(electricityMeter);
            //电费信息同步 更新
            ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
            bean.electricityRateUpdate(electricityMeter);
        }
    }

    @Override
    public void frequencyRecord(String msg) {
        Double frequency = tcpDataHandler.receivedFrequencyHex2String(msg);
        electricity_meter_allMap.put("频率",frequency);
        if (electricity_meter_allMap.size()==7){
            ElectricityMeter electricityMeter = this.updateIfStateOK();
            electricityMeterMapper.insertElectricityMeter(electricityMeter);
            //电费信息同步 更新
            ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
            bean.electricityRateUpdate(electricityMeter);
        }
    }

    @Override
    public void AERecord(String msg) {
        Double activeElectricity = tcpDataHandler.receivedAEHex2String(msg);
        electricity_meter_allMap.put("总有功电量",activeElectricity);
        if (electricity_meter_allMap.size()==7){
            ElectricityMeter electricityMeter = this.updateIfStateOK();
            electricityMeterMapper.insertElectricityMeter(electricityMeter);
            //电费信息同步 更新
            ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
            bean.electricityRateUpdate(electricityMeter);
        }
    }

    @Override
    public void stateRecord(String msg) {
        Double stateD = tcpDataHandler.receivedStateHex2String(msg).doubleValue();
        electricity_meter_allMap.put("拉合闸状态",stateD);
        if (electricity_meter_allMap.size()==7){
            ElectricityMeter electricityMeter = this.updateIfStateOK();
            electricityMeterMapper.insertElectricityMeter(electricityMeter);
            //电费信息同步 更新
            ElectricityRateServiceImpl bean = SpringUtils.getBean(ElectricityRateServiceImpl.class);
            bean.electricityRateUpdate(electricityMeter);
        }
    }


    private static ElectricityMeter updateIfStateOK(){
            ElectricityMeter electricityMeter = new ElectricityMeter();
            electricityMeter.setEmA(electricity_meter_allMap.get("电流"));
            electricityMeter.setEmU(electricity_meter_allMap.get("电压"));
            electricityMeter.setEmFrequency(electricity_meter_allMap.get("频率"));
            electricityMeter.setEmActivePower(electricity_meter_allMap.get("有功功率"));
            electricityMeter.setEmState(electricity_meter_allMap.get("拉合闸状态").intValue());
            electricityMeter.setEmPowerFactor(electricity_meter_allMap.get("功率因数"));
            electricityMeter.setEmActiveElectricity(electricity_meter_allMap.get("总有功电量"));
            electricityMeter.setRecordTime(new Date());
        DeviceLimitMapper bean = SpringUtils.getBean(DeviceLimitMapper.class);
        DeviceAlertMapper beanAlert = SpringUtils.getBean(DeviceAlertMapper.class);
        DeviceLimit deviceLimitA = bean.selectDeviceLimitByCheckValue("em_a");
        DeviceLimit deviceLimitU = bean.selectDeviceLimitByCheckValue("em_u");
        DeviceLimit deviceLimitAP = bean.selectDeviceLimitByCheckValue("em_active_power");

        if (electricityMeter.getEmA() > deviceLimitA.getLimitUpper()){
            deviceLimitA.setDeviceState("电流过高");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表电流过高",electricityMeter.getEmA().toString(),"未处理",new Date()));
        }else if (electricityMeter.getEmA() < deviceLimitA.getLimitLower()){
            deviceLimitA.setDeviceState("电流过低");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表电流过低",electricityMeter.getEmA().toString(),"未处理",new Date()));
        }else{
            deviceLimitA.setDeviceState("正常");
        }

        if (electricityMeter.getEmU() > deviceLimitU.getLimitUpper()){
            deviceLimitU.setDeviceState("电压过高");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表电压过高",electricityMeter.getEmU().toString(),"未处理",new Date()));
        }else if (electricityMeter.getEmU() < deviceLimitU.getLimitLower()){
            deviceLimitU.setDeviceState("电压过低");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表电压过低",electricityMeter.getEmU().toString(),"未处理",new Date()));
        }else{
            deviceLimitU.setDeviceState("正常");
        }

        if (electricityMeter.getEmActivePower() > deviceLimitAP.getLimitUpper()){
            deviceLimitAP.setDeviceState("功率过高");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表功率过高",electricityMeter.getEmActivePower().toString(),"未处理",new Date()));
        }else if (electricityMeter.getEmActivePower() < deviceLimitAP.getLimitLower()){
            deviceLimitAP.setDeviceState("功率过低");
            beanAlert.insertDeviceAlert(new DeviceAlert(null,"电表","电表功率过低",electricityMeter.getEmActivePower().toString(),"未处理",new Date()));
        }else{
            deviceLimitAP.setDeviceState("正常");
        }

        //添加报警信息详情内容
        bean.updateDeviceLimit(deviceLimitA);
        bean.updateDeviceLimit(deviceLimitU);
        bean.updateDeviceLimit(deviceLimitAP);
        electricity_meter_allMap.clear();
        return electricityMeter;
    }

    @Override
    public List<ElectricityMeter> selectElectricityListByDate(Date selectTime){
        List a = new ArrayList();
        List ae = new ArrayList();
        List time = new ArrayList();
        List all = new ArrayList();
        List<ElectricityMeter> electricityMeters = electricityMeterMapper.selectElectricityListByTime(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        for (int i = 0; i < electricityMeters.size(); i++) {
            ElectricityMeter electricityMeter =  electricityMeters.get(i);
            a.add(electricityMeter.getEmA());
            ae.add(electricityMeter.getEmActivePower());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(electricityMeter.getRecordTime()));
        }
        Collections.addAll(all,a,ae,time);
        return all;
    }

    @Override
    public ElectricityMeter selectLastElectricity(Date selectDate){
        ElectricityMeter electricityMeter = electricityMeterMapper.selectLastElectricityByDate(new SimpleDateFormat("yyyy-MM-dd").format(selectDate));
        return electricityMeter;
    }

    @Override
    public ElectricityMeter selectElectricityByHourTime(String selectHourTime){
        ElectricityMeter electricityMeter = electricityMeterMapper.selectElectricityMeterDataByHour(selectHourTime);
        return electricityMeter;
    }

}
