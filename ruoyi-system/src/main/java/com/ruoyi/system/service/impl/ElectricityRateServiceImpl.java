package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.request.dataIntegration.ElectricPowerRateRequest;
import com.ruoyi.system.mapper.ElectricityMeterMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ElectricityRateMapper;
import com.ruoyi.system.domain.ElectricityRate;
import com.ruoyi.system.service.IElectricityRateService;

/**
 * 电费Service业务层处理
 * 
 * @author Lin
 * @date 2023-03-03
 */
@Service
public class ElectricityRateServiceImpl implements IElectricityRateService 
{
    @Autowired
    private ElectricityRateMapper electricityRateMapper;

    @Autowired
    private ElectricityMeterMapper electricityMeterMapper;

    /**
     * 查询电费
     * 
     * @param id 电费主键
     * @return 电费
     */
    @Override
    public ElectricityRate selectElectricityRateById(Long id)
    {
        return electricityRateMapper.selectElectricityRateById(id);
    }

    /**
     * 查询电费列表
     * 
     * @param electricityRate 电费
     * @return 电费
     */
    @Override
    public List<ElectricityRate> selectElectricityRateList(ElectricityRate electricityRate)
    {
        return electricityRateMapper.selectElectricityRateList(electricityRate);
    }

    /**
     * 新增电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    @Override
    public int insertElectricityRate(ElectricityRate electricityRate)
    {
        return electricityRateMapper.insertElectricityRate(electricityRate);
    }

    /**
     * 修改电费
     * 
     * @param electricityRate 电费
     * @return 结果
     */
    @Override
    public int updateElectricityRate(ElectricityRate electricityRate)
    {
        return electricityRateMapper.updateElectricityRate(electricityRate);
    }

    /**
     * 批量删除电费
     * 
     * @param ids 需要删除的电费主键
     * @return 结果
     */
    @Override
    public int deleteElectricityRateByIds(Long[] ids)
    {
        return electricityRateMapper.deleteElectricityRateByIds(ids);
    }

    /**
     * 删除电费信息
     * 
     * @param id 电费主键
     * @return 结果
     */
    @Override
    public int deleteElectricityRateById(Long id)
    {
        return electricityRateMapper.deleteElectricityRateById(id);
    }


    /**
     * 该方法用于自动生成新的电费记录数据
     * 定时任务在每天的0点0分开始生成
     * 后续数据更新该记录
     */
    public void newDailyRateRecordGenerate(){
        //生成空记录，并且设置初始值
        ElectricityRate electricityRate = new ElectricityRate(null,0.0,0.0,0.0,0.0,0.0,new Date());
        electricityRateMapper.insertElectricityRate(electricityRate);
    }


    /**
     * 该方法用于更新当前电费使用，更新逻辑基于 newDailyRateRecordGenerate() 方法
     * newDailyRateRecordGenerate() 方法将会在每日零点新增一个空记录 而本方法将会定时按照每日生成的空记录更新
     * 根据电表设备回传的能耗信息进行整理 整理后更新到表中
     * @electricityMeter 该参数为从设备网关中传回的电表信息 从中取出能耗信息提供使用
     */
    @Override
    public void electricityRateUpdate(ElectricityMeter electricityMeter){
        try {
            //构造当日的日期格式字符串：年月日
            String dateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            //从数据库中查询本日记录（由自动生成方法：newDailyRateRecordGenerate 提供）
            ElectricityRate electricityRate = electricityRateMapper.selectLatestRecord();

            electricityRate.setRecordTime(new Date());

            int hour = DateUtil.hour(electricityMeter.getRecordTime(), true);
            if (hour >= 0 & hour < 10) {
                //当传入数据在0点-10点之间，平，电价：0.7038元/kwh
                //查找出本时段第一条数据作为数据的基准数
                ElectricityMeter electricityMeter00 = electricityMeterMapper.selectElectricityMeterDataByHour(dateString + " 00");
                //更新电费记录表的内容
                electricityRate.setElectricityRateCommon(0.7038 * (electricityMeter.getEmActiveElectricity() - electricityMeter00.getEmActiveElectricity()));
                electricityRate.setElectricityRateAll(electricityRate.getElectricityRateLow() + electricityRate.getElectricityRateCommon() + electricityRate.getElectricityRateHigh() + electricityRate.getElectricityRateTop());
                electricityRateMapper.updateElectricityRate(electricityRate);
            } else if (hour >= 10 & hour < 15) {
                //当传入数据在10点-15点之间，谷，电价：0.3699元/kwh
                //查找出本时段第一条数据作为数据的基准数
                ElectricityMeter electricityMeter10 = electricityMeterMapper.selectElectricityMeterDataByHour(dateString + " 10");
                //更新电费记录表的内容
                electricityRate.setElectricityRateLow(0.3699 * (electricityMeter.getEmActiveElectricity() - electricityMeter10.getEmActiveElectricity()));
                electricityRate.setElectricityRateAll(electricityRate.getElectricityRateLow() + electricityRate.getElectricityRateCommon() + electricityRate.getElectricityRateHigh() + electricityRate.getElectricityRateTop());
                electricityRateMapper.updateElectricityRate(electricityRate);
            } else if (hour >= 15 & hour < 17) {
                //当传入数据在15点-17点之间，平，电价：0.7038元/kwh
                //查找出本时段第一条数据作为数据的基准数
                ElectricityMeter electricityMeter15 = electricityMeterMapper.selectElectricityMeterDataByHour(dateString + " 15");
                //更新电费记录表的内容
                electricityRate.setElectricityRateCommon(0.7038 * (electricityMeter.getEmActiveElectricity() - electricityMeter15.getEmActiveElectricity()));
                electricityRate.setElectricityRateAll(electricityRate.getElectricityRateLow() + electricityRate.getElectricityRateCommon() + electricityRate.getElectricityRateHigh() + electricityRate.getElectricityRateTop());
                electricityRateMapper.updateElectricityRate(electricityRate);
            } else if (hour >= 17 & hour < 22) {
                //当传入数据在17点-22点之间，峰，电价：1.0577元/kwh
                //查找出本时段第一条数据作为数据的基准数
                ElectricityMeter electricityMeter17 = electricityMeterMapper.selectElectricityMeterDataByHour(dateString + " 17");
                //更新电费记录表的内容
                electricityRate.setElectricityRateHigh(1.0577 * (electricityMeter.getEmActiveElectricity() - electricityMeter17.getEmActiveElectricity()));
                electricityRate.setElectricityRateAll(electricityRate.getElectricityRateLow() + electricityRate.getElectricityRateCommon() + electricityRate.getElectricityRateHigh() + electricityRate.getElectricityRateTop());
                electricityRateMapper.updateElectricityRate(electricityRate);
            } else if (hour >= 22) {
                //当传入数据在22点之后，平，电价：0.7038元/kwh
                //查找出本时段第一条数据作为数据的基准数
                ElectricityMeter electricityMeter22 = electricityMeterMapper.selectElectricityMeterDataByHour(dateString + " 22");
                //更新电费记录表的内容
                electricityRate.setElectricityRateCommon(0.7038 * (electricityMeter.getEmActiveElectricity() - electricityMeter22.getEmActiveElectricity()));
                electricityRate.setElectricityRateAll(electricityRate.getElectricityRateLow() + electricityRate.getElectricityRateCommon() + electricityRate.getElectricityRateHigh() + electricityRate.getElectricityRateTop());
                electricityRateMapper.updateElectricityRate(electricityRate);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 该方法返回 电费相关的request 内容包含电费以及当前的电费计算状态
     * @return
     */
    @Override
    public ElectricPowerRateRequest electricRateData(){

        ElectricPowerRateRequest electricPowerRateRequest = new ElectricPowerRateRequest();

        ElectricityRate electricityRate = electricityRateMapper.selectLatestRecord();
        //电费部分copy进
        BeanUtils.copyProperties(electricityRate,electricPowerRateRequest);
        //首先取出当前时间,并且拿到当前属于的时间范围
        Date date = new Date();
        int hour = DateUtil.hour(date, true);
        if (hour >= 0 & hour < 10) {
            electricPowerRateRequest.setElectricityRate(0.7038);
            electricPowerRateRequest.setElectricityRateType("平值电费");
        } else if (hour >= 10 & hour < 15) {
            electricPowerRateRequest.setElectricityRate(0.3699);
            electricPowerRateRequest.setElectricityRateType("谷值电费");
        } else if (hour >= 15 & hour < 17) {
            electricPowerRateRequest.setElectricityRate(0.7038);
            electricPowerRateRequest.setElectricityRateType("平值电费");
        } else if (hour >= 17 & hour < 22) {
            electricPowerRateRequest.setElectricityRate(1.0577);
            electricPowerRateRequest.setElectricityRateType("峰值电费");
        } else if (hour >= 22) {
            electricPowerRateRequest.setElectricityRate(0.7038);
            electricPowerRateRequest.setElectricityRateType("平值电费");
        }
       //查询最新一条能耗数据
        ElectricityMeter electricityMeter = electricityMeterMapper.selectLatestElectricityMeter();
        //查询本日最初一条数据电表数据
        ElectricityMeter electricityMeter1 = electricityMeterMapper.selectElectricityMeterDataByHour(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00");
        electricPowerRateRequest.setActiveElectricity(NumberUtil.round(electricityMeter.getEmActiveElectricity()-electricityMeter1.getEmActiveElectricity(),3).doubleValue());
        return electricPowerRateRequest;
    }


    /**
     * 提供电费曲线，默认返回七天
     */
    @Override
    public List selectRateRecordForLine(Integer count){
        List rate = new ArrayList();
        List time = new ArrayList();
        List line = new ArrayList();
        List<ElectricityRate> electricityRates = electricityRateMapper.selectRateRecordForLine(count);
        for (int i = electricityRates.size()-1; i >= 0; i--) {
            ElectricityRate electricityRate =  electricityRates.get(i);
            rate.add(electricityRate.getElectricityRateAll());
            time.add(new SimpleDateFormat("yyyy-MM-dd").format(electricityRate.getRecordTime()));
        }
        Collections.addAll(line,rate,time);
        return line;
    }
 }
