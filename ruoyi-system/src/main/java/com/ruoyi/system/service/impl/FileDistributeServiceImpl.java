package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FileDistributeMapper;
import com.ruoyi.system.domain.FileDistribute;
import com.ruoyi.system.service.IFileDistributeService;

/**
 * 文件分发DemoService业务层处理
 * 
 * @author Lin
 * @date 2022-11-21
 */
@Service
public class FileDistributeServiceImpl implements IFileDistributeService 
{
    @Autowired
    private FileDistributeMapper fileDistributeMapper;

    /**
     * 查询文件分发Demo
     * 
     * @param id 文件分发Demo主键
     * @return 文件分发Demo
     */
    @Override
    public FileDistribute selectFileDistributeById(Long id)
    {
        return fileDistributeMapper.selectFileDistributeById(id);
    }

    /**
     * 查询文件分发Demo列表
     * 
     * @param fileDistribute 文件分发Demo
     * @return 文件分发Demo
     */
    @Override
    public List<FileDistribute> selectFileDistributeList(FileDistribute fileDistribute)
    {
        return fileDistributeMapper.selectFileDistributeList(fileDistribute);
    }

    /**
     * 新增文件分发Demo
     * 
     * @param fileDistribute 文件分发Demo
     * @return 结果
     */
    @Override
    public int insertFileDistribute(FileDistribute fileDistribute)
    {
        return fileDistributeMapper.insertFileDistribute(fileDistribute);
    }

    /**
     * 修改文件分发Demo
     * 
     * @param fileDistribute 文件分发Demo
     * @return 结果
     */
    @Override
    public int updateFileDistribute(FileDistribute fileDistribute)
    {
        return fileDistributeMapper.updateFileDistribute(fileDistribute);
    }

    /**
     * 批量删除文件分发Demo
     * 
     * @param ids 需要删除的文件分发Demo主键
     * @return 结果
     */
    @Override
    public int deleteFileDistributeByIds(Long[] ids)
    {
        return fileDistributeMapper.deleteFileDistributeByIds(ids);
    }

    /**
     * 删除文件分发Demo信息
     * 
     * @param id 文件分发Demo主键
     * @return 结果
     */
    @Override
    public int deleteFileDistributeById(Long id)
    {
        return fileDistributeMapper.deleteFileDistributeById(id);
    }

    /**
     * 给文件生存周期的文件减少时间 等于0 的文件不会再减少，也不会再在接收人处显示
     */
    public void reduceFileLife(){
        //查询所有文件列表
        List<FileDistribute> fileDistributes = fileDistributeMapper.selectFileDistributeList(new FileDistribute());
        for (int i = 0; i < fileDistributes.size(); i++) {
            FileDistribute fileDistribute =  fileDistributes.get(i);
            if (fileDistribute.getFileLife() > 0 ){
                fileDistribute.setFileLife(fileDistribute.getFileLife()-1);
            }
            fileDistributeMapper.updateFileDistribute(fileDistribute);
        }
    }
}
