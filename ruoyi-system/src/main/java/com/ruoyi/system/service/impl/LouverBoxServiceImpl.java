package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.domain.OfficeReport;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.TcpDataHandler;
import com.ruoyi.system.tcp.DataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LouverBoxMapper;
import com.ruoyi.system.domain.LouverBox;
import com.ruoyi.system.service.ILouverBoxService;

/**
 * 百叶箱Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Service
@Slf4j
public class LouverBoxServiceImpl implements ILouverBoxService 
{
    @Autowired
    private LouverBoxMapper louverBoxMapper;

    @Autowired
    TcpDataHandler tcpDataHandler;

    @Autowired
    DeviceLimitMapper deviceLimitMapper;

    @Autowired
    DeviceAlertMapper deviceAlertMapper;

    /**
     * 静态Map类，用于存放温湿度，光照，当温湿度和光照三个元素都集齐时，再进行一次完整的更新
     */
    public static Map<String,Double> LouverBoxAllMap =new HashMap<>();

    /**
     * 查询百叶箱
     * 
     * @param id 百叶箱主键
     * @return 百叶箱
     */
    @Override
    public LouverBox selectLouverBoxById(Integer id)
    {
        return louverBoxMapper.selectLouverBoxById(id);
    }

    /**
     * 查询百叶箱列表
     * 
     * @param louverBox 百叶箱
     * @return 百叶箱
     */
    @Override
    public List<LouverBox> selectLouverBoxList(LouverBox louverBox)
    {
        return louverBoxMapper.selectLouverBoxList(louverBox);
    }

    /**
     * 新增百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    @Override
    public int insertLouverBox(LouverBox louverBox)
    {
        return louverBoxMapper.insertLouverBox(louverBox);
    }

    /**
     * 修改百叶箱
     * 
     * @param louverBox 百叶箱
     * @return 结果
     */
    @Override
    public int updateLouverBox(LouverBox louverBox)
    {
        return louverBoxMapper.updateLouverBox(louverBox);
    }

    /**
     * 批量删除百叶箱
     * 
     * @param ids 需要删除的百叶箱主键
     * @return 结果
     */
    @Override
    public int deleteLouverBoxByIds(Integer[] ids)
    {
        return louverBoxMapper.deleteLouverBoxByIds(ids);
    }

    /**
     * 删除百叶箱信息
     * 
     * @param id 百叶箱主键
     * @return 结果
     */
    @Override
    public int deleteLouverBoxById(Integer id)
    {
        return louverBoxMapper.deleteLouverBoxById(id);
    }

    /**
     * 通过tcp向客户端发送询问 百叶箱式集成传感器_温湿度 消息
     */
//    @Scheduled(cron = "0 1/4 * * * ?")
    public void sendMessageForLouverBoxA() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "010300000002C40B";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }

    }

    /**
     * 通过tcp向客户端发送询问 百叶箱式集成传感器_温湿度 消息
     */
//    @Scheduled(cron = "0 2/4 * * * ?")
    public void sendMessageForLouverBoxB() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "010300070004F5C8";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }

    }


    /**
     * 与温湿度采集器不同，百叶盒的温湿度 光照是分别传进来的，需要暂时存放后，组合在一起再更新
     * 百叶箱集成式传感器数据--温湿度暂时存放，满足条件后更新
     * @param msg
     */
    @Override
    public void humitureRecord(String msg) {
        //从工具类中取出截取处理后的map
        Map<String, Double> utilMapForTempAndHumiture = tcpDataHandler.receivedLouverBoxAHex2Str(msg);
        //把map集合里边的温度，湿度，存放到当前这个类的AllMap里，这个map相当于一个水桶，当水桶集齐了一桶满的水的时候，就进行一次更新
        LouverBoxAllMap.put("温度", utilMapForTempAndHumiture.get("温度"));
        LouverBoxAllMap.put("湿度", utilMapForTempAndHumiture.get("湿度"));
        if (LouverBoxAllMap.get("温度") != null & LouverBoxAllMap.get("湿度") != null & LouverBoxAllMap.get("光照强度") != null) {
            LouverBox louverBox = new LouverBox();
            louverBox.setRecordTime(new Date());
            louverBox.setLbTemperature(LouverBoxAllMap.get("温度"));
            louverBox.setLbHumidity(LouverBoxAllMap.get("湿度"));
            louverBox.setLbIllumination(LouverBoxAllMap.get("光照强度").longValue());

            DeviceLimit deviceLimitTemp = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_temperature");
            DeviceLimit deviceLimitIll = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_illumination");
            DeviceLimit deviceLimitHumi = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_humidity");

            if (louverBox.getLbTemperature() > deviceLimitTemp.getLimitUpper()){
                deviceLimitTemp.setDeviceState("温度过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","温度过高",louverBox.getLbTemperature().toString(),"未处理",new Date()));
            }else if (louverBox.getLbTemperature() < deviceLimitTemp.getLimitLower()){
                deviceLimitTemp.setDeviceState("温度过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","温度过低",louverBox.getLbTemperature().toString(),"未处理",new Date()));
            }else {
                deviceLimitTemp.setDeviceState("正常");
            }

            if (louverBox.getLbHumidity() > deviceLimitHumi.getLimitUpper()){
                deviceLimitHumi.setDeviceState("湿度过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","湿度过高",louverBox.getLbHumidity().toString(),"未处理",new Date()));
            }else if (louverBox.getLbHumidity() < deviceLimitHumi.getLimitLower()){
                deviceLimitHumi.setDeviceState("湿度过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","湿度过低",louverBox.getLbHumidity().toString(),"未处理",new Date()));
            }else{
                deviceLimitHumi.setDeviceState("正常");
            }

            if (louverBox.getLbIllumination() > deviceLimitIll.getLimitUpper()){
                deviceLimitIll.setDeviceState("光强过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","光强过高",louverBox.getLbIllumination().toString(),"未处理",new Date()));
            }else if (louverBox.getLbIllumination() < deviceLimitIll.getLimitLower()){
                deviceLimitIll.setDeviceState("光强过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","光强过低",louverBox.getLbIllumination().toString(),"未处理",new Date()));
            }else{
                deviceLimitIll.setDeviceState("正常");
            }
            louverBoxMapper.insertLouverBox(louverBox);
            deviceLimitMapper.updateDeviceLimit(deviceLimitTemp);
            deviceLimitMapper.updateDeviceLimit(deviceLimitHumi);
            deviceLimitMapper.updateDeviceLimit(deviceLimitIll);
            LouverBoxAllMap.clear();
        }
    }

        /**
         * 百叶箱集成式传感器数据录入方法
         * @param msg
         */
        @Override
        public void illuminationRecord(String msg){
            //从工具类中取出截取处理后的map
            Map<String,Double> utilMapForIllumination = tcpDataHandler.receivedLouverBoxBHex2String(msg);
            //把map集合里边的温度，湿度，存放到当前这个类的AllMap里，这个map相当于一个水桶，当水桶集齐了一桶满的水的时候，就进行一次更新
            LouverBoxAllMap.put("光照强度",utilMapForIllumination.get("光照强度"));
            if (LouverBoxAllMap.get("温度") != null & LouverBoxAllMap.get("湿度") != null & LouverBoxAllMap.get("光照强度") != null) {
                LouverBox louverBox = new LouverBox();
                louverBox.setRecordTime(new Date());
                louverBox.setLbTemperature(LouverBoxAllMap.get("温度"));
                louverBox.setLbHumidity(LouverBoxAllMap.get("湿度"));
                louverBox.setLbIllumination(LouverBoxAllMap.get("光照强度").longValue());

                DeviceLimit deviceLimitTemp = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_temperature");
                DeviceLimit deviceLimitIll = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_illumination");
                DeviceLimit deviceLimitHumi = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_humidity");

                if (louverBox.getLbTemperature() > deviceLimitTemp.getLimitUpper()){
                    deviceLimitTemp.setDeviceState("温度过高");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","温度过高",louverBox.getLbTemperature().toString(),"未处理",new Date()));
                }else if (louverBox.getLbTemperature() < deviceLimitTemp.getLimitLower()){
                    deviceLimitTemp.setDeviceState("温度过低");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","温度过低",louverBox.getLbTemperature().toString(),"未处理",new Date()));
                }else {
                    deviceLimitTemp.setDeviceState("正常");
                }

                if (louverBox.getLbHumidity() > deviceLimitHumi.getLimitUpper()){
                    deviceLimitHumi.setDeviceState("湿度过高");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","湿度过高",louverBox.getLbHumidity().toString(),"未处理",new Date()));
                }else if (louverBox.getLbHumidity() < deviceLimitHumi.getLimitLower()){
                    deviceLimitHumi.setDeviceState("湿度过低");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","湿度过低",louverBox.getLbHumidity().toString(),"未处理",new Date()));
                }else{
                    deviceLimitHumi.setDeviceState("正常");
                }

                if (louverBox.getLbIllumination() > deviceLimitIll.getLimitUpper()){
                    deviceLimitIll.setDeviceState("光强过高");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","光强过高",louverBox.getLbIllumination().toString(),"未处理",new Date()));
                }else if (louverBox.getLbIllumination() < deviceLimitIll.getLimitLower()){
                    deviceLimitIll.setDeviceState("光强过高");
                    deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"百叶箱式集成传感器","光强过低",louverBox.getLbIllumination().toString(),"未处理",new Date()));
                }else{
                    deviceLimitIll.setDeviceState("正常");
                }
                louverBoxMapper.insertLouverBox(louverBox);
                deviceLimitMapper.updateDeviceLimit(deviceLimitTemp);
                deviceLimitMapper.updateDeviceLimit(deviceLimitHumi);
                deviceLimitMapper.updateDeviceLimit(deviceLimitIll);
                LouverBoxAllMap.clear();
            }

    }

    /**
     * 用于查询百叶箱集成式采集器 温湿度的曲线数组方法
     * @param selectTime 查询时间
     * @return
     */
    @Override
    public List selectLouverHumitureList(Date selectTime){
        List temperature = new ArrayList();
        List humidity = new ArrayList();
        List time = new ArrayList();
        List all =new ArrayList();
        List<LouverBox> louverBoxes = louverBoxMapper.selectLouverHumitureList(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        for (int i = 0; i < louverBoxes.size(); i++) {
            LouverBox louverBox =  louverBoxes.get(i);
            temperature.add(louverBox.getLbTemperature());
            humidity.add(louverBox.getLbHumidity());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(louverBox.getRecordTime()));
        }
        Collections.addAll(all,temperature,humidity,time);
        return all;
    }

    /**
     * 用于查询百叶箱集成式采集器 光照强度的曲线数组方法
     * @param selectTime 查询时间
     * @return
     */
    @Override
    public List selectIlluminationList(Date selectTime) {
        List illumination = new ArrayList();
        List time = new ArrayList();
        List all =new ArrayList();
        List<LouverBox> louverBoxes = louverBoxMapper.selectLouverHumitureList(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        for (int i = 0; i < louverBoxes.size(); i++) {
            LouverBox louverBox =  louverBoxes.get(i);
            illumination.add(louverBox.getLbIllumination());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(louverBox.getRecordTime()));
        }
        Collections.addAll(all,illumination,time);
        return all;
    }

    @Override
    public OfficeReport rangeOfHumitureIllData(Date selectTime){
        OfficeReport officeReport = louverBoxMapper.rangeOfHumitureIllData(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        return officeReport;
    }




}
