package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.request.dataIntegration.DeviceLatestDataRequest;
import com.ruoyi.system.domain.request.dataIntegration.DeviceStateRequest;
import com.ruoyi.system.domain.request.dataIntegration.StockStateRequest;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IOfficeDeviceService;

/**
 * 设备Service业务层处理
 *
 * @author Lin
 * @date 2022-11-18
 */
@Service
public class OfficeDeviceServiceImpl implements IOfficeDeviceService {
    @Autowired
    private OfficeDeviceMapper officeDeviceMapper;

    @Autowired
    private OfficeStockMapper officeStockMapper;

    @Autowired
    private ElectricityMeterMapper electricityMeterMapper;

    @Autowired
    private LouverBoxMapper louverBoxMapper;

    @Autowired
    private SmokeDetectorMapper smokeDetectorMapper;

    @Autowired
    private DeviceLimitMapper deviceLimitMapper;

    /**
     * 查询设备
     *
     * @param id 设备主键
     * @return 设备
     */
    @Override
    public OfficeDevice selectOfficeDeviceById(Integer id) {
        return officeDeviceMapper.selectOfficeDeviceById(id);
    }

    /**
     * 查询设备列表
     *
     * @param officeDevice 设备
     * @return 设备
     */
    @Override
    public List<OfficeDevice> selectOfficeDeviceList(OfficeDevice officeDevice) {
        return officeDeviceMapper.selectOfficeDeviceList(officeDevice);
    }

    /**
     * 新增设备
     *
     * @param officeDevice 设备
     * @return 结果
     */
    @Override
    public int insertOfficeDevice(OfficeDevice officeDevice) {
        return officeDeviceMapper.insertOfficeDevice(officeDevice);
    }

    /**
     * 修改设备
     *
     * @param officeDevice 设备
     * @return 结果
     */
    @Override
    public int updateOfficeDevice(OfficeDevice officeDevice) {
        return officeDeviceMapper.updateOfficeDevice(officeDevice);
    }

    /**
     * 批量删除设备
     *
     * @param ids 需要删除的设备主键
     * @return 结果
     */
    @Override
    public int deleteOfficeDeviceByIds(Integer[] ids) {
        return officeDeviceMapper.deleteOfficeDeviceByIds(ids);
    }

    /**
     * 删除设备信息
     *
     * @param id 设备主键
     * @return 结果
     */
    @Override
    public int deleteOfficeDeviceById(Integer id) {
        return officeDeviceMapper.deleteOfficeDeviceById(id);
    }

    @Override
    public DeviceStateRequest deviceStateRequest() {
        List<OfficeDevice> officeDevices = officeDeviceMapper.selectOfficeDeviceList(new OfficeDevice());
        return new DeviceStateRequest(officeDevices.size(), officeDevices.size(), "正常");
    }

    @Override
    public StockStateRequest stockStateRequest() {
        //库存不足的库存数量
        Integer stockWarnNum = 0;

        //遍历库存
        List<OfficeStock> officeStocks = officeStockMapper.selectOfficeStockList(new OfficeStock());
        for (int i = 0; i < officeStocks.size(); i++) {
            OfficeStock officeStock = officeStocks.get(i);
            if (officeStock.getStockNum() <= 0) {
                stockWarnNum++;
            }
        }

        //回传前端的对象
        StockStateRequest stockStateRequest = new StockStateRequest(officeStocks.size(), stockWarnNum);
        return stockStateRequest;
    }

    /**
     * 当前所有设备最新数据返回信息
     */
    @Override
    public List<DeviceLatestDataRequest> deviceLatestDataRequest() {
        List<DeviceLatestDataRequest> deviceLatestDataRequests = new ArrayList<>();
        //电表：电流 电压 有功功率 时间
        ElectricityMeter electricityMeter = electricityMeterMapper.selectLatestElectricityMeter();
        //百叶箱集成传感器： 温度 湿度 光照
        LouverBox louverBox = louverBoxMapper.selectLatestLouverBox();
        //烟感：是否触发
        SmokeDetector smokeDetector = smokeDetectorMapper.selectLatestSmokeDetector();

        DeviceLatestDataRequest i = new DeviceLatestDataRequest("电表：电流",electricityMeter.getEmA()+"A",electricityMeter.getRecordTime(),null);
        DeviceLatestDataRequest u = new DeviceLatestDataRequest("电表：电压",electricityMeter.getEmU()+"V",electricityMeter.getRecordTime(),null);
        DeviceLatestDataRequest ap = new DeviceLatestDataRequest("电表：有功功率",electricityMeter.getEmActivePower()+"kw",electricityMeter.getRecordTime(),null);
        DeviceLatestDataRequest temperature = new DeviceLatestDataRequest("集成传感器测试：温度",louverBox.getLbTemperature()+"℃",louverBox.getRecordTime(),null);
        DeviceLatestDataRequest humidity = new DeviceLatestDataRequest("集成传感器测试：湿度",louverBox.getLbHumidity()+"%",louverBox.getRecordTime(),null);
        DeviceLatestDataRequest illumination = new DeviceLatestDataRequest("集成传感器测试：光照",louverBox.getLbIllumination()+"流明",louverBox.getRecordTime(),null);
        DeviceLatestDataRequest detector = new DeviceLatestDataRequest("烟雾报警器",smokeDetector.getDetectorState().toString(),smokeDetector.getRecordTime(),null);
        String em_a = deviceLimitMapper.selectDeviceLimitByCheckValue("em_a").getDeviceState();
        String em_u = deviceLimitMapper.selectDeviceLimitByCheckValue("em_u").getDeviceState();
        String em_active_power = deviceLimitMapper.selectDeviceLimitByCheckValue("em_active_power").getDeviceState();
        String lb_temperature = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_temperature").getDeviceState();
        String lb_humidity = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_humidity").getDeviceState();
        String lb_illumination = deviceLimitMapper.selectDeviceLimitByCheckValue("lb_illumination").getDeviceState();
        String detector_state = deviceLimitMapper.selectDeviceLimitByCheckValue("detector_state").getDeviceState();
        if (DateUtil.between(electricityMeter.getRecordTime() , new Date(),DateUnit.MINUTE) > 4){
            em_a = em_a + "；数据响应超时";
            em_u = em_u + "；数据响应超时";
            em_active_power = em_active_power + "；数据响应超时";
        }else{
            em_a = em_a + ";数据响应正常";
            em_u = em_u + ";数据响应正常";
            em_active_power = em_active_power + ";数据响应正常";
        }

        if (DateUtil.between(louverBox.getRecordTime() , new Date(),DateUnit.MINUTE) > 4){
            lb_temperature = lb_temperature + "；数据响应超时";
            lb_humidity = lb_humidity + "；数据响应超时";
            lb_illumination = lb_illumination + "；数据响应超时";
        }else{
            lb_temperature = lb_temperature + ";数据响应正常";
            lb_humidity = lb_humidity + ";数据响应正常";
            lb_illumination = lb_illumination + ";数据响应正常";
        }

        if (DateUtil.between(smokeDetector.getRecordTime() , new Date(),DateUnit.MINUTE) > 4){
            detector_state = detector_state + "；数据响应超时";
        }else{
            detector_state = detector_state+ ";数据响应正常";
        }
        i.setDateState(em_a);
        u.setDateState(em_u);
        ap.setDateState(em_active_power);
        temperature.setDateState(lb_temperature);
        humidity.setDateState(lb_humidity);
        illumination.setDateState(lb_illumination);
        detector.setDateState(detector_state);

        Collections.addAll(deviceLatestDataRequests,i,u,ap,temperature,humidity,illumination,detector);
        return deviceLatestDataRequests;
    }


}
