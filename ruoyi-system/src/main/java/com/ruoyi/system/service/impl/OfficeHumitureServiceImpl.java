package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import cn.hutool.core.util.NumberUtil;
import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.TcpDataHandler;
import com.ruoyi.common.utils.math.MathUtil;
import com.ruoyi.system.domain.request.OfficeHumitureRequest;
import com.ruoyi.system.tcp.DataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OfficeHumitureMapper;
import com.ruoyi.system.domain.OfficeHumiture;
import com.ruoyi.system.service.IOfficeHumitureService;

/**
 * humitureService业务层处理
 * 
 * @author Lin
 * @date 2022-10-31
 */
@Service
@Slf4j
public class OfficeHumitureServiceImpl implements IOfficeHumitureService 
{
    @Autowired
    private OfficeHumitureMapper officeHumitureMapper;

    @Autowired
    private DeviceLimitMapper deviceLimitMapper;

    @Autowired
    private DeviceAlertMapper deviceAlertMapper;

    /**
     * 查询humiture
     * 
     * @param id humiture主键
     * @return humiture
     */
    @Override
    public OfficeHumiture selectOfficeHumitureById(Long id)
    {
        return officeHumitureMapper.selectOfficeHumitureById(id);
    }

    /**
     * 查询humiture列表
     * 
     * @param officeHumiture humiture
     * @return humiture
     */
    @Override
    public List<OfficeHumiture> selectOfficeHumitureList(OfficeHumiture officeHumiture)
    {
        return officeHumitureMapper.selectOfficeHumitureList(officeHumiture);
    }

    /**
     * 新增humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    @Override
    public int insertOfficeHumiture(OfficeHumiture officeHumiture)
    {
        return officeHumitureMapper.insertOfficeHumiture(officeHumiture);
    }

    /**
     * 修改humiture
     * 
     * @param officeHumiture humiture
     * @return 结果
     */
    @Override
    public int updateOfficeHumiture(OfficeHumiture officeHumiture)
    {
        return officeHumitureMapper.updateOfficeHumiture(officeHumiture);
    }

    /**
     * 批量删除humiture
     * 
     * @param ids 需要删除的humiture主键
     * @return 结果
     */
    @Override
    public int deleteOfficeHumitureByIds(Long[] ids)
    {
        return officeHumitureMapper.deleteOfficeHumitureByIds(ids);
    }

    /**
     * 删除humiture信息
     * 
     * @param id humiture主键
     * @return 结果
     */
    @Override
    public int deleteOfficeHumitureById(Long id)
    {
        return officeHumitureMapper.deleteOfficeHumitureById(id);
    }


    /**
     * 温湿度传感器 数据录入方法
     * @param msg
     */
    @Override
    public void humitureRecord(String msg){
        TcpDataHandler tcpDataHandler = new TcpDataHandler();
        Map<String,Double> humitureMap = tcpDataHandler.receivedTempAndHumitureHex2Str(msg);
        //实例化空白的温湿度对象
        OfficeHumiture officeHumiture = new OfficeHumiture();
        //设置值
        officeHumiture.setOfficeTemperature(humitureMap.get("温度"));
        officeHumiture.setOfficeHumidity(humitureMap.get("湿度"));
        officeHumiture.setRecordTime(new Date());
        log.info(officeHumiture.toString());
        DeviceLimit deviceLimitTemp = deviceLimitMapper.selectDeviceLimitByCheckValue("office_temperature");
        DeviceLimit deviceLimitHumi = deviceLimitMapper.selectDeviceLimitByCheckValue("office_humidity");
        if (officeHumiture.getOfficeTemperature() > deviceLimitTemp.getLimitUpper()){
            deviceLimitTemp.setDeviceState("温度过高");
            deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"86盒液晶温湿度变送器","温度过高",officeHumiture.getOfficeTemperature().toString(),"未处理",new Date()));
        }else if (officeHumiture.getOfficeTemperature() < deviceLimitTemp.getLimitLower()){
            deviceLimitTemp.setDeviceState("温度过低");
            deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"86盒液晶温湿度变送器","温度过低",officeHumiture.getOfficeTemperature().toString(),"未处理",new Date()));
        }else{
            deviceLimitTemp.setDeviceState("正常");
        }
        if (officeHumiture.getOfficeHumidity() > deviceLimitHumi.getLimitUpper()){
            deviceLimitHumi.setDeviceState("湿度过高");
            deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"86盒液晶温湿度变送器","湿度过高",officeHumiture.getOfficeHumidity().toString(),"未处理",new Date()));
        }else if (officeHumiture.getOfficeHumidity() < deviceLimitHumi.getLimitLower()){
            deviceLimitHumi.setDeviceState("湿度过低");
            deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"86盒液晶温湿度变送器","湿度过低",officeHumiture.getOfficeHumidity().toString(),"未处理",new Date()));
        }else{
            deviceLimitHumi.setDeviceState("正常");
        }
        deviceLimitMapper.updateDeviceLimit(deviceLimitHumi);
        deviceLimitMapper.updateDeviceLimit(deviceLimitTemp);
        officeHumitureMapper.insertOfficeHumiture(officeHumiture);
    }


    /**
     * 通过tcp向客户端发送询问温湿度消息
     * 执行频率，4分钟/次，从0秒钟开始
     */
//    @Scheduled(cron = "10 0/4 * * * ? ")
    public void sendMessageForHumiture() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "050300000002C58F";
            //发送处理
            ByteBuf bufff = Unpooled.buffer();
            bufff.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(bufff);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }

    }

    /**
     * 根据时间查询温湿度数组，如时间参数为空，默认返回当天数据
     */
    @Override
    public List<List> selectHumitureListByTime(Date selectTime){
        List temperature = new ArrayList();
        List humidity = new ArrayList();
        List time = new ArrayList();
        List all =new ArrayList();
        List<OfficeHumiture> officeHumitures = officeHumitureMapper.selectHumitureListByTime(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        for (int i = 0; i < officeHumitures.size(); i++) {
            OfficeHumiture officeHumiture =  officeHumitures.get(i);
            temperature.add(officeHumiture.getOfficeTemperature());
            humidity.add(officeHumiture.getOfficeHumidity());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(officeHumiture.getRecordTime()));
        }
        Collections.addAll(all,temperature,humidity,time);
        return all;
    }

    /**
     * 返回办公室温湿度的完整信息类，包含 最高、最低、平均 温湿度，最后一次数据的采集时间
     * @return
     */
    @Override
    public OfficeHumitureRequest getHumitureNowData(){
        Double tSum = 0d;
        Double hSum = 0d;
        Double tMax = 0d;
        Double tMin = 100d;
        Double hMax = 0d;
        Double hMin = 100d;
        OfficeHumitureRequest officeHumitureRequest = new OfficeHumitureRequest();
        //查询最新一条温湿度数据
        OfficeHumiture officeHumiture = officeHumitureMapper.selectLastHumiture();
        //查询本日所有温湿度列表，计算出最低，最高，平均
        List<OfficeHumiture> officeHumitures = officeHumitureMapper.selectHumitureListByTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        for (int i = 0; i < officeHumitures.size(); i++) {
            OfficeHumiture humiture =  officeHumitures.get(i);
            tSum += humiture.getOfficeTemperature();
            hSum += humiture.getOfficeHumidity();
            if (humiture.getOfficeTemperature()>tMax){
                tMax = humiture.getOfficeTemperature();
            }else if (humiture.getOfficeTemperature()<tMin){
                tMin = humiture.getOfficeTemperature();
            }
            if (humiture.getOfficeHumidity()>hMax){
                hMax = humiture.getOfficeHumidity();
            }else if (humiture.getOfficeHumidity() < hMin){
                hMin = humiture.getOfficeHumidity();
            }
        }
        //使用最大值最小值工具类中找出温湿度最大值
        MathUtil mathUtil = new MathUtil();
        officeHumitureRequest.setMaxTemperature(tMax);
        officeHumitureRequest.setMinTemperature(tMin);
        officeHumitureRequest.setMaxOfficeHumidity(hMax);
        officeHumitureRequest.setMinOfficeHumidity(hMin);
        //查询最新一条温湿度数据，复制到返回类里
        BeanUtils.copyProperties(officeHumiture,officeHumitureRequest);
        //设置温湿度平均值，并且保留两位小数
        officeHumitureRequest.setAverageHumidity(NumberUtil.round(hSum/officeHumitures.size(),2).doubleValue());
        officeHumitureRequest.setAverageTemperature(NumberUtil.round(tSum/officeHumitures.size(),2).doubleValue());
        return officeHumitureRequest;
    }

}
