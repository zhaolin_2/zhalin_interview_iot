package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import cn.hutool.core.util.NumberUtil;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.domain.DeviceCount;
import com.ruoyi.system.domain.ElectricityMeter;
import com.ruoyi.system.domain.TElectricityMeter;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.OfficeReport;
import com.ruoyi.system.service.IOfficeReportService;

/**
 * 报表Service业务层处理
 *
 * @author Allin
 * @date 2022-11-10
 */
@Service
public class OfficeReportServiceImpl implements IOfficeReportService {
    @Autowired
    private OfficeReportMapper officeReportMapper;


    @Autowired
    private LouverBoxMapper louverBoxMapper;

    @Autowired
    private ElectricityMeterMapper electricityMeterMapper;

    @Autowired
    private TElectricityMeterMapper tElectricityMeterMapper;

    @Autowired
    private DeviceCountMapper deviceCountMapper;


    /**
     * 查询报表
     *
     * @param id 报表主键
     * @return 报表
     */
    @Override
    public OfficeReport selectOfficeReportById(Long id) {
        return officeReportMapper.selectOfficeReportById(id);
    }

    /**
     * 查询报表列表
     *
     * @param officeReport 报表
     * @return 报表
     */
    @Override
    public List<OfficeReport> selectOfficeReportList(OfficeReport officeReport) {
        return officeReportMapper.selectOfficeReportList(officeReport);
    }

    /**
     * 新增报表
     *
     * @param officeReport 报表
     * @return 结果
     */
    @Override
    public int insertOfficeReport(OfficeReport officeReport) {
        return officeReportMapper.insertOfficeReport(officeReport);
    }

    /**
     * 修改报表
     *
     * @param officeReport 报表
     * @return 结果
     */
    @Override
    public int updateOfficeReport(OfficeReport officeReport) {
        return officeReportMapper.updateOfficeReport(officeReport);
    }

    /**
     * 批量删除报表
     *
     * @param ids 需要删除的报表主键
     * @return 结果
     */
    @Override
    public int deleteOfficeReportByIds(Long[] ids) {
        return officeReportMapper.deleteOfficeReportByIds(ids);
    }

    /**
     * 删除报表信息
     *
     * @param id 报表主键
     * @return 结果
     */
    @Override
    public int deleteOfficeReportById(Long id) {
        return officeReportMapper.deleteOfficeReportById(id);
    }

    /**
     * 查询办公室能耗方法，
     * @param count 查询多少天
     * @return 返回数组，包含能耗数组和对应的时间
     */
    @Override
    public List selectConsumptionList(Integer count){
            List consum = new ArrayList();
            List time = new ArrayList();
            List all = new ArrayList();
        try {
            List<OfficeReport> officeReports = officeReportMapper.selectAllReport(count);
            for (int i = officeReports.size() - 1; i >= 0; i--) {
                OfficeReport officeReport = officeReports.get(i);
                consum.add(officeReport.getoApAll());
                time.add(new SimpleDateFormat("MM-dd").format(officeReport.getRecordTime()));
            }
            Collections.addAll(all, consum, time);
        }catch (Exception e){
            throw new RuntimeException("请输入有效数字");
        }
        return all;
    }

    /**
     * 查询三项电表能耗的方法，
     * @param count 查询多少天
     * @return 返回数组，包含能耗数组和对应的时间
     */
    @Override
    public List selectConsumptionListForT(Integer count){
        //能耗数组
        List consum = new ArrayList();
        //时间数组
        List time = new ArrayList();
        //整体数组
        List all = new ArrayList();
        List<OfficeReport> officeReports = officeReportMapper.selectAllReport(count);
        for (int i = officeReports.size()-1; i >= 0 ; i--) {
            OfficeReport officeReport =  officeReports.get(i);
            consum.add(officeReport.gettApAll());
            time.add(new SimpleDateFormat("yyyy-MM-dd").format(officeReport.getRecordTime()));
        }
        Collections.addAll(all,consum,time);
        return all;
    }

    /**
     * 从数据库查询
     *
     * @param selectTime
     * @return
     */
    @Override
    public OfficeReport selectReportGenerate(Date selectTime) {
        OfficeReport officeReport;
        try {
            officeReport = officeReportMapper.selectReportByDate(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
            if (officeReport == null){
                throw new RuntimeException("没有查询到该日数据！");
            }
            return officeReport;
        } catch (Exception e) {
            throw new RuntimeException("没有查询到该日数据！");
        }
    }





    /**
     * 定时任务：生成报表
     *
     * @return
     */
//    @Scheduled(cron = "0 58 23 * * ?")
    public void reportGenerateDaily() {
        OfficeReport officeReport = reportGenerate(new Date());
        officeReportMapper.insertOfficeReport(officeReport);
    }


    public OfficeReport reportGenerate(Date selectTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //构造今天日期的查询字符串
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(selectTime);
        calendar.add(Calendar.DATE, -1);
        String toady = simpleDateFormat.format(selectTime);
        String yesterday = simpleDateFormat.format(calendar.getTime());
        //准备插入的报表数据
        OfficeReport officeReport = new OfficeReport();

        //先取温湿度 光照 峰值
        OfficeReport humitureAndIll = louverBoxMapper.rangeOfHumitureIllData(toady);
        //取电流峰值
        OfficeReport electricityRange = electricityMeterMapper.rangeOfElectricityData(toady);
        //取办公室本日能耗
        ElectricityMeter electricityMeterToday = electricityMeterMapper.selectLastElectricityByDate(toady);
        //取办公室昨日能耗
        ElectricityMeter electricityMeterYtd = electricityMeterMapper.selectLastElectricityByDate(yesterday);
        //取三相电表本日能耗
        TElectricityMeter tElectricityMeterToday = tElectricityMeterMapper.selectLastTElectricityByDate(toady);
        //取三相电表昨日能耗
        TElectricityMeter tElectricityMeterYdt = tElectricityMeterMapper.selectLastTElectricityByDate(yesterday);
        //取生产数量本日数量
        DeviceCount deviceCountToday = deviceCountMapper.selectLastCountByDate(toady);
        //取生产数量昨日数据
        DeviceCount deviceCountYtd = deviceCountMapper.selectLastCountByDate(yesterday);
  
        //写入数据
        officeReport.setoTempAvg(NumberUtil.round(humitureAndIll.getoTempAvg(), 2).doubleValue());
        officeReport.setoTempMax(humitureAndIll.getoTempMax());
        officeReport.setoTempMin(humitureAndIll.getoTempMin());
        officeReport.setoHumiAvg(NumberUtil.round(humitureAndIll.getoHumiAvg(), 2).doubleValue());
        officeReport.setoHumiMin(humitureAndIll.getoHumiMin());
        officeReport.setoHumiMax(humitureAndIll.getoHumiMax());
        officeReport.setoIllAvg(NumberUtil.round(humitureAndIll.getoIllAvg(), 2).doubleValue());
        officeReport.setoIllMax(humitureAndIll.getoIllMax());
        officeReport.setoIllMin(humitureAndIll.getoIllMin());
        officeReport.setoAmax(electricityRange.getoAmax());
        officeReport.setoAmin(electricityRange.getoAmin());
        officeReport.setoApAll(NumberUtil.round(electricityMeterToday.getEmActiveElectricity() - electricityMeterYtd.getEmActiveElectricity(), 2).doubleValue());
        officeReport.settApAll(NumberUtil.round(tElectricityMeterToday.getAeAll() - tElectricityMeterYdt.getAeAll(),2).doubleValue());
        officeReport.setOutput(deviceCountToday.getCount()-deviceCountYtd.getCount());
        Double a = (deviceCountToday.getCount()-deviceCountYtd.getCount())/(electricityMeterToday.getEmActiveElectricity() - electricityMeterYtd.getEmActiveElectricity());
        officeReport.setProportion(NumberUtil.round(a,2).doubleValue());
        officeReport.setRecordTime(new Date());
        return officeReport;
    }




}
