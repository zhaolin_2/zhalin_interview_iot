package com.ruoyi.system.service.impl;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.ObjectComparator;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.OfficeStockWaste;
import com.ruoyi.system.domain.StockMessage;
import com.ruoyi.system.domain.request.dataIntegration.StockStateRequest;
import com.ruoyi.system.mapper.OfficeStockWasteMapper;
import com.ruoyi.system.mapper.StockMessageMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OfficeStockMapper;
import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.service.IOfficeStockService;

/**
 * 库存管理Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-30
 */
@Service
@Slf4j
public class OfficeStockServiceImpl implements IOfficeStockService 
{
    @Autowired
    private OfficeStockMapper officeStockMapper;

    @Autowired
    private StockMessageMapper stockMessageMapper;

    @Autowired
    private OfficeStockWasteMapper officeStockWasteMapper;

    /**
     * 查询库存管理
     * 
     * @param id 库存管理主键
     * @return 库存管理
     */
    @Override
    public OfficeStock selectOfficeStockById(Long id)
    {
        return officeStockMapper.selectOfficeStockById(id);
    }

    /**
     * 查询库存管理列表
     * 
     * @param officeStock 库存管理
     * @return 库存管理
     */
    @Override
    public List<OfficeStock> selectOfficeStockList(OfficeStock officeStock)
    {
        return officeStockMapper.selectOfficeStockList(officeStock);
    }

    /**
     * 新增库存管理
     * 
     * @param officeStock 库存管理
     * @return 结果
     */
    @Override
    public int insertOfficeStock(OfficeStock officeStock)
    {
        //为新增的库存设置时间
        officeStock.setRecordTime(new Date());
        //库存查询后，为该库存设置初始化入库信息
        int i = officeStockMapper.insertOfficeStock(officeStock);
        stockMessageMapper.insertStockMessage(new StockMessage(null,officeStock.getId(),SecurityUtils.getLoginUser().getUser().getNickName(),"入库",officeStock.getStockNum(),"该操作为库存首次添加，初始化数量为"+officeStock.getStockNum(),new Date()));
        return i;
    }

    /**
     * 修改库存管理
     * 
     * @param officeStock 库存管理
     * @return 结果
     */
    @Override
    public int updateOfficeStock(OfficeStock officeStock)
    {

        //更新操作需要 记录，首先使用该库存查出来未修改前的对象，并且对比不同项目后，将信息插入
        OfficeStock stockOriginal = officeStockMapper.selectOfficeStockById(officeStock.getId());
        String updateMessage = new ObjectComparator().compareObjects(stockOriginal, officeStock);
        stockMessageMapper.insertStockMessage(new StockMessage(null,officeStock.getId(),SecurityUtils.getLoginUser().getUser().getNickName(),"库存信息修改",0l,SecurityUtils.getLoginUser().getUser().getNickName()+updateMessage,new Date()));
        return officeStockMapper.updateOfficeStock(officeStock);
    }

    /**
     * 批量删除库存管理
     * 
     * @param ids 需要删除的库存管理主键
     * @return 结果
     */
    @Override
    public int deleteOfficeStockByIds(Long[] ids)
    {
        return officeStockMapper.deleteOfficeStockByIds(ids);
    }

    /**
     * 删除库存管理信息
     * 
     * @param id 库存管理主键
     * @return 结果
     */
    @Override
    public int deleteOfficeStockById(Long id)
    {
        return officeStockMapper.deleteOfficeStockById(id);
    }

    /**
     * 出库
     * @param id 库存id
     * @param num 出库数量
     * @param name 操作人
     * @param message 出库信息
     * @return
     */
    @Override
    public int outStock(Long id,Long num,String name,String message) {
        OfficeStock officeStock = officeStockMapper.selectOfficeStockById(id);
        StockMessage stockMessage = new StockMessage();
        //确定出库数量小于库存
        if (num> officeStock.getStockNum()){
            throw new RuntimeException("库存不足，请重新填写出库数量！");
        }
        //修改库存
        officeStock.setStockNum(officeStock.getStockNum() - num);

        //录入出库记录
        stockMessage.setStockId(id);
        stockMessage.setHandleMessage(message);
        stockMessage.setHandleMan(name);
        stockMessage.setHandleType("出库");
        stockMessage.setHandleNum(num);
        stockMessage.setHandleTime(new Date());
        //写入数据库
        stockMessageMapper.insertStockMessage(stockMessage);
        return officeStockMapper.updateOfficeStock(officeStock);
    }

    /**
     * 入库
     * @param id 库存id
     * @param num 入库数量
     * @param name  入库操作人
     * @param message 入库信息备注
     * @return
     */
    @Override
    public int inStock(Long id,Long num,String name,String message) {
        OfficeStock officeStock = officeStockMapper.selectOfficeStockById(id);
        StockMessage stockMessage = new StockMessage();
        officeStock.setStockNum(officeStock.getStockNum() + num);
        //录入入库记录
        stockMessage.setStockId(id);
        stockMessage.setHandleMessage(message);
        stockMessage.setHandleMan(name);
        stockMessage.setHandleType("入库");
        stockMessage.setHandleNum(num);
        stockMessage.setHandleTime(new Date());
        //写入数据库
        stockMessageMapper.insertStockMessage(stockMessage);
        return officeStockMapper.updateOfficeStock(officeStock);
    }

    /**
     * 根据库存id  查询该id下属的所有出入库信息
     * @param id 库存id
     * @return
     */
    @Override
    public List<StockMessage> selectsStockMessagesByStockId(Long id){
        return stockMessageMapper.selectStockMessageListByStockId(id);
    }

    /**
     * 将库存移入废品库
     * @param id
     * @param num
     * @param message
     */
    @Override
    public void outStockToWst(Long id,Long num,String message) {
        OfficeStock officeStock = officeStockMapper.selectOfficeStockById(id);
        OfficeStockWaste officeStockWaste = new OfficeStockWaste();
        //确定出库数量小于库存
        if (num> officeStock.getStockNum()){
            throw new RuntimeException("库存不足，请重新填写报废数量！");
        }
        //修改库存
        officeStock.setStockNum(officeStock.getStockNum() - num);
        BeanUtils.copyProperties(officeStock,officeStockWaste);
        officeStockWaste.setRecordTime(new Date());
        officeStockWaste.setTemp2(message);
        officeStockMapper.updateOfficeStock(officeStock);
        officeStockWasteMapper.insertOfficeStockWaste(officeStockWaste);
    }

    /**
     * 导入用户数据
     *
     * @param stockList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @return 结果
     */
    @Override
    public String importStock(List<OfficeStock> stockList, Boolean isUpdateSupport)
    {
        if (StringUtils.isNull(stockList) || stockList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (OfficeStock stock : stockList)
        {
            try
            {
                // 验证是否存在这个用户
                List<OfficeStock> isStockEmpty = officeStockMapper.selectOfficeStockList(stock);
                if (isStockEmpty.size() == 0)
                {
                    this.insertOfficeStock(stock);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "个库存导入成功");
                }
                else if (isUpdateSupport)
                {
                    this.updateOfficeStock(stock);
                    successNum++;
                    successMsg.append("<br/>" + successNum +"个库存更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "个库存已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }


}
