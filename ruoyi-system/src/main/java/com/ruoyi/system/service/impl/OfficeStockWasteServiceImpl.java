package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.OfficeStockWaste;
import com.ruoyi.system.mapper.OfficeStockMapper;
import com.ruoyi.system.service.IOfficeStockWasteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 废弃品管理Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-30
 */
@Service
public class OfficeStockWasteServiceImpl implements IOfficeStockWasteService
{
    @Autowired
    private OfficeStockMapper officeStockWasteMapper;


    @Override
    public OfficeStockWaste selectOfficeStockWasteById(Long id) {
        return null;
    }

    @Override
    public List<OfficeStockWaste> selectOfficeStockWasteList(OfficeStockWaste officeStockWaste) {
        return null;
    }

    @Override
    public int insertOfficeStockWaste(OfficeStockWaste officeStockWaste) {
        return 0;
    }

    @Override
    public int updateOfficeStock(OfficeStockWaste officeStockWaste) {
        return 0;
    }

    @Override
    public int deleteOfficeStockWasteById(Long id) {
        return 0;
    }
}
