package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OrderMgtMapper;
import com.ruoyi.system.domain.OrderMgt;
import com.ruoyi.system.service.IOrderMgtService;

/**
 * 订单管理Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-10
 */
@Service
public class OrderMgtServiceImpl implements IOrderMgtService 
{
    @Autowired
    private OrderMgtMapper orderMgtMapper;

    /**
     * 查询订单管理
     * 
     * @param id 订单管理主键
     * @return 订单管理
     */
    @Override
    public OrderMgt selectOrderMgtById(Long id)
    {
        return orderMgtMapper.selectOrderMgtById(id);
    }

    /**
     * 查询订单管理列表
     * 
     * @param orderMgt 订单管理
     * @return 订单管理
     */
    @Override
    public List<OrderMgt> selectOrderMgtList(OrderMgt orderMgt)
    {
        return orderMgtMapper.selectOrderMgtList(orderMgt);
    }

    /**
     * 新增订单管理
     * 
     * @param orderMgt 订单管理
     * @return 结果
     */
    @Override
    public int insertOrderMgt(OrderMgt orderMgt)
    {
        return orderMgtMapper.insertOrderMgt(orderMgt);
    }

    /**
     * 修改订单管理
     * 
     * @param orderMgt 订单管理
     * @return 结果
     */
    @Override
    public int updateOrderMgt(OrderMgt orderMgt)
    {
        return orderMgtMapper.updateOrderMgt(orderMgt);
    }

    /**
     * 批量删除订单管理
     * 
     * @param ids 需要删除的订单管理主键
     * @return 结果
     */
    @Override
    public int deleteOrderMgtByIds(Long[] ids)
    {
        return orderMgtMapper.deleteOrderMgtByIds(ids);
    }

    /**
     * 删除订单管理信息
     * 
     * @param id 订单管理主键
     * @return 结果
     */
    @Override
    public int deleteOrderMgtById(Long id)
    {
        return orderMgtMapper.deleteOrderMgtById(id);
    }
}
