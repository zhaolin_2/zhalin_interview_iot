package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PaintingMgtMapper;
import com.ruoyi.system.domain.PaintingMgt;
import com.ruoyi.system.service.IPaintingMgtService;

/**
 * 书画管理Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-15
 */
@Service
public class PaintingMgtServiceImpl implements IPaintingMgtService 
{
    @Autowired
    private PaintingMgtMapper paintingMgtMapper;

    /**
     * 查询书画管理
     * 
     * @param id 书画管理主键
     * @return 书画管理
     */
    @Override
    public PaintingMgt selectPaintingMgtById(Long id)
    {
        return paintingMgtMapper.selectPaintingMgtById(id);
    }

    /**
     * 查询书画管理列表
     * 
     * @param paintingMgt 书画管理
     * @return 书画管理
     */
    @Override
    public List<PaintingMgt> selectPaintingMgtList(PaintingMgt paintingMgt)
    {
        return paintingMgtMapper.selectPaintingMgtList(paintingMgt);
    }

    /**
     * 新增书画管理
     * 
     * @param paintingMgt 书画管理
     * @return 结果
     */
    @Override
    public int insertPaintingMgt(PaintingMgt paintingMgt)
    {
        return paintingMgtMapper.insertPaintingMgt(paintingMgt);
    }

    /**
     * 修改书画管理
     * 
     * @param paintingMgt 书画管理
     * @return 结果
     */
    @Override
    public int updatePaintingMgt(PaintingMgt paintingMgt)
    {
        return paintingMgtMapper.updatePaintingMgt(paintingMgt);
    }

    /**
     * 批量删除书画管理
     * 
     * @param ids 需要删除的书画管理主键
     * @return 结果
     */
    @Override
    public int deletePaintingMgtByIds(Long[] ids)
    {
        return paintingMgtMapper.deletePaintingMgtByIds(ids);
    }

    /**
     * 删除书画管理信息
     * 
     * @param id 书画管理主键
     * @return 结果
     */
    @Override
    public int deletePaintingMgtById(Long id)
    {
        return paintingMgtMapper.deletePaintingMgtById(id);
    }
}
