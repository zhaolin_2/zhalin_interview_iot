package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.TemplateUnit;
import com.ruoyi.system.domain.request.OfficeStockRequest;
import com.ruoyi.system.mapper.OfficeStockMapper;
import com.ruoyi.system.mapper.TemplateUnitMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductTemplateMapper;
import com.ruoyi.system.domain.ProductTemplate;
import com.ruoyi.system.service.IProductTemplateService;

/**
 * 模板Service业务层处理
 * 
 * @author lin
 * @date 2022-12-07
 */
@Service
public class ProductTemplateServiceImpl implements IProductTemplateService 
{
    @Autowired
    private ProductTemplateMapper productTemplateMapper;

    @Autowired
    private TemplateUnitMapper templateUnitMapper;

    @Autowired
    private OfficeStockMapper officeStockMapper;

    /**
     * 查询模板
     * 
     * @param id 模板主键
     * @return 模板
     */
    @Override
    public ProductTemplate selectProductTemplateById(Long id)
    {
        return productTemplateMapper.selectProductTemplateById(id);
    }

    /**
     * 查询模板列表
     * 
     * @param productTemplate 模板
     * @return 模板
     */
    @Override
    public List<ProductTemplate> selectProductTemplateList(ProductTemplate productTemplate)
    {
        return productTemplateMapper.selectProductTemplateList(productTemplate);
    }

    /**
     * 新增模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    @Override
    public int insertProductTemplate(ProductTemplate productTemplate)
    {
        return productTemplateMapper.insertProductTemplate(productTemplate);
    }

    /**
     * 修改模板
     * 
     * @param productTemplate 模板
     * @return 结果
     */
    @Override
    public int updateProductTemplate(ProductTemplate productTemplate)
    {
        return productTemplateMapper.updateProductTemplate(productTemplate);
    }

    /**
     * 批量删除模板
     * 
     * @param ids 需要删除的模板主键
     * @return 结果
     */
    @Override
    public int deleteProductTemplateByIds(Long[] ids)
    {
        return productTemplateMapper.deleteProductTemplateByIds(ids);
    }

    /**
     * 删除模板信息
     * 
     * @param id 模板主键
     * @return 结果
     */
    @Override
    public int deleteProductTemplateById(Long id)
    {
        return productTemplateMapper.deleteProductTemplateById(id);
    }

    /**
     * 组装前核对组装后库存剩余
     * @param id 组装的模板产品id
     * @param num 组装数量
     * @return 返回经过核算和库存后的库存数量
     */
    @Override
    public List<OfficeStockRequest> checkBeforeAssemble(Long id, Long num) {
        List<TemplateUnit> templateUnits = templateUnitMapper.selectTemplateUnitByTemplateId(id);
        List<OfficeStockRequest> list = new ArrayList<>();
        for (int i = 0; i < templateUnits.size(); i++) {
            //遍历组件
            TemplateUnit templateUnit =  templateUnits.get(i);
            //根据组件中的stockId,查询库存中对应的组件
            OfficeStock officeStock = officeStockMapper.selectOfficeStockById(templateUnit.getStockId());
            OfficeStockRequest officeStockRequest = new OfficeStockRequest();
            BeanUtils.copyProperties(officeStock,officeStockRequest);
            //组装后数量计算：等于库存 - 该产品需要的组件数量*组装数量
            officeStockRequest.setAssembledStockNum(officeStock.getStockNum()-num*templateUnit.getStockNum());
            officeStockRequest.setStockSum(num*templateUnit.getStockNum());
            list.add(officeStockRequest);
        }
        return list;
    }

    @Override
    public void assembleToStock(OfficeStock officeStock) {

        try {
            //查看库存中是否已经存在有该产品
            OfficeStock stkSelect = new OfficeStock();
            stkSelect.setStockName(officeStock.getStockName());
            stkSelect.setStockType(officeStock.getStockType());
            stkSelect.setStockPackage(officeStock.getStockPackage());
            List<OfficeStock> officeStocks = officeStockMapper.selectOfficeStockList(stkSelect);
            //如果有 则更新数量 如果没有则插入一条新内容
            if (officeStocks.size() > 0) {
                officeStocks.get(0).setStockNum(officeStock.getStockNum()+officeStocks.get(0).getStockNum());
                List<TemplateUnit> templateUnits = templateUnitMapper.selectTemplateUnitByTemplateId(officeStock.getId());
                for (int i = 0; i < templateUnits.size(); i++) {
                    TemplateUnit templateUnit =  templateUnits.get(i);
                    //根据组件中的stockId,查询库存中对应的组件,并且扣除组装需要的组件数量并且更新
                    OfficeStock stockById = officeStockMapper.selectOfficeStockById(templateUnit.getStockId());
                    stockById.setStockNum(stockById.getStockNum()-officeStock.getStockNum()*templateUnit.getStockNum());
                    officeStockMapper.updateOfficeStock(stockById);
                }
                officeStockMapper.updateOfficeStock(officeStocks.get(0));
            } else if (officeStocks.size() == 0) {
                officeStock.setRecordTime(new Date());
//                officeStock.setStockManager(SecurityUtils.getUsername());
                officeStock.setStockClass("组装");
                List<TemplateUnit> templateUnits = templateUnitMapper.selectTemplateUnitByTemplateId(officeStock.getId());
                for (int i = 0; i < templateUnits.size(); i++) {
                    TemplateUnit templateUnit =  templateUnits.get(i);
                    //根据组件中的stockId,查询库存中对应的组件,并且扣除组装需要的组件数量并且更新
                    OfficeStock stockById = officeStockMapper.selectOfficeStockById(templateUnit.getStockId());
                    stockById.setStockNum(stockById.getStockNum()-officeStock.getStockNum()*templateUnit.getStockNum());
                    officeStockMapper.updateOfficeStock(stockById);
                }
                officeStockMapper.insertOfficeStock(officeStock);
            }
        } catch (Exception e) {
            throw new RuntimeException("访问异常");
        }

    }
}
