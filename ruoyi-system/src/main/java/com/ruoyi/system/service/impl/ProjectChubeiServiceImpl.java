package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProjectChubeiMapper;
import com.ruoyi.system.domain.ProjectChubei;
import com.ruoyi.system.service.IProjectChubeiService;

/**
 * 项目储备Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-22
 */
@Service
public class ProjectChubeiServiceImpl implements IProjectChubeiService 
{
    @Autowired
    private ProjectChubeiMapper projectChubeiMapper;

    /**
     * 查询项目储备
     * 
     * @param id 项目储备主键
     * @return 项目储备
     */
    @Override
    public ProjectChubei selectProjectChubeiById(Long id)
    {
        return projectChubeiMapper.selectProjectChubeiById(id);
    }

    /**
     * 查询项目储备列表
     * 
     * @param projectChubei 项目储备
     * @return 项目储备
     */
    @Override
    public List<ProjectChubei> selectProjectChubeiList(ProjectChubei projectChubei)
    {
        return projectChubeiMapper.selectProjectChubeiList(projectChubei);
    }

    /**
     * 新增项目储备
     * 
     * @param projectChubei 项目储备
     * @return 结果
     */
    @Override
    public int insertProjectChubei(ProjectChubei projectChubei)
    {
        return projectChubeiMapper.insertProjectChubei(projectChubei);
    }

    /**
     * 修改项目储备
     * 
     * @param projectChubei 项目储备
     * @return 结果
     */
    @Override
    public int updateProjectChubei(ProjectChubei projectChubei)
    {
        return projectChubeiMapper.updateProjectChubei(projectChubei);
    }

    /**
     * 批量删除项目储备
     * 
     * @param ids 需要删除的项目储备主键
     * @return 结果
     */
    @Override
    public int deleteProjectChubeiByIds(Long[] ids)
    {
        return projectChubeiMapper.deleteProjectChubeiByIds(ids);
    }

    /**
     * 删除项目储备信息
     * 
     * @param id 项目储备主键
     * @return 结果
     */
    @Override
    public int deleteProjectChubeiById(Long id)
    {
        return projectChubeiMapper.deleteProjectChubeiById(id);
    }
}
