package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProjectLuohuMapper;
import com.ruoyi.system.domain.ProjectLuohu;
import com.ruoyi.system.service.IProjectLuohuService;

/**
 * 落户项目Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-22
 */
@Service
public class ProjectLuohuServiceImpl implements IProjectLuohuService 
{
    @Autowired
    private ProjectLuohuMapper projectLuohuMapper;

    /**
     * 查询落户项目
     * 
     * @param id 落户项目主键
     * @return 落户项目
     */
    @Override
    public ProjectLuohu selectProjectLuohuById(Long id)
    {
        return projectLuohuMapper.selectProjectLuohuById(id);
    }

    /**
     * 查询落户项目列表
     * 
     * @param projectLuohu 落户项目
     * @return 落户项目
     */
    @Override
    public List<ProjectLuohu> selectProjectLuohuList(ProjectLuohu projectLuohu)
    {
        return projectLuohuMapper.selectProjectLuohuList(projectLuohu);
    }

    /**
     * 新增落户项目
     * 
     * @param projectLuohu 落户项目
     * @return 结果
     */
    @Override
    public int insertProjectLuohu(ProjectLuohu projectLuohu)
    {
        return projectLuohuMapper.insertProjectLuohu(projectLuohu);
    }

    /**
     * 修改落户项目
     * 
     * @param projectLuohu 落户项目
     * @return 结果
     */
    @Override
    public int updateProjectLuohu(ProjectLuohu projectLuohu)
    {
        return projectLuohuMapper.updateProjectLuohu(projectLuohu);
    }

    /**
     * 批量删除落户项目
     * 
     * @param ids 需要删除的落户项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectLuohuByIds(Long[] ids)
    {
        return projectLuohuMapper.deleteProjectLuohuByIds(ids);
    }

    /**
     * 删除落户项目信息
     * 
     * @param id 落户项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectLuohuById(Long id)
    {
        return projectLuohuMapper.deleteProjectLuohuById(id);
    }
}
