package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProjectMtgMapper;
import com.ruoyi.system.domain.ProjectMtg;
import com.ruoyi.system.service.IProjectMtgService;

/**
 * 项目管理Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-22
 */
@Service
public class ProjectMtgServiceImpl implements IProjectMtgService 
{
    @Autowired
    private ProjectMtgMapper projectMtgMapper;

    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    @Override
    public ProjectMtg selectProjectMtgById(Long id)
    {
        return projectMtgMapper.selectProjectMtgById(id);
    }

    /**
     * 查询项目管理列表
     * 
     * @param projectMtg 项目管理
     * @return 项目管理
     */
    @Override
    public List<ProjectMtg> selectProjectMtgList(ProjectMtg projectMtg)
    {
        return projectMtgMapper.selectProjectMtgList(projectMtg);
    }

    /**
     * 新增项目管理
     * 
     * @param projectMtg 项目管理
     * @return 结果
     */
    @Override
    public int insertProjectMtg(ProjectMtg projectMtg)
    {
        return projectMtgMapper.insertProjectMtg(projectMtg);
    }

    /**
     * 修改项目管理
     * 
     * @param projectMtg 项目管理
     * @return 结果
     */
    @Override
    public int updateProjectMtg(ProjectMtg projectMtg)
    {
        return projectMtgMapper.updateProjectMtg(projectMtg);
    }

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deleteProjectMtgByIds(Long[] ids)
    {
        return projectMtgMapper.deleteProjectMtgByIds(ids);
    }

    /**
     * 删除项目管理信息
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    @Override
    public int deleteProjectMtgById(Long id)
    {
        return projectMtgMapper.deleteProjectMtgById(id);
    }
}
