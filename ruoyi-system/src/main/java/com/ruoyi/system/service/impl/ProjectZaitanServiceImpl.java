package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProjectZaitanMapper;
import com.ruoyi.system.domain.ProjectZaitan;
import com.ruoyi.system.service.IProjectZaitanService;

/**
 * 在谈项目Service业务层处理
 * 
 * @author Lin
 * @date 2023-02-22
 */
@Service
public class ProjectZaitanServiceImpl implements IProjectZaitanService 
{
    @Autowired
    private ProjectZaitanMapper projectZaitanMapper;

    /**
     * 查询在谈项目
     * 
     * @param id 在谈项目主键
     * @return 在谈项目
     */
    @Override
    public ProjectZaitan selectProjectZaitanById(Long id)
    {
        return projectZaitanMapper.selectProjectZaitanById(id);
    }

    /**
     * 查询在谈项目列表
     * 
     * @param projectZaitan 在谈项目
     * @return 在谈项目
     */
    @Override
    public List<ProjectZaitan> selectProjectZaitanList(ProjectZaitan projectZaitan)
    {
        return projectZaitanMapper.selectProjectZaitanList(projectZaitan);
    }

    /**
     * 新增在谈项目
     * 
     * @param projectZaitan 在谈项目
     * @return 结果
     */
    @Override
    public int insertProjectZaitan(ProjectZaitan projectZaitan)
    {
        return projectZaitanMapper.insertProjectZaitan(projectZaitan);
    }

    /**
     * 修改在谈项目
     * 
     * @param projectZaitan 在谈项目
     * @return 结果
     */
    @Override
    public int updateProjectZaitan(ProjectZaitan projectZaitan)
    {
        return projectZaitanMapper.updateProjectZaitan(projectZaitan);
    }

    /**
     * 批量删除在谈项目
     * 
     * @param ids 需要删除的在谈项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectZaitanByIds(Long[] ids)
    {
        return projectZaitanMapper.deleteProjectZaitanByIds(ids);
    }

    /**
     * 删除在谈项目信息
     * 
     * @param id 在谈项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectZaitanById(Long id)
    {
        return projectZaitanMapper.deleteProjectZaitanById(id);
    }
}
