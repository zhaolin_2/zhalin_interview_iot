package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.TcpDataHandler;
import com.ruoyi.system.tcp.DataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PtDeviceMapper;
import com.ruoyi.system.domain.PtDevice;
import com.ruoyi.system.service.IPtDeviceService;

/**
 * pt测温Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Service
@Slf4j
public class PtDeviceServiceImpl implements IPtDeviceService 
{
    @Autowired
    private PtDeviceMapper ptDeviceMapper;

    @Autowired
    private DeviceLimitMapper deviceLimitMapper;

    /**
     * 查询pt测温
     * 
     * @param id pt测温主键
     * @return pt测温
     */
    @Override
    public PtDevice selectPtDeviceById(Long id)
    {
        return ptDeviceMapper.selectPtDeviceById(id);
    }

    /**
     * 查询pt测温列表
     * 
     * @param ptDevice pt测温
     * @return pt测温
     */
    @Override
    public List<PtDevice> selectPtDeviceList(PtDevice ptDevice)
    {
        return ptDeviceMapper.selectPtDeviceList(ptDevice);
    }

    /**
     * 新增pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    @Override
    public int insertPtDevice(PtDevice ptDevice)
    {
        return ptDeviceMapper.insertPtDevice(ptDevice);
    }

    /**
     * 修改pt测温
     * 
     * @param ptDevice pt测温
     * @return 结果
     */
    @Override
    public int updatePtDevice(PtDevice ptDevice)
    {
        return ptDeviceMapper.updatePtDevice(ptDevice);
    }

    /**
     * 批量删除pt测温
     * 
     * @param ids 需要删除的pt测温主键
     * @return 结果
     */
    @Override
    public int deletePtDeviceByIds(Long[] ids)
    {
        return ptDeviceMapper.deletePtDeviceByIds(ids);
    }

    /**
     * 删除pt测温信息
     * 
     * @param id pt测温主键
     * @return 结果
     */
    @Override
    public int deletePtDeviceById(Long id)
    {
        return ptDeviceMapper.deletePtDeviceById(id);
    }

    /**
     * 通过tcp向客户端发送询问温湿度消息
     */
//    @Scheduled(cron = "0 4/4 * * * ?")
    public void sendMessageForHumiture() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "030300010001D428";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }

    }

    /**
     * 温湿度传感器 数据录入方法
     * @param msg
     */
    @Override
    public void ptTemperatureRecord(String msg){
        TcpDataHandler tcpDataHandler = new TcpDataHandler();
        Map<String, Double> ptTemperatureMap = tcpDataHandler.receivedTPDeviceHex2String(msg);
        //实例化空白的pt100采集器 温度对象
        PtDevice ptDevice = new PtDevice();
        ptDevice.setRecordTime(new Date());
        ptDevice.setPtTemperature(ptTemperatureMap.get("温度"));
        //设置值
        log.info(ptDevice.toString());
        DeviceLimit deviceLimit = deviceLimitMapper.selectDeviceLimitByCheckValue("pt_temperature");
        if (ptDevice.getPtTemperature() > deviceLimit.getLimitUpper()){
            deviceLimit.setDeviceState("温度过高");
        }else if (ptDevice.getPtTemperature() < deviceLimit.getLimitLower()){
            deviceLimit.setDeviceState("温度过低");
        }else{
            deviceLimit.setDeviceState("正常");
        }
        deviceLimitMapper.updateDeviceLimit(deviceLimit);
        ptDeviceMapper.insertPtDevice(ptDevice);
    }

    /**
     * 该方法用于查询pt100的温度曲线
     * @param selectTime 查询时间
     * @return 温度曲线
     */
    @Override
    public List ptTemperatureList(Date selectTime){

        List temperature = new ArrayList();
        List time = new ArrayList();
        List all = new ArrayList();
        List<PtDevice> ptDevices = ptDeviceMapper.selectPtTemperatureListByDate(new SimpleDateFormat("yyyy-MM-dd").format(selectTime));
        for (int i = 0; i < ptDevices.size(); i++) {
            PtDevice ptDevice =  ptDevices.get(i);
            temperature.add(ptDevice.getPtTemperature());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(ptDevice.getRecordTime()));
        }
        all.add(temperature);
        all.add(time);
        return all;
    }




}
