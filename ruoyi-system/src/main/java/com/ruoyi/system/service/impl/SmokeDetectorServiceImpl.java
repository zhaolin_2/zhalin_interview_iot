package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.Sample;
import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.TcpDataHandler;
import com.ruoyi.system.tcp.DataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SmokeDetectorMapper;
import com.ruoyi.system.domain.SmokeDetector;
import com.ruoyi.system.service.ISmokeDetectorService;

/**
 * 报警器Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-04
 */
@Service
@Slf4j
public class SmokeDetectorServiceImpl implements ISmokeDetectorService 
{
    @Autowired
    private SmokeDetectorMapper smokeDetectorMapper;

    @Autowired
    private DeviceLimitMapper deviceLimitMapper;

    @Autowired
    private DeviceAlertMapper deviceAlertMapper;

    /**
     * 查询报警器
     * 
     * @param id 报警器主键
     * @return 报警器
     */
    @Override
    public SmokeDetector selectSmokeDetectorById(Integer id)
    {
        return smokeDetectorMapper.selectSmokeDetectorById(id);
    }

    /**
     * 查询报警器列表
     * 
     * @param smokeDetector 报警器
     * @return 报警器
     */
    @Override
    public List<SmokeDetector> selectSmokeDetectorList(SmokeDetector smokeDetector)
    {
        return smokeDetectorMapper.selectSmokeDetectorList(smokeDetector);
    }

    /**
     * 新增报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    @Override
    public int insertSmokeDetector(SmokeDetector smokeDetector)
    {
        return smokeDetectorMapper.insertSmokeDetector(smokeDetector);
    }

    /**
     * 修改报警器
     * 
     * @param smokeDetector 报警器
     * @return 结果
     */
    @Override
    public int updateSmokeDetector(SmokeDetector smokeDetector)
    {
        return smokeDetectorMapper.updateSmokeDetector(smokeDetector);
    }

    /**
     * 批量删除报警器
     * 
     * @param ids 需要删除的报警器主键
     * @return 结果
     */
    @Override
    public int deleteSmokeDetectorByIds(Integer[] ids)
    {
        return smokeDetectorMapper.deleteSmokeDetectorByIds(ids);
    }

    /**
     * 删除报警器信息
     * 
     * @param id 报警器主键
     * @return 结果
     */
    @Override
    public int deleteSmokeDetectorById(Integer id)
    {
        return smokeDetectorMapper.deleteSmokeDetectorById(id);
    }

    /**
     * 通过tcp向客户端发送询问烟雾报警器状态
     */
//    @Scheduled(cron = "30 0/4 * * * ?")
    public void sendMessageForSmokeDetector() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "040300030001745F";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    /**
     * 温湿度传感器 数据录入方法
     * @param msg
     */
    @Override
    public void smokeDetectorRecord(String msg) throws Exception {
        TcpDataHandler tcpDataHandler = new TcpDataHandler();
        Map<String, Integer> smokeDetectorMap = tcpDataHandler.receivedSmokeDetectorHex2String(msg);
        //实例化空白的烟雾报警器 状态对象
        SmokeDetector smokeDetector = new SmokeDetector();
        smokeDetector.setRecordTime(new Date());
        smokeDetector.setDetectorState(smokeDetectorMap.get("报警状态"));
        //设置值
        log.info(smokeDetector.toString());

        DeviceLimit deviceLimit = deviceLimitMapper.selectDeviceLimitByCheckValue("detector_state");
        if (smokeDetector.getDetectorState() > deviceLimit.getLimitLower()){
            deviceLimit.setDeviceState("报警");
            deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"烟雾报警器","烟雾报警器触发","报警","未处理",new Date()));
            new Sample().sendSmokeAlert();
        }else{
            deviceLimit.setDeviceState("正常");
        }

        smokeDetectorMapper.insertSmokeDetector(smokeDetector);
        deviceLimitMapper.updateDeviceLimit(deviceLimit);
    }



}
