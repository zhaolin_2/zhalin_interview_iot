package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockMessageMapper;
import com.ruoyi.system.domain.StockMessage;
import com.ruoyi.system.service.IStockMessageService;

/**
 * 库存详情Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-30
 */
@Service
public class StockMessageServiceImpl implements IStockMessageService 
{
    @Autowired
    private StockMessageMapper stockMessageMapper;

    /**
     * 查询库存详情
     * 
     * @param id 库存详情主键
     * @return 库存详情
     */
    @Override
    public StockMessage selectStockMessageById(Long id)
    {
        return stockMessageMapper.selectStockMessageById(id);
    }

    /**
     * 查询库存详情列表
     * 
     * @param stockMessage 库存详情
     * @return 库存详情
     */
    @Override
    public List<StockMessage> selectStockMessageList(StockMessage stockMessage)
    {
        return stockMessageMapper.selectStockMessageList(stockMessage);
    }

    /**
     * 新增库存详情
     * 
     * @param stockMessage 库存详情
     * @return 结果
     */
    @Override
    public int insertStockMessage(StockMessage stockMessage)
    {
        return stockMessageMapper.insertStockMessage(stockMessage);
    }

    /**
     * 修改库存详情
     * 
     * @param stockMessage 库存详情
     * @return 结果
     */
    @Override
    public int updateStockMessage(StockMessage stockMessage)
    {
        return stockMessageMapper.updateStockMessage(stockMessage);
    }

    /**
     * 批量删除库存详情
     * 
     * @param ids 需要删除的库存详情主键
     * @return 结果
     */
    @Override
    public int deleteStockMessageByIds(Long[] ids)
    {
        return stockMessageMapper.deleteStockMessageByIds(ids);
    }

    /**
     * 删除库存详情信息
     * 
     * @param id 库存详情主键
     * @return 结果
     */
    @Override
    public int deleteStockMessageById(Long id)
    {
        return stockMessageMapper.deleteStockMessageById(id);
    }
}
