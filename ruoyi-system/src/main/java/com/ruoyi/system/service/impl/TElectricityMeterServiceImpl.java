package com.ruoyi.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.common.utils.encoding.MyEncoder;
import com.ruoyi.system.domain.DeviceAlert;
import com.ruoyi.system.domain.DeviceLimit;
import com.ruoyi.system.mapper.DeviceAlertMapper;
import com.ruoyi.system.mapper.DeviceLimitMapper;
import com.ruoyi.system.tcp.DataHandler;
import com.ruoyi.system.tcp.TcpDataHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TElectricityMeterMapper;
import com.ruoyi.system.domain.TElectricityMeter;
import com.ruoyi.system.service.ITElectricityMeterService;

/**
 * 三项电表Service业务层处理
 * 
 * @author Lin
 * @date 2022-11-09
 */
@Service
@Slf4j
public class TElectricityMeterServiceImpl implements ITElectricityMeterService 
{
    @Autowired
    private TElectricityMeterMapper tElectricityMeterMapper;

    @Autowired
    TcpDataHandler tcpDataHandler;

    @Autowired
    DeviceLimitMapper deviceLimitMapper;

    @Autowired
    DeviceAlertMapper deviceAlertMapper;

    /**
     * 存储三项电表数据的map，当所有内容都集齐时，进行一次数据库写入操作
     */
    public static Map<String,Double> T_electricity_allMap =new HashMap<>();


    /**
     * 查询三项电表
     * 
     * @param id 三项电表主键
     * @return 三项电表
     */
    @Override
    public TElectricityMeter selectTElectricityMeterById(Long id)
    {
        return tElectricityMeterMapper.selectTElectricityMeterById(id);
        
    }

    /**
     * 查询三项电表列表
     * 
     * @param tElectricityMeter 三项电表
     * @return 三项电表
     */
    @Override
    public List<TElectricityMeter> selectTElectricityMeterList(TElectricityMeter tElectricityMeter)
    {
        return tElectricityMeterMapper.selectTElectricityMeterList(tElectricityMeter);
    }

    /**
     * 新增三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    @Override
    public int insertTElectricityMeter(TElectricityMeter tElectricityMeter)
    {
        return tElectricityMeterMapper.insertTElectricityMeter(tElectricityMeter);
    }

    /**
     * 修改三项电表
     * 
     * @param tElectricityMeter 三项电表
     * @return 结果
     */
    @Override
    public int updateTElectricityMeter(TElectricityMeter tElectricityMeter)
    {
        return tElectricityMeterMapper.updateTElectricityMeter(tElectricityMeter);
    }

    /**
     * 批量删除三项电表
     * 
     * @param ids 需要删除的三项电表主键
     * @return 结果
     */
    @Override
    public int deleteTElectricityMeterByIds(Long[] ids)
    {
        return tElectricityMeterMapper.deleteTElectricityMeterByIds(ids);
    }

    /**
     * 删除三项电表信息
     * 
     * @param id 三项电表主键
     * @return 结果
     */
    @Override
    public int deleteTElectricityMeterById(Long id)
    {
        return tElectricityMeterMapper.deleteTElectricityMeterById(id);
    }


    /**
     * 通过tcp向客户端发送询问 电表的电压 电流 有功功率
     */
//    @Scheduled(cron = "15 1/4 * * * ?")
    public void sendMessageForThreeElectricity() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "060301000016C44F";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }


    /**
     * 通过tcp向客户端发送询问 有功电能
     */
//    @Scheduled(cron = "25 1/4 * * * ?")
    public void sendMessageForTKWH() {
        try {
            //从tcp的会话处理类里提取出保存的会话map
            Map<String, ChannelHandlerContext> ctxMap = new DataHandler().ctxMap;
            //取出存储的会话
            ChannelHandlerContext tcp = ctxMap.get("tcp");
            //询问温湿度的16进制字符串
            String a = "0603001D000255BA";
            //发送处理
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeBytes(MyEncoder.hexString2Bytes(a));
            //发送
            tcp.write(buffer);
            tcp.flush();
        }catch (Exception e){
            log.info("无客户端连接，请检查后重试。");
        }
    }

    /**
     * 三项电表 数据-电流 电压 有功功率 录入方法
     * @param msg
     */
    @Override
    public void tElectricityMeterRecord(String msg){
        Map<String, Double> tElectricityMap = tcpDataHandler.receivedThreeXHex2String(msg);
        TElectricityMeter tElectricityMeter = new TElectricityMeter();
        T_electricity_allMap.put("A项电流",tElectricityMap.get("A项电流"));
        T_electricity_allMap.put("B项电流",tElectricityMap.get("B项电流"));
        T_electricity_allMap.put("C项电流",tElectricityMap.get("C项电流"));
        T_electricity_allMap.put("A项电压",tElectricityMap.get("A项电压"));
        T_electricity_allMap.put("B项电压",tElectricityMap.get("B项电压"));
        T_electricity_allMap.put("C项电压",tElectricityMap.get("C项电压"));
        T_electricity_allMap.put("总有功功率",tElectricityMap.get("总有功功率"));
        T_electricity_allMap.put("A项有功功率",tElectricityMap.get("A项有功功率"));
        T_electricity_allMap.put("B项有功功率",tElectricityMap.get("B项有功功率"));
        T_electricity_allMap.put("C项有功功率",tElectricityMap.get("C项有功功率"));
        if (T_electricity_allMap.size() == 11){
            tElectricityMeter.setRecordTime(new Date());
            tElectricityMeter.setApA(T_electricity_allMap.get("A项有功功率"));
            tElectricityMeter.setApB(T_electricity_allMap.get("B项有功功率"));
            tElectricityMeter.setApC(T_electricity_allMap.get("C项有功功率"));
            tElectricityMeter.setUc(T_electricity_allMap.get("C项电压"));
            tElectricityMeter.setUb(T_electricity_allMap.get("B项电压"));
            tElectricityMeter.setUa(T_electricity_allMap.get("A项电压"));
            tElectricityMeter.setIc(T_electricity_allMap.get("C项电流"));
            tElectricityMeter.setIb(T_electricity_allMap.get("B项电流"));
            tElectricityMeter.setIa(T_electricity_allMap.get("A项电流"));
            tElectricityMeter.setApAll(T_electricity_allMap.get("总有功功率"));
            tElectricityMeter.setAeAll(T_electricity_allMap.get("总有功电能"));

            DeviceLimit deviceLimitIA = deviceLimitMapper.selectDeviceLimitByCheckValue("ia");
            DeviceLimit deviceLimitUA = deviceLimitMapper.selectDeviceLimitByCheckValue("ua");
            DeviceLimit deviceLimitAP = deviceLimitMapper.selectDeviceLimitByCheckValue("ap_all");
            if (tElectricityMeter.getIa() > deviceLimitIA.getLimitUpper()){
                deviceLimitIA.setDeviceState("电流过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电流过高",tElectricityMeter.getIa().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getIa() < deviceLimitIA.getLimitLower()){
                deviceLimitIA.setDeviceState("电流过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电流过低",tElectricityMeter.getIa().toString(),"未处理",new Date()));
            }else{
                deviceLimitIA.setDeviceState("正常");
            }

            if (tElectricityMeter.getUa() > deviceLimitUA.getLimitUpper()){
                deviceLimitUA.setDeviceState("电压过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电压过高",tElectricityMeter.getUa().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getUa() < deviceLimitUA.getLimitLower()){
                deviceLimitUA.setDeviceState("电压过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电压过低",tElectricityMeter.getUa().toString(),"未处理",new Date()));
            }else{
                deviceLimitUA.setDeviceState("正常");
            }

            if (tElectricityMeter.getApAll() > deviceLimitAP.getLimitUpper()){
                deviceLimitAP.setDeviceState("功率过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","功率过高",tElectricityMeter.getApAll().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getApAll() < deviceLimitAP.getLimitLower()){
                deviceLimitAP.setDeviceState("功率过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","功率过低",tElectricityMeter.getApAll().toString(),"未处理",new Date()));
            }else{
                deviceLimitAP.setDeviceState("正常");
            }

            deviceLimitMapper.updateDeviceLimit(deviceLimitIA);
            deviceLimitMapper.updateDeviceLimit(deviceLimitUA);
            deviceLimitMapper.updateDeviceLimit(deviceLimitAP);
            tElectricityMeterMapper.insertTElectricityMeter(tElectricityMeter);
            T_electricity_allMap.clear();
        }
    }

    /**
     * 三项电表 数据-电流 电压 有功功率 录入方法
     * @param msg
     */
    @Override
    public void tElectricityMeterKWHRecord(String msg){
        Map<String, Double> tElectricityMap = tcpDataHandler.receivedThreeKWHHex2String(msg);
        TElectricityMeter tElectricityMeter = new TElectricityMeter();
        T_electricity_allMap.put("总有功电能",tElectricityMap.get("总有功电能"));
        if (T_electricity_allMap.size() == 11){
            tElectricityMeter.setRecordTime(new Date());
            tElectricityMeter.setApA(T_electricity_allMap.get("A项有功功率"));
            tElectricityMeter.setApB(T_electricity_allMap.get("B项有功功率"));
            tElectricityMeter.setApC(T_electricity_allMap.get("C项有功功率"));
            tElectricityMeter.setUc(T_electricity_allMap.get("C项电压"));
            tElectricityMeter.setUb(T_electricity_allMap.get("B项电压"));
            tElectricityMeter.setUa(T_electricity_allMap.get("A项电压"));
            tElectricityMeter.setIc(T_electricity_allMap.get("C项电流"));
            tElectricityMeter.setIb(T_electricity_allMap.get("B项电流"));
            tElectricityMeter.setIa(T_electricity_allMap.get("A项电流"));
            tElectricityMeter.setApAll(T_electricity_allMap.get("总有功功率"));
            tElectricityMeter.setAeAll(T_electricity_allMap.get("总有功电能"));

            DeviceLimit deviceLimitIA = deviceLimitMapper.selectDeviceLimitByCheckValue("ia");
            DeviceLimit deviceLimitUA = deviceLimitMapper.selectDeviceLimitByCheckValue("ua");
            DeviceLimit deviceLimitAP = deviceLimitMapper.selectDeviceLimitByCheckValue("ap_all");
            if (tElectricityMeter.getIa() > deviceLimitIA.getLimitUpper()){
                deviceLimitIA.setDeviceState("电流过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电流过高",tElectricityMeter.getIa().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getIa() < deviceLimitIA.getLimitLower()){
                deviceLimitIA.setDeviceState("电流过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电流过低",tElectricityMeter.getIa().toString(),"未处理",new Date()));
            }else{
                deviceLimitIA.setDeviceState("正常");
            }

            if (tElectricityMeter.getUa() > deviceLimitUA.getLimitUpper()){
                deviceLimitUA.setDeviceState("电压过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电压过高",tElectricityMeter.getUa().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getUa() < deviceLimitUA.getLimitLower()){
                deviceLimitUA.setDeviceState("电压过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","电压过低",tElectricityMeter.getUa().toString(),"未处理",new Date()));
            }else{
                deviceLimitUA.setDeviceState("正常");
            }

            if (tElectricityMeter.getApAll() > deviceLimitAP.getLimitUpper()){
                deviceLimitAP.setDeviceState("功率过高");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","功率过高",tElectricityMeter.getApAll().toString(),"未处理",new Date()));
            }else if(tElectricityMeter.getApAll() < deviceLimitAP.getLimitLower()){
                deviceLimitAP.setDeviceState("功率过低");
                deviceAlertMapper.insertDeviceAlert(new DeviceAlert(null,"4P三相电子式导轨电表","功率过低",tElectricityMeter.getApAll().toString(),"未处理",new Date()));
            }else{
                deviceLimitAP.setDeviceState("正常");
            }

            deviceLimitMapper.updateDeviceLimit(deviceLimitIA);
            deviceLimitMapper.updateDeviceLimit(deviceLimitUA);
            deviceLimitMapper.updateDeviceLimit(deviceLimitAP);
            tElectricityMeterMapper.insertTElectricityMeter(tElectricityMeter);
            T_electricity_allMap.clear();
        }
    }


    /**
     * 查询电流与瞬时功率曲线
     * @param selectDate
     * @return
     */
    @Override
    public List selectTElectricityListByDate(Date selectDate){
        List ia = new ArrayList();
        List ap_all = new ArrayList();
        List time = new ArrayList();
        List all = new ArrayList();
        List<TElectricityMeter> tElectricityMeters = tElectricityMeterMapper.selectTElectricityListByDate(new SimpleDateFormat("yyyy-MM-dd").format(selectDate));
        for (int i = 0; i < tElectricityMeters.size(); i++) {
            TElectricityMeter tElectricityMeter =  tElectricityMeters.get(i);
            ia.add(tElectricityMeter.getIa());
            ap_all.add(tElectricityMeter.getApAll());
            time.add(new SimpleDateFormat("MM-dd HH:mm:ss").format(tElectricityMeter.getRecordTime()));
        }
        Collections.addAll(all,ia,ap_all,time);
        return all;
    }

    /**
     * 查询最新一条记录
     * @return
     */
    @Override
    public TElectricityMeter selectLastTElectricity(){
        TElectricityMeter tElectricityMeter = tElectricityMeterMapper.selectLastTElectricity();
        return tElectricityMeter;
    }



}
