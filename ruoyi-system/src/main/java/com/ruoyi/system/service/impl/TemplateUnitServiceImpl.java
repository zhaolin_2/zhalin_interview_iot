package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.system.domain.OfficeStock;
import com.ruoyi.system.domain.request.TemplateUnitRequest;
import com.ruoyi.system.mapper.OfficeStockMapper;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TemplateUnitMapper;
import com.ruoyi.system.domain.TemplateUnit;
import com.ruoyi.system.service.ITemplateUnitService;

/**
 * 组件Service业务层处理
 * 
 * @author lin
 * @date 2022-12-07
 */
@Service
public class TemplateUnitServiceImpl implements ITemplateUnitService 
{
    @Autowired
    private TemplateUnitMapper templateUnitMapper;

    @Autowired
    private OfficeStockMapper officeStockMapper;

    /**
     * 查询组件
     * 
     * @param id 组件主键
     * @return 组件
     */
    @Override
    public TemplateUnit selectTemplateUnitById(Long id)
    {
        return templateUnitMapper.selectTemplateUnitById(id);
    }

    /**
     * 查询组件列表
     * 
     * @param templateUnit 组件
     * @return 组件
     */
    @Override
    public List<TemplateUnit> selectTemplateUnitList(TemplateUnit templateUnit)
    {
        return templateUnitMapper.selectTemplateUnitList(templateUnit);
    }

    /**
     * 新增组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    @Override
    public int insertTemplateUnit(TemplateUnit templateUnit)
    {
        return templateUnitMapper.insertTemplateUnit(templateUnit);
    }

    /**
     * 修改组件
     * 
     * @param templateUnit 组件
     * @return 结果
     */
    @Override
    public int updateTemplateUnit(TemplateUnit templateUnit)
    {
        return templateUnitMapper.updateTemplateUnit(templateUnit);
    }

    /**
     * 批量删除组件
     * 
     * @param ids 需要删除的组件主键
     * @return 结果
     */
    @Override
    public int deleteTemplateUnitByIds(Long[] ids)
    {
        return templateUnitMapper.deleteTemplateUnitByIds(ids);
    }

    /**
     * 删除组件信息
     * 
     * @param id 组件主键
     * @return 结果
     */
    @Override
    public int deleteTemplateUnitById(Long id)
    {
        return templateUnitMapper.deleteTemplateUnitById(id);
    }


    /**
     * 前端传回模板id 程序组装一个数组，内含该组件包含模板的组件信息
     * @param templateId
     * @return
     */
    @Override
    public List<TemplateUnitRequest> showTemplateUnitMes(Long templateId){
        //查询该模板id下的所有组件信息
        TemplateUnit t = new TemplateUnit();
        t.setTemplateId(templateId);
        List<TemplateUnit> templateUnits = templateUnitMapper.selectTemplateUnitList(t);
        /**
         * 将查询到的组件全部包装为返回类，存入数组返回
         */
        List<TemplateUnitRequest> stockUnits = new ArrayList<>();
        for (int i = 0; i < templateUnits.size(); i++) {
            TemplateUnit templateUnit =  templateUnits.get(i);
            OfficeStock officeStock = officeStockMapper.selectOfficeStockById(templateUnit.getStockId());
            TemplateUnitRequest templateUnitRequest = new TemplateUnitRequest((long) (i+1),officeStock.getStockName(),officeStock.getStockType(),officeStock.getStockPackage(),templateUnit.getStockNum(),officeStock.getId());
            stockUnits.add(templateUnitRequest);
        }
        return stockUnits;
    }

    @Override
    public void addTemplateUnits(Long templateId, Long stockId, Long num) {
        TemplateUnit templateUnit1 = templateUnitMapper.selectTemplateUnitByStockId(stockId);
        if (templateUnit1 == null){
            TemplateUnit templateUnit = new TemplateUnit(null,templateId,stockId,num);
            templateUnitMapper.insertTemplateUnit(templateUnit);
        }else{
            TemplateUnit templateUnit = new TemplateUnit(templateUnit1.getId(),templateId,stockId,templateUnit1.getStockNum()+num);
            templateUnitMapper.updateTemplateUnit(templateUnit);
        }
    }

    @Override
    public int updateTemplateUnits(Long stockId,Long num) {
        TemplateUnit templateUnit = templateUnitMapper.selectTemplateUnitByStockId(stockId);
        templateUnit.setStockNum(num);
        return templateUnitMapper.updateTemplateUnit(templateUnit);
    }

    @Override
    public int deleteTemplateUnits(Long stockId) {
        TemplateUnit templateUnit = templateUnitMapper.selectTemplateUnitByStockId(stockId);
        return templateUnitMapper.deleteTemplateUnitById(templateUnit.getId());
    }


}
