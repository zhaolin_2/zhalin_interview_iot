package com.ruoyi.system.service.impl;

import com.ruoyi.system.service.TestService;

import org.springframework.stereotype.Service;

/**
 * @Author Lin
 * @Date 2022 10 27 09
 **/
@Service
public class TestServiceImpl implements TestService {

    @Override
    public void sendTcpTest() {
//        String order= "01 03 00 01 00 01 D5 CA";
//        byte[] bytes = hexStringToByteArray(order);
//        new DataHandler().sendMessage(bytes);
    }

    /**
     * 16进制表示的字符串转换为字节数组
     *
     * @param hexString 16进制表示的字符串
     * @return byte[] 字节数组
     */
    public static byte[] hexStringToByteArray(String hexString) {
        hexString = hexString.replaceAll(" ", "");
        int len = hexString.length();
        byte[] bytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
            bytes[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character
                    .digit(hexString.charAt(i + 1), 16));
        }
        return bytes;
    }
}
