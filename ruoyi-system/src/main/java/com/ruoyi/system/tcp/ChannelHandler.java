package com.ruoyi.system.tcp;

import com.ruoyi.common.utils.encoding.MyDecoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

public class ChannelHandler extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {

        //自定义解码类，将16进制字符串转为16进制数字
        socketChannel.pipeline().addLast("decoder",new MyDecoder());

//        socketChannel.pipeline().addLast(new StringDecoder(CharsetUtil.UTF_8));

        socketChannel.pipeline().addLast(new StringEncoder(CharsetUtil.UTF_8));

        socketChannel.pipeline().addLast(new DataHandler());






    }
}
