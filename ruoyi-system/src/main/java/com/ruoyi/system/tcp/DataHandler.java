package com.ruoyi.system.tcp;
import cn.hutool.json.JSONUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.service.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 数据处理：包括数据接收和数据发送
 */
@Slf4j
public class DataHandler extends SimpleChannelInboundHandler<String> {

    //存放TCP会话
    public static Map<String, ChannelHandlerContext> ctxMap = new ConcurrentHashMap<String, ChannelHandlerContext>(16);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        //注意：如果发生编码错误，就会断开链接，且无任何报错提示
        log.info(msg);
        //温湿度采集器服务类
        IOfficeHumitureService HumitureBean = SpringUtils.getBean(IOfficeHumitureService.class);
        //百叶箱集成传感器
        ILouverBoxService louverBoxBean = SpringUtils.getBean(ILouverBoxService.class);
        //pt100 温度采集器
        IPtDeviceService ptTemperatureBean = SpringUtils.getBean(IPtDeviceService.class);
        //烟雾报警器 服务类
        ISmokeDetectorService smokeDetectorBean = SpringUtils.getBean(ISmokeDetectorService.class);
        //电表信息 服务类
        IElectricityMeterService electricityMeterService = SpringUtils.getBean(IElectricityMeterService.class);
        //三项电表 服务类
        ITElectricityMeterService t_electricityMeterService = SpringUtils.getBean(ITElectricityMeterService.class);
        try {
            if(msg.startsWith("0503")){
                //温湿度传感器 温湿度处理
                HumitureBean.humitureRecord(msg);
            }else if(msg.startsWith("010304")){
                //百叶箱式集成传感器--温湿度
                louverBoxBean.humitureRecord(msg);
            }else if(msg.startsWith("010308")){
                //百叶箱式集成传感器--光照
                louverBoxBean.illuminationRecord(msg);
            }else if(msg.startsWith("030302")){
                //PT100温度采集模块
                ptTemperatureBean.ptTemperatureRecord(msg);
            }else if(msg.startsWith("020428")){
                //电表--电压/电流/有功功率
                electricityMeterService.AUApRecord(msg);
            }else if(msg.startsWith("020404")){
                //电表--功率因数
                electricityMeterService.PfRecord(msg);
            }else if(msg.startsWith("020406")){
                //电表--频率
                electricityMeterService.frequencyRecord(msg);
            }else if(msg.startsWith("020408")){
                //电表--总有功电量
                electricityMeterService.AERecord(msg);
            }else if(msg.startsWith("020402")){
                //电表--拉合闸状态
                electricityMeterService.stateRecord(msg);
            }else if(msg.startsWith("040302")){
                //百叶箱式集成传感器--光照
                smokeDetectorBean.smokeDetectorRecord(msg);
            }else if(msg.startsWith("06032C")){
                //三项电表 数据 带t：
                t_electricityMeterService.tElectricityMeterRecord(msg);
            }else if(msg.startsWith("060304")){
                //三项电表-总有功电量 数据 带t：
                t_electricityMeterService.tElectricityMeterKWHRecord(msg);
            }else if(msg.startsWith("FFFF")){
                log.info("收到心跳消息");
            }else{
                log.info("收到其他未知内容");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ctx.flush();
        }
        ctx.flush();
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.flush();
        ctx.close();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        ctxMap.put("tcp", ctx);
    }

    /**
     * 服务端接收客户端发送过来的数据结束之后调用
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        removeChannelMap(ctx);
        super.channelInactive(ctx);
    }


    /**
     * 移除已经失效的链接
     *
     * @param ctx
     */
    private void removeChannelMap(ChannelHandlerContext ctx) {
        for (String key : ctxMap.keySet()) {
            if (ctxMap.get(key) != null && ctxMap.get(key).equals(ctx)) {
                ctxMap.remove(key);
            }
        }
    }


}
