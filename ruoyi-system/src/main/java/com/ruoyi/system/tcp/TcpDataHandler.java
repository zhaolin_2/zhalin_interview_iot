package com.ruoyi.system.tcp;

import cn.hutool.core.util.NumberUtil;
import com.ruoyi.system.domain.DeviceData;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author Lin
 * @Date 2022 10 27 16
 * 处理收到的16进制数据的工具类，处理并且截取
 **/
@Component
public class TcpDataHandler {

    /**
     * 温湿度 的有效数据截取方法
     *
     * @param receivedMsg 接收的msg数据
     * @return 返回一个map集合，内含温度 湿度
     */
    public Map<String, Double> receivedTempAndHumitureHex2Str(String receivedMsg) {
        //使用map存储处理好的数据，分别存储温度，湿度
        Map<String, Double> humiture = new HashMap<>();
        //将收到的数组截取(湿度)
        String substringHum = receivedMsg.substring(6, 10);
        //将收到的数组截取(温度)
        String substringTemp = receivedMsg.substring(10, 14);
        //16进制转10进制
        int temp = Integer.parseInt(substringTemp, 16);
        int hum = Integer.parseInt(substringHum, 16);
        //将int类型转换为Float类型
        double dTemp = Double.valueOf(temp);
        double dHum = Double.valueOf(hum);
        //将拿到的数组除10 处理，因为取到的数组为被扩大十倍的整数，需要处理
        double divT = NumberUtil.div(dTemp, (double) 10);
        double divH = NumberUtil.div(dHum, (double) 10);
        //保留1位小数，但是返回了BigDecimal类型
        BigDecimal roundT = NumberUtil.round(divT, 1);
        BigDecimal roundH = NumberUtil.round(divH, 1);
        //转换BigDecimal到double类型
        double vT = NumberUtil.toDouble(roundT);
        double vH = NumberUtil.toDouble(roundH);
        humiture.put("温度", vT);
        humiture.put("湿度", vH);
        //返回结果
        return humiture;
    }

    /**
     * 百叶箱集成式传感器 的有效数据截取方法
     * 因为数据处理规则等同于温湿度传感器，所以使用温湿度传感器的方法处理
     *
     * @param receivedMsg 接收的msg数据
     * @return 返回一个map集合，内含温度 湿度
     */
    //TODO:内含温度 湿度
    public Map<String, Double> receivedLouverBoxAHex2Str(String receivedMsg) {
        Map<String, Double> stringDoubleMap = this.receivedTempAndHumitureHex2Str(receivedMsg);
        return stringDoubleMap;
    }

    /**
     * 百叶箱式传感器 光照数据截取处理
     *
     * @param receivedMsg
     * @return
     */
    public Map<String, Double> receivedLouverBoxBHex2String(String receivedMsg) {
        //构造新的map,用来存放光照强度数据
        Map<String, Double> illumination = new HashMap<>();
        //截取
        String illuminationString = receivedMsg.substring(6, 14);
        //转换为十进制数据并且转换为Double类型
        Double a = Double.valueOf(Integer.parseInt(illuminationString, 16));
        illumination.put("光照强度", a);
        return illumination;
    }

    /**
     * pt100 温度传感器数据截取处理
     *
     * @param receivedMsg
     */
    public Map<String, Double> receivedTPDeviceHex2String(String receivedMsg) {
        //构造新的map,用来存放光照强度数据
        Map<String, Double> ptTemperatureMap = new HashMap<>();
        //截取
        String ptString = receivedMsg.substring(6, 10);
        double a = Double.valueOf(Integer.parseInt(ptString, 16));
        double div = NumberUtil.div(a, 10.0);
        ptTemperatureMap.put("温度", div);
        return ptTemperatureMap;
    }

    /**
     * 烟雾报警器 数据截取处理
     *
     * @param receivedMsg
     */
    public Map<String, Integer> receivedSmokeDetectorHex2String(String receivedMsg) {
        Integer detectorState = null;
        //构造新的map,用来存放光照强度数据
        Map<String, Integer> smokeDetectorMap = new HashMap<>();
        //截取
        String substring = receivedMsg.substring(6, 10);
        if (substring.equals("0000")) {
            detectorState = 0;
        } else if (substring.equals("0001")) {
            detectorState = 1;
        } else {
            detectorState = 2;
        }
        smokeDetectorMap.put("报警状态", detectorState);
        return smokeDetectorMap;
    }

    /**
     * 处理电压电流 有功功率的方法 a u active power
     *
     * @param receivedMsg
     */
    public Map<String, Double> receivedAUApHex2String(String receivedMsg) {
        Map<String, Double> AUApMap = new HashMap<>();
        //电压
        BigDecimal roundV = NumberUtil.round(Float.intBitsToFloat(new BigInteger(receivedMsg.substring(6, 14), 16).intValue()), 2);
        Double v = roundV.doubleValue();
        //电流
        BigDecimal roundA = NumberUtil.round(Float.intBitsToFloat((new BigInteger(receivedMsg.substring(38, 46), 16).intValue())), 2);
        Double a = roundA.doubleValue();
        //有功功率
        BigDecimal roundAP = NumberUtil.round(Float.intBitsToFloat((new BigInteger(receivedMsg.substring(78, 86), 16).intValue())), 2);
        Double ap = roundAP.doubleValue();
        AUApMap.put("电压", v);
        AUApMap.put("电流", a);
        AUApMap.put("有功功率", ap);
        return AUApMap;
    }


    /**
     * 处理功率因数的方法power factor
     *
     * @param receivedMsg
     */
    public Double receivedFPHex2String(String receivedMsg) {
        BigDecimal roundPF = NumberUtil.round(Float.intBitsToFloat(new BigInteger(receivedMsg.substring(6, 14), 16).intValue()), 2);
        return roundPF.doubleValue();
    }


    /**
     * 处理频率的方法
     *
     * @param
     */
    public Double receivedFrequencyHex2String(String receivedMsg) {
        BigDecimal roundF = NumberUtil.round(Float.intBitsToFloat(new BigInteger(receivedMsg.substring(6, 14), 16).intValue()), 2);
        return roundF.doubleValue();
    }


    /**
     * 处理总有功电量 active electricity
     *
     * @param
     */
    public Double receivedAEHex2String(String receivedMsg) {
        BigDecimal roundAE = NumberUtil.round(Float.intBitsToFloat(new BigInteger(receivedMsg.substring(6, 14), 16).intValue()), 2);
        return roundAE.doubleValue();
    }


    /**
     * 处理 拉合闸状态
     *
     * @param
     */
    public Integer receivedStateHex2String(String receivedMsg) {
        Integer intState;
        String substring = receivedMsg.substring(6, 10);
        if (substring.equals("0055")) {
            intState = 0;
        } else if (substring.equals("00AA")) {
            intState = 1;
        } else {
            intState = 2;
        }
        System.out.println(intState);
        return intState;
    }

    /**
     * @param receivedMsg 传入的16进制数据
     * @return
     */
    public Map<String, Double> receivedThreeXHex2String(String receivedMsg) {
        Double ua = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(6, 14),16),10.0),1).doubleValue();//A项电压;
        Double ub = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(14,22),16),10.0),1).doubleValue();;//B项电压
        Double uc = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(22,30),16),10.0),1).doubleValue();;//C项电压
        Double ia = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(30,38),16),1000),2).doubleValue();;        //A项电流
        Double ib = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(38,46),16),1000),2).doubleValue();;        //B项电流
        Double ic = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(46,54),16),1000),2).doubleValue();;        //C项电流
        Double ap_all = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(62,70),16),10.0),1).doubleValue();;        //总有功功率
        Double ap_a = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(70,78),16),10.0),1).doubleValue();;        //A项有功功率
        Double ap_b = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(78,86),16),10.0),1).doubleValue();;        //B项有功功率
        Double ap_c = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(86,94),16),10.0),1).doubleValue();;        //C项有功功率
        Map<String,Double> tElectricitiMap = new HashMap<>();
        tElectricitiMap.put("A项电压",ua);
        tElectricitiMap.put("B项电压",ub);
        tElectricitiMap.put("C项电压",uc);
        tElectricitiMap.put("A项电流",ia);
        tElectricitiMap.put("B项电流",ib);
        tElectricitiMap.put("C项电流",ic);
        tElectricitiMap.put("总有功功率",ap_all);
        tElectricitiMap.put("A项有功功率",ap_a);
        tElectricitiMap.put("B项有功功率",ap_b);
        tElectricitiMap.put("C项有功功率",ap_c);
        return tElectricitiMap;
    }

    /**
     * @param receivedMsg 传入的16进制数据
     * @return
     */
    public Map<String, Double> receivedThreeKWHHex2String(String receivedMsg) {
        Double up = NumberUtil.round(NumberUtil.div(Integer.parseInt(receivedMsg.substring(6, 14),16),100.0),1).doubleValue();
        Map<String,Double> tElectricityMap = new HashMap<>();
        tElectricityMap.put("总有功电能",up);
        System.out.println(up);
        return tElectricityMap;
    }


    /**
     * 动态处理数据类
     */
    public  String dataHandlerForTest(DeviceData deviceData){
        Double result ;
        try {
        String substringData = deviceData.getReceivedData().substring(deviceData.getSubHead(), deviceData.getSubEnd());
        if (deviceData.isDataType()){
            Integer intData = Integer.parseInt(substringData,16);
            result = intData.doubleValue();
        }else {
            Float floatData = Float.intBitsToFloat(new BigInteger(substringData,16).intValue());
            result = floatData.doubleValue();
        }
        result = result * deviceData.getDataArg();
        result = NumberUtil.round(result, deviceData.getDataRound()).doubleValue();
        } catch (Exception e){
            throw new RuntimeException("请输入正确字符串");
        }
        return result + deviceData.getDataUnit();
    }



}
