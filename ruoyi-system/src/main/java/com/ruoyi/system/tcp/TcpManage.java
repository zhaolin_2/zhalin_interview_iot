package com.ruoyi.system.tcp;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;

@Slf4j
@Component
public class TcpManage implements ApplicationRunner {


    //初始化
    private EventLoopGroup boss = new NioEventLoopGroup();

    private EventLoopGroup work = new NioEventLoopGroup();


    public void init(InetSocketAddress inetSocketAddress) {
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(boss, work)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(inetSocketAddress)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childOption(ChannelOption.AUTO_READ, true)
                    .childHandler(new ChannelHandler());
            ChannelFuture future = bootstrap.bind(inetSocketAddress).sync();
            if(future.isSuccess()){
                log.info("===>>>TCP 服务端启动");
            }
        } catch (Exception e) {
            e.printStackTrace();
            boss.shutdownGracefully();
            work.shutdownGracefully();
        }
    }


    // >> TODO 这是一个启动TCP代码
    @Override
    public void run(ApplicationArguments args){
        try {
            //172.24.70.146
            //192.168.0.143
            InetSocketAddress tcpAddress = new InetSocketAddress("192.168.0.68",  1885);
            init(tcpAddress);
        }catch ( Exception e){
            log.error("启动失败"+e.getMessage());
        }
    }


}
